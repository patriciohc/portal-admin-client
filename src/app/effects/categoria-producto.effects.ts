import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import * as categoriaActions from '../actions/categoria-producto.actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { CategoriaProductoService } from '../logposeapp-api/categoria-producto.service';
import { RequestError } from '../models/request-error';
import * as mssageActions from '../actions/message.actions';


@Injectable()
export class CategoriaProductoEffects {

    constructor(
        private actions$: Actions,
        private service: CategoriaProductoService
    ) { }

    @Effect()
    loadListCategoriaProducto$: Observable<Action> = this.actions$.pipe(
        ofType(categoriaActions.loadListCategoriaProducto),
        switchMap(() => {
            return this.service.getListCategorias()
                .pipe(
                    map((categorias) => {
                        return categoriaActions.setListCategoriaProducto({ categorias });
                    }),
                    catchError(this.handlingError)
                );
        })
    );

    @Effect()
    addCategoria$: Observable<Action> = this.actions$.pipe(
        ofType(categoriaActions.addNewCategoriaProducto),
        switchMap((action) => {
            return this.service.addCategoria(action.categoria)
                .pipe(
                    switchMap((categoria) => [
                        categoriaActions.addLocalNewCategoriaProducto({ categoria }),
                        mssageActions.newMessage({ text: 'Se guardo correctamente' }),
                    ]),
                    catchError(this.handlingError)
                );
        })
    );

    @Effect()
    updateCategoria$: Observable<Action> = this.actions$.pipe(
        ofType(categoriaActions.updateCategoriaProducto),
        switchMap((action) => {
            return this.service.updateCategoria(action.categoria)
                .pipe(
                    switchMap(() => [
                        categoriaActions.updateLocalCategoriaProducto({ categoria: action.categoria }),
                        mssageActions.newMessage({ text: 'Se actualizo correctamente' }),
                    ]),
                    catchError(this.handlingError)
                );
        })
    );

    @Effect()
    removeCategoriaProducto$: Observable<Action> = this.actions$.pipe(
        ofType(categoriaActions.removeCategoriaProducto),
        switchMap(({ id }) => {
            return this.service.delteCategoria(id)
                .pipe(
                    map(() => {
                        return categoriaActions.removeLocalCategoriaProducto({ id });
                    }),
                    catchError(this.handlingError)
                );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error : 'Error al comunicarse con el servidor',
            statusCode: error ? error.status : -1
        };
        return of(categoriaActions.requestFailure({ requestError }));
        /*return switchMap(() => [
            of(categoriaActions.requestFailure({ requestError })),
            of(mssageActions.newMessage({ level: 'error', text: requestError.message }))
        ])*/
    }

}

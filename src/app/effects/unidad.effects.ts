import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as unidadesActions from '../actions/unidad.actions';
import * as mssageActions from '../actions/message.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {RequestError} from '../models/request-error';
import { UnidadService } from '../logposeapp-api/unidad.service';

@Injectable()
export class UnidadEffects {

    constructor(
        private actions$: Actions,
        private unidadesService: UnidadService
    ) {}

    @Effect()
    loadListUnidades$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.loadListUnidades),
        switchMap(() => {
            return this.unidadesService.getListUnidadesBySocio()
            .pipe(
                map((unidades) => {
                    return unidadesActions.setListUnidades({unidades});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    addUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.addUnidad),
        switchMap((action) => {
            return this.unidadesService.addUnidadToSocio(action.unidad)
            .pipe(
                map((unidad) => {
                    return unidadesActions.addUnidadToList({unidad});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    updateUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.updateUnidad),
        switchMap((action) => {
            return this.unidadesService.updateUnidad(action.unidad)
            .pipe(
                switchMap(() => [
                    unidadesActions.updateLocalUnidad({unidad: action.unidad}),
                    mssageActions.newMessage({level: 'info', text: 'Se guardo correctamente'})
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    delteUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.deleteUnidad),
        switchMap((action) => {
            return this.unidadesService.deleteUnidadToSocio(action.id)
            .pipe(
                map(() => {
                    return unidadesActions.deleteUnidadToList({id: action.id});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    addProductsToUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.addProductsToUnidad),
        switchMap((action) => {
            return this.unidadesService.addProductsToUnidad(action.id, action.list)
            .pipe(
                map((list) => {
                    return unidadesActions.localUpdateProductsUnidad({id: action.id, list});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    addAllProductsToUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.addAllProductsToUnidad),
        switchMap((action) => {
            return this.unidadesService.addAllProductsToUnidad(action.idUnidad)
            .pipe(
                map((list) => {
                    return unidadesActions.localUpdateProductsUnidad({id: action.idUnidad, list});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    removeProductFromUnidad$: Observable<Action> = this.actions$.pipe(
        ofType(unidadesActions.removeProductFromUnidad),
        switchMap((action) => {
            return this.unidadesService.removeProductFromUnidad(action.idUnidad, action.idProducto)
            .pipe(
                map(() => {
                    return unidadesActions.localRemoveProductsUnidad({idUnidad: action.idUnidad, idProducto: action.idProducto});
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : error.message,
            statusCode: error ? error.status : -1
        };
        return of(unidadesActions.requestFailure({requestError}));
    }
}

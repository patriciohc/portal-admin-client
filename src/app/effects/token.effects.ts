import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import * as refreshTokenActions from '../actions/token.actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { RequestError } from '../models/request-error';

const SERVER_URL = env.logposeApiConfig.api

@Injectable()
export class RefreshTokenEffects {

    path = 'account/tokenrefresh';

    constructor(
        private actions$: Actions,
        private http: HttpClient
    ) { }

    @Effect()
    loadRefreshToken$: Observable<Action> = this.actions$.pipe(
        ofType(refreshTokenActions.loadRefreshToken),
        switchMap((payload) => {
            const access = 'Basic ' + btoa(payload.correo_electronico + ':' + payload.password);
            const options = {
                headers: { Authorization: access }
            };
            return this.http.get<{ token: string }>(
                SERVER_URL + this.path + '?tipo=' + payload.typeAccount, options
            )
                .pipe(
                    map((response) => {
                        return refreshTokenActions.setRefreshToken({ refreshToken: response.token });
                    }),
                    catchError(this.handlingError)
                );
        })
    );

    @Effect()
    reloadRefreshToken$: Observable<Action> = this.actions$.pipe(
        ofType(refreshTokenActions.reloadRefreshToken),
        switchMap(() => {
            return this.http.get<{ token: string }>(
                SERVER_URL + this.path + '/reload'
            )
                .pipe(
                    map((response) => {
                        return refreshTokenActions.setRefreshToken({ refreshToken: response.token });
                    }),
                    catchError(this.handlingError)
                );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : error.message,
            statusCode: error ? error.status : -1
        };
        return of(refreshTokenActions.loadTokenFailure({requestError}));
    }
}

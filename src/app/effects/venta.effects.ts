import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as ventaActions from '../actions/venta.actions';
import * as mssageActions from '../actions/message.actions';
import * as ticketActions from '../actions/ticket.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {RequestError} from '../models/request-error';
import { VentaService } from '../logposeapp-api/venta.service';
import { GenericResponse } from '../models/generic-response';
import { Venta } from './../models/venta.model';
import { PedidoService } from '../logposeapp-api/pedido.service';

@Injectable()
export class VentaEffects {

    constructor(
        private actions$: Actions,
        private ventaService: VentaService
    ) {}

    @Effect()
    saveVentaEnSitio$: Observable<Action> = this.actions$.pipe(
        ofType(ventaActions.saveVenta),
        switchMap(({venta}) => {
            return this.ventaService.saveVenta(venta)
            .pipe(
                switchMap((venta: Venta) => [
                    mssageActions.newMessage({text: 'La venta se guardo correctamente'}),
                    ticketActions.newTicket({venta})
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    makeRecarga$: Observable<Action> = this.actions$.pipe(
        ofType(ventaActions.makeRecarga),
        switchMap(({recarga}) => {
            return this.ventaService.makeRecarga(recarga)
            .pipe(
                switchMap((response: GenericResponse) => [
                    this.recargaMessage(response),
                    this.createTicket(response),
                    ventaActions.setStatusRecargaRequest({statusRecargaRequest: response.success})
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    loadReportVentas$: Observable<Action> = this.actions$.pipe(
        ofType(ventaActions.loadReportVentas),
        switchMap(({unidadId, initDate, endDate}) => {
            return this.ventaService.getTotalSales(unidadId, initDate, endDate)
            .pipe(
                map((reporteVentas) => {
                    return ventaActions.setReportVentas({reporteVentas});
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        var message = '';
        if (error && error.error && typeof (error.error) == 'string') {
            message = error.error;
        }

        if (error && error.error && typeof (error.error) == 'object' && error.error.message) {
            message = error.error.message;
        }

        const requestError: RequestError = {
            message: message,
            statusCode: error ? error.status : -1
        };
        return of(ventaActions.requestFailure({requestError}));
    }

    private recargaMessage(response: GenericResponse) {
        if (response.success) {
            return mssageActions.newMessage({text: 'La recarga se realizo correctamente'});
        } else {
            return mssageActions.newMessage({text: response.error});
        }
    }

    private createTicket(response: GenericResponse) {
        console.log(response);
        if (response.success) {
            if (response && response.data) {
                if (response.data.datosVenta) {
                    return ticketActions.newTicket({venta: response.data.datosVenta});
                }
                if (response.data.recargaResult) {
                    let venta = Venta.creteFromRecarga(response.data.recargaResult);
                    return ticketActions.newTicket({venta});
                }
            }
        } else {
            return mssageActions.newMessage({text: 'No se puedo realizar la recarga' + response.error});
        }
    }

}

import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as productActions from '../actions/producto.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {RequestError} from '../models/request-error';
import { ProductoService } from '../logposeapp-api/producto.service';
import * as mssageActions from '../actions/message.actions';

@Injectable()
export class ProductoEffects {

    constructor(
        private actions$: Actions,
        private productoService: ProductoService
    ) {}

    @Effect()
    loadListProductos$: Observable<Action> = this.actions$.pipe(
        ofType(productActions.loadListProductos),
        switchMap(() => {
            return this.productoService.getListProductosBySocio()
            .pipe(
                map((productos) => {
                    return productActions.setListProductos({productos});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    addProducto$: Observable<Action> = this.actions$.pipe(
        ofType(productActions.addProducto),
        switchMap((action) => {
            return this.productoService.addProductoToSocio(action.producto)
            .pipe(
                switchMap((producto) => [
                    productActions.addProductoToList({producto: producto }),
                    mssageActions.newMessage({ text: 'Se guardo correctamente' })
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    updateProducto$: Observable<Action> = this.actions$.pipe(
        ofType(productActions.updateProducto),
        switchMap((action) => {
            return this.productoService.updateProductoToSocio(action.producto)
            .pipe(
                switchMap((producto) => [
                    productActions.updateProductoToList({producto: action.producto}),
                    mssageActions.newMessage({ text: 'Se actulizo correctamente' })
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    delteProducto$: Observable<Action> = this.actions$.pipe(
        ofType(productActions.deleteProducto),
        switchMap((action) => {
            return this.productoService.deleteProducto(action.id)
            .pipe(
                map(() => {
                    return productActions.deleteProductoToList({id: action.id});
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : 'Error al comunicarse con el servidor',
            statusCode: error ? error.status : -1
        };
        /*return switchMap(() => [
            of(productActions.requestFailure({ requestError })),
            of(mssageActions.newMessage({ level: 'error', text: requestError.message }))
        ])*/
        return of(productActions.requestFailure({ requestError }));
    }
}

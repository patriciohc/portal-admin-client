import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as AccountActions from '../actions/account.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import { AccountService } from '../logposeapp-api/account.service';
import {RequestError} from '../models/request-error';


@Injectable()
export class AccountEffects {

  constructor(
    private actions$: Actions,
    private accountService: AccountService
  ) {}

  @Effect()
  loadAccount$: Observable<Action> = this.actions$.pipe(
    ofType(AccountActions.loadAccount),
    switchMap(() => {
      return this.accountService.getAccount()
        .pipe(
          map((account) => {
            return AccountActions.setAccount({account});
          }),
          catchError(this.handlingError)
        );
    })
  );

  @Effect()
  updateUsuario$: Observable<Action> = this.actions$.pipe(
    ofType(AccountActions.updateUsuario),
    switchMap(({usuario}) => {
      return this.accountService.updateUsuario(usuario)
        .pipe(
          map(() => {
            return AccountActions.setLocalDataUsuario({usuario});
          }),
          catchError(this.handlingError)
        );
    })
  );

  private handlingError(error: any) {
    const requestError: RequestError = {
        message: error && error.error ? error.error.message : error.message,
        statusCode: error ? error.status : -1
    };
    return of(AccountActions.loadAccountFailure({requestError}));
}
}

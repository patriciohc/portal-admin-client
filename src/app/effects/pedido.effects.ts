import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as pedidoActions from '../actions/pedido.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {RequestError} from '../models/request-error';
import { AngularFirestore } from '@angular/fire/firestore';
import { Venta } from '../models/venta.model';
import { PedidoService } from '../logposeapp-api/pedido.service';

@Injectable()
export class PedidosEffects {

    constructor(
        private actions$: Actions,
        // private afs: AngularFirestore,
        private pedidoService: PedidoService
    ) {}

    @Effect()
    updateEstatus$: Observable<Action> = this.actions$.pipe(
        ofType(pedidoActions.updateEstatus),
        switchMap(({pedidoId, estatus}) => {
            return this.pedidoService.updateEstatus(pedidoId, estatus)
            .pipe(
                map(() => {
                    return pedidoActions.updateEstatusLocal({pedidoId, estatus});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    loadPedidos$: Observable<Action> = this.actions$.pipe(
        ofType(pedidoActions.loadListPedidos),
        switchMap(({unidadId}) => {
            return this.pedidoService.getListPedidos(unidadId)
            .pipe(
                switchMap((pedidos: Array<Venta>) => {
                    pedidos.forEach((p) => {
                        Venta.convertToModel(p);
                    });
                    return [
                        pedidoActions.setListPedidos({pedidos})
                    ]
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : error.message,
            statusCode: error ? error.status : -1
        };
        return of(pedidoActions.requestFailure({requestError}));
    }

}

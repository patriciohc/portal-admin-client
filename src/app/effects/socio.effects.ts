import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as socioActions from '../actions/socio.actions';
import * as pedidoActions from '../actions/pedido.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import { Socio } from '../models/socio.model';
import {RequestError} from '../models/request-error';
import { GenericResponse } from '../models/generic-response';
import { SocioService } from '../logposeapp-api/socio.service';


@Injectable()
export class SocioEffects {

    constructor(
        private actions$: Actions,
        private socioService: SocioService
    ) {}

    @Effect()
    loadSocio$: Observable<Action> = this.actions$.pipe(
        ofType(socioActions.loadSocio),
        switchMap(() => {
            return this.socioService.getSocio()
            .pipe(
                switchMap((socio) => [
                    socioActions.setSocio({socio, isNew: false})
                ]),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    updateSocio$: Observable<Action> = this.actions$.pipe(
        ofType(socioActions.updateSocio),
        switchMap((action) => {
            return this.socioService.updateSocio(action.socio)
            .pipe(
                map((response) => {
                    return socioActions.updateSocioLocalData({socio: action.socio});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    updateStyles$: Observable<Action> = this.actions$.pipe(
        ofType(socioActions.updateStyles),
        switchMap((action) => {
            return this.socioService.updateStyles(action.styles)
            .pipe(
                map((styles) => {
                    return socioActions.updateLocalStyles({styles});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    createSocio$: Observable<Action> = this.actions$.pipe(
        ofType(socioActions.createSocio),
        switchMap((action) => {
            return this.socioService.createSocio(action.socio)
            .pipe(
                map((response) => {
                    return socioActions.setSocio({socio: action.socio, isNew: true});
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : error.message,
            statusCode: error ? error.status : -1
        };
        return of(socioActions.loadSocioFailure({requestError}));
    }
    
}

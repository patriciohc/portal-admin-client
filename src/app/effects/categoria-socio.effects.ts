import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as categoriaActions from '../actions/categoria-socio.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import { CategoriaSocioService } from '../logposeapp-api/categoria-socio.service';
import {RequestError} from '../models/request-error';


@Injectable()
export class CategoriaSocioEffects {

  constructor(
    private actions$: Actions,
    private service: CategoriaSocioService
  ) {}

  @Effect()
  loadCategoriasSocio$: Observable<Action> = this.actions$.pipe(
    ofType(categoriaActions.loadListCategoriasSocio),
    switchMap(() => {
      return this.service.getListCategorias()
        .pipe(
          map((categorias) => {
            return categoriaActions.setListCategoriasSocio({categorias});
          }),
          catchError((error) => {
            const requestError: RequestError = {
              message: error && error.error ? error.error.message : error.message,
              statusCode: error ? error.status : -1
            };
            return of(categoriaActions.requestFailure({requestError}));
          })
        );
    })
  );
}

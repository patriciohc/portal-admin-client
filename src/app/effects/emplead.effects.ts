import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import { Action } from '@ngrx/store';
import * as empleadosActions from '../actions/empleado.actions';
import {switchMap, map, catchError} from 'rxjs/operators';
import {RequestError} from '../models/request-error';
import { EmpleadosService } from '../logposeapp-api/emplead.service';

@Injectable()
export class EmpleadoEffects {

    constructor(
        private actions$: Actions,
        private empleadosService: EmpleadosService
    ) {}

    @Effect()
    loadListEmpleados$: Observable<Action> = this.actions$.pipe(
        ofType(empleadosActions.loadListEmpleados),
        switchMap(() => {
            return this.empleadosService.getListBySocio()
            .pipe(
                map((empleados) => {
                    return empleadosActions.setListEmpleados({empleados});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    addEmpleado$: Observable<Action> = this.actions$.pipe(
        ofType(empleadosActions.addEmpleado),
        switchMap((action) => {
            return this.empleadosService.addEmpleadoToSocio(action.correo_electronico)
            .pipe(
                map((empleado) => {
                    return empleadosActions.addEmpleadoToList({empleado});
                }),
                catchError(this.handlingError)
            );
        })
    );

    @Effect()
    delteEmpleado$: Observable<Action> = this.actions$.pipe(
        ofType(empleadosActions.deleteEmpleado),
        switchMap((action) => {
            return this.empleadosService.deleteEmpleadoToSocio(action.id)
            .pipe(
                map(() => {
                    return empleadosActions.deleteEmpleadoToList({id: action.id});
                }),
                catchError(this.handlingError)
            );
        })
    );

    private handlingError(error: any) {
        const requestError: RequestError = {
            message: error && error.error ? error.error.message : error.message,
            statusCode: error ? error.status : -1
        };
        return of(empleadosActions.requestEmpleadosFailure({requestError}));
    }
}

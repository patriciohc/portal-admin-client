import { Request } from '../models/request';
import { GenericResponse } from '../models/generic-response';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Unidad } from '../models/unidad.model';
import { Injectable } from '@angular/core';
import { Producto } from './../models/producto.model';

@Injectable({
  providedIn: 'root'
})
export class ProductoService extends BaseService {

  contextPath = 'producto';
  
  constructor (public http: HttpClient) {
    super(http);
  }

  public getListProductosBySocio() {
    return this.get<Array<Producto>>(this.contextPath);
  }

  public addProductoToSocio(producto: Producto) {
    return this.post<Producto>(this.contextPath, {body: producto});
  }

  public updateProductoToSocio(producto: Producto) {
    return this.put<Producto>(this.contextPath, {body: producto, params: producto.id});
  }

  public deleteProducto(id: string) {
    return this.delete<GenericResponse>(this.contextPath, {params: id})
  }

}
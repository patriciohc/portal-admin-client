import { Request } from '../models/request';
import { GenericResponse } from '../models/generic-response';
import { InfoRecargas, Recarga } from '../models/recarga';
import { Venta, TipoVenta } from '../models/venta.model';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
  })
export class VentaService extends BaseService {

    contextPath = 'venta';

    constructor (public http: HttpClient) {
        super(http);
    }

    /***
     * guarda una venta 
     */
    public saveVenta(venta: Venta) {
        return this.post(this.contextPath + '/punto-venta/', {body: venta})
    }
  
    /**
     * regresa los tatales vendidos por dia en un rago de fechas
     * @param unidadId id de unidad
     * @param init fecha de inicio
     * @param fin fecha de fin
     */
    public getTotalSales(unidadId: string, init: string, fin: string): Observable<Array<any>> {
        let request: Request = {
            query: {unidadId, init, fin}
        }
        return this.get<Array<any>>(this.contextPath + '/totales', request);
    }

    /**
     * regresa resumen de ventas vendias en el dia especificado
     * @param unidadId id de unidad
     * @param fecha fecha
     */
    public async getSaleByDay(unidadId: string, fecha: string, tipo: TipoVenta) {
        let request: Request = {
            query: {unidadId, fecha, tipo}
        }
        return this.get<Array<any>>(this.contextPath + '/ventas-day', request)
            .toPromise();
    }
  
    /**
     * regresa los productos vendidos en el dia especificado
     * @param unidadId id de unidad
     * @param fecha fecha
     */
    public async getProductsByDay(unidadId: string, fecha: string) {
        let request: Request = {
            query: {unidadId, fecha}
        }
        return this.get<Array<any>>(this.contextPath + '/productos-day', request)
            .toPromise();
    }
  
    /**
     * regresa la recargas realizadas en el dia esfecificado
     * @param unidadId id de unidad
     * @param fecha fecha
     */
    public async getRecargasByDay(unidadId: string, fecha: string) {
        let request: Request = {
            query: {unidadId, fecha}
        }
        return this.get<Array<any>>(this.contextPath + '/recargas-day', request)
            .toPromise();
    }
  
    /**
     * regresa la informacion nesesaria para seleccionar alguna recarga
     * @param pin pin que requiere back para desencriptar informacion neseria
     * para realizar peticiones al api del proveedor de servicios de recargas
     */
    public async getInfoRecargas(pin: string): Promise<Array<InfoRecargas>|{}> {
        let request: Request = {
            body: {pin}
        }
        return this.post<Array<InfoRecargas>>(this.contextPath + '/recarga-electronica/info', request)
            .toPromise();
    }
  
    /**
     * realiza una recarga
     * @param recarga informacion de la recarga
     */
    public makeRecarga(recarga: Recarga): Observable<GenericResponse|{}> {
        let request: Request = {
            body: recarga
        }
        return this.post<GenericResponse>(this.contextPath + '/recarga-electronica/', request);
    }

    public async findById(id: string) {
        return this.get<Venta>(this.contextPath, {params: id}).toPromise();
    }

}
import { Routes } from '../models/paths';


  // static OPERADOR = {
  //   login:            'login-operador/',
  //   passwordRecovery: 'operador/password-recovery/',
  //   passwordReset:    'operador/password-reset/',
  // }

export const routesEntyties: Routes = {
  Usuario: {
    basePath: 'usuario'
  },

  Socio: {
    basePath: 'socio'
  },

  Unidad: {
    basePath: 'unidad'
  },

  Imagen: {
    basePath: 'imagen'
  },

  Venta: {
    basePath: 'venta'
  },

  CategoriaProducto: {
    basePath: 'categoria-producto'
  },

  Producto: {
    basePath: 'producto'
  },

  VentaEnSitio: {
    basePath: 'venta'
  },

}

  export const API_USUARIO = {
    toString: () => 'usuario',
    refreshToken: 'refresh-token-not-expired',
    logout: 'account/logout',
    login: 'account/tokenrefresh',
    account: 'account',
    confirmEmail: 'confirmar-email',
    passwordRecovery: 'account/recuperar-contrasena',
    passwordReset:    'account/cambiar-contrasena',
  }

  export const API_SOCIO = {
    toString: () => 'socio'
  }

  export const API_CLIENTE = {
    producto:   'cliente/producto/',
    unidad:     'cliente/unidad/',
    categoria:  'cliente/categoria'
  }

  export const API_UNIDAD = {
    toString: () => 'unidad/',
    producto:       'unidad/producto/',
  }

  export const API_IMAGEN = {
    toString: () => 'image/',
    info: 'image/info/',
    signedRequest: 'image/signed-request/'
  }

  export const API_VENTA = {
    toString: () => 'venta/',
  }

  export const CATEGORIA = {
    toString: () => 'categoria-producto/',
    // getListCategories:'categoria/',
    // registerCategory: 'categoria/',
    // getCategory:      'categoria/',
    // updateCategory:   'categoria/',
  }

  export const API = {
    getListUnidad:    'cliente/unidad/',
    registerUnidad:   'unidad/',
    addPosition:      'unidad-posicion/',
    addPolygon:       'unidad-poligono/',
    getUnidadPolygon: 'unidad-poligono/',
    // getUnidadProducts:'unidad-producto/',
    // addUnidadProducts:'unidad-producto/',
    updateUnidad:     'unidad/',
    deleteUnidad:     'unidad/',
    getImages:        'cliente/',
    // signedS3:         'image/',
    uploadImageS3:    '',
    // saveInfoImage:    'image/info/',
    // getListProducts:  'cliente/producto/',
    registerProduct:  'producto/',
    getProduct:       'producto/detalle/',
    updateProduct:    'producto/',
    deleteProduct:    'producto/',

    getListPedidos:   'pedido/',
    setStatusPedidos: 'pedido-estatus/',
    getDataGraph:     'pedido-grafica-semana/',
    getListOperadores:'cliente/operador/',
    registerOperador: 'operador/',
    addOperador:      'cliente/operador',
    deleteOperadorFromClient: 'cliente/operador/',
    getOperador:      'operador/',
    updateOperador:   'cliente/operador/',
    asignarRepartidor:'pedido-repartidor/',
    siginSucursal:    'login-unidad-operador/',
    getListUnidadesOperador: 'list-unidad-operador/',
    subscribeNotification: 'subscribe/',
    sendNotification:  'send-notification/',
    getListCategoriaUnidad: 'categoria-unidad/',
    getListRoles:      'roles/',
  }
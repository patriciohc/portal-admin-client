import { Request } from '../models/request';
import { GenericResponse } from '../models/generic-response';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SocioSettings } from './../models/socio-settings.model';

@Injectable({
    providedIn: 'root'
})
export class SocioSettingService extends BaseService {

  contextPath = 'sttings';
  
  constructor (public http: HttpClient) {
    super(http);
  }

  public getSettings(socioId: string) {
    const request: Request = {
      query: {socioId}
    }
    return this.get<Array<SocioSettings>>(this.contextPath, request);
  }

  public updateSettings(id: string, settings: SocioSettings) {
    let request: Request = {
        params: id,
        body: settings
    }
    return this.put<GenericResponse>(this.contextPath, request);
  }

  public createSettings(settings: SocioSettings) {
    let request: Request = {
        body: settings
    }
    return this.post<SocioSettings>(this.contextPath, request);
  }

}
import { Request } from '../models/request';
import { GenericResponse } from '../models/generic-response';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Unidad } from '../models/unidad.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UnidadService extends BaseService {

  contextPath = 'unidad';
  
  constructor (public http: HttpClient) {
    super(http);
  }

  public addUnidadToSocio(unidad: Unidad) {
    return this.post<Unidad>(this.contextPath, {body: unidad});
  }

  public updateUnidad(unidad: Unidad) {
    let id = unidad.id;
    let request = {
      params: id,
      body: {
        ...unidad,
        id: undefined
      }
    }
    return this.put<GenericResponse>(this.contextPath, request);
  }

  public deleteUnidadToSocio(id: string) {
    return this.delete<GenericResponse>(this.contextPath, {params: id});
  }

  public getListUnidadesBySocio() {
    return this.get<Array<Unidad>>(this.contextPath)
  }

  public addProductsToUnidad(idUnidad: string, list: Array<{id: string}>) {
    let request: Request = {
        query: {all: false},
        body: list
    }
    return this.put<Array<{id: string}>>(this.contextPath + '/' + idUnidad + '/productos', request)
  }

  public addAllProductsToUnidad(idUnidad: string) {
    let request: Request = {
        query: {all: true}
    }
    return this.put<Array<{id: string}>>(this.contextPath + '/'+ idUnidad + '/productos', request)
  }

  public removeProductFromUnidad(idUnidad: string, idProducto: string) {
    return this.delete<GenericResponse>(this.contextPath + '/'+ idUnidad + '/productos/' + idProducto, {})
  }

  public isActivo(idUnidad: string) {
    return this.get<GenericResponse>(this.contextPath + '/'+ idUnidad + '/isActivo/', {})
  }

  public activar(idUnidad: string) {
    return this.put<GenericResponse>(this.contextPath + '/'+ idUnidad + '/activar/', {})
  }

  public desactivar(idUnidad: string) {
    return this.put<GenericResponse>(this.contextPath + '/'+ idUnidad + '/desactivar/', {})
  }

}
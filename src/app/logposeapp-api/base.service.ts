import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs/';
import { makeUrl } from './api-utils';
// import { environment as env } from '../../environments/environment';
import { Request } from '../models/request';

// const BASE_URL = env.logposeApiConfig.api

export class BaseService {

    constructor(public http: HttpClient) {}

    public get<T>(path: string, params: Request = {}): Observable<T> {
        let url = makeUrl(path, params);
        return this.http.get<T>(url)
            .pipe(catchError(this.handleError(path)));
    }

    public post<T>(path: string, params: Request): Observable<T> {
        let url = makeUrl(path, params);
        return this.http.post<T>(url, params.body)
            .pipe(catchError(this.handleError(path)));
    }

    public put<T>(path: string, params: Request) {
        let url = makeUrl(path, params);
        return this.http.put<T>(url, params.body)
            .pipe(catchError(this.handleError(path)));
    }

    public delete<T>(path: string, params: Request): Observable<T> {
        let url = makeUrl(path, params);
        return this.http.delete<T>(url)
            .pipe(catchError(this.handleError(path)));
    }

    public rawPost<T>(url: string, body: any): Observable<T> {
        return this.http.post<T>(url, body)
            .pipe(catchError(this.handleError(url)));
    }

    public rawGet<T>(url: string): Observable<T> {
        return this.http.get<T>(url)
            .pipe(catchError(this.handleError(url)));
    }

    handleError(operation = 'operation') {
        return (error: any) => {
            console.error(`[API] ${operation}: `, error);
            return throwError(error);
        }
    }
}





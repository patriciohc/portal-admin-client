import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Account } from './../models/account';
import { GenericResponse } from './../models/generic-response';
import { Usuario } from './../models/usuario';

@Injectable({
    providedIn: 'root'
})
export class AccountService extends BaseService {

    constructor(public http: HttpClient) {
        super(http);
    }

    getAccount() {
        return this.get<Account>('account');
    }

    updateUsuario(usuario: Usuario) {
        return this.put<GenericResponse>('usuario', {body: usuario});
    }

}
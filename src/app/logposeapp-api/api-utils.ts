import { environment as env } from '../../environments/environment';
import { Request } from '../models/request';

/**
 * make string, query params example:
 * @param params - { param1: value1, param2:value2 }
 * @returns - "param1=value1&param2=value2"
 */
export function makeQueryParams (params: Object): string {
  var pares = []; 
  for (let key in params) {
    if (params[key] != undefined) {
      pares.push(key + '=' + params[key]);
    }
  }
  if (pares.length > 0) {
    return pares.join('&');
  } else {
    return '';
  }
}

/**
 * make final url for request
 * @param endPoint
 * @param params
 */
export function makeUrl (endPoint: string, params: Request): string {
  let url = env.logposeApiConfig.api + endPoint;
  url += params.params ? '/' + params.params + '/' : '';
  url += params.query ? '?' + makeQueryParams(params.query): '';
  return url;
}
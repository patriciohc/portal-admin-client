import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Empleado } from '../models/empleado.model';

@Injectable({
    providedIn: 'root'
})
export class EmpleadosService extends BaseService {

    constructor(public http: HttpClient) {
        super(http);
    }

    getListBySocio() {
        return this.get<Array<Empleado>>('socio/empleado');
    }

    addEmpleadoToSocio(correo_electronico: string) {
        return this.put<Empleado>('socio/empleado', {body: {correo_electronico}});
    }

    deleteEmpleadoToSocio(id: string) {
        return this.delete<Empleado>('socio/empleado', {params: id});
    }

}
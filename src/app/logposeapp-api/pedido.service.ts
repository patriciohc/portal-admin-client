import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppStore } from './../reducers/index';
import { Store } from '@ngrx/store';
import { Empleado } from './../models/empleado.model';
import { Venta, EstatusVenta } from './../models/venta.model';
import { Request } from '../models/request';

@Injectable({
  providedIn: 'root'
})
export class PedidoService extends BaseService {

  socioId: string;

  contextPath = 'venta';
  
  constructor (
      public http: HttpClient,
      private afs: AngularFirestore,
      private store: Store<AppStore>,
    ) {
    super(http);
    this.store.select('socio').subscribe(socioState => {
      if (socioState.socio) this.socioId = socioState.socio.id;
    });
  }

  public updateEstatus(pedidoId: string, estatus: EstatusVenta) {
    return this.put(this.contextPath + '/pedidos/' + pedidoId + '/estatus/' + estatus, {})
    // const url = 'socios/'+ this.socioId + '/pedidos/';
    // return this.afs.collection(url).doc(pedidoId).update({estatus})
  }

  public asignarRepartidor(pedidoId: string, empleado: Empleado) {
    const url = 'socios/'+ this.socioId + '/pedidos/';
    return this.afs.collection(url).doc(pedidoId).update({
      estatus: 2,
      repartidor_asignado: empleado
    })
  }

  public cerrarPedido(venta: Venta) {
    const urlFire = 'socios/'+ this.socioId + '/pedidos/';
    this.afs.collection(urlFire).doc(venta.id).update({estatus: 3})
    this.post('venta', {body: venta})
  }

  public getListPedidos(unidadId: string) {
      const request: Request = {
          query: {unidadId}
      }
      return this.get(this.contextPath+ '/pedidos/', request)        
  }

  public getTotalPedidos() {
    return this.get(this.contextPath + '/total-pedidos/', {})        
}

}
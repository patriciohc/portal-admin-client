import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs/';
import { makeUrl } from './api-utils';
import { CATEGORIA } from './api-routes';
import { environment as env } from '../../environments/environment';
import { Request} from '../models/request';
import {Response} from '../models/models';

// import { CategoriaProducto } from '../models/categoriaProducto.model';

const BASE_URL = env.logposeApiConfig.api

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient) {
  }

  public get<T>(path: string, params: Request): Observable<T|Array<T>> {
    let url = makeUrl(path, params);
    return this.http.get<T|Array<T>>(url)
    .pipe(catchError(this.handleError(path)));
  }
   
  public post<T>(path: string, params: Request): Observable<{}|T> {
    let url = makeUrl(path, params);
    return this.http.post<T>(url, params.body)
    .pipe(catchError(this.handleError(path)));
  }

  public put<T>(path: string, params: Request) {
    let url = makeUrl(path, params);
    return this.http.put<T>(url, params.body)
    .pipe(catchError(this.handleError(path)));
  }

  public delete<T>(path: string, params: Request): Observable<T> {
    let url = makeUrl(path, params);
    return this.http.delete<T>(url)
    .pipe(catchError(this.handleError(path)));
  }


  public rawPost<T>(url: string, body: any): Observable<{}|T> {
    return this.http.post<T>(url, body)
    .pipe(catchError(this.handleError(url)));
  }

  //- ==================
  //- SERVICES USUARIO
  //- ==================
  // public loginCliente(correo_electronico: string, password: string) {
  //   return this.http.post<Response>(SERVER + 'cliente/login', {correo_electronico, password})
  //   .pipe(map((response: any) => {
  //     if (response.code == 'SUCCESS') this.sesion.saveUser(response.data);
  //     return response;
  //   }))
  //   .pipe(catchError(this.handleError('loginCliente')));
  // }

  //- ==================
  //- SERVICES UNIDAD
  //- ==================
  // public getListUnidad () {
  //   return this.http.get<Response>(SERVER + 'cliente/unidad/?id_cliente='+this.sesion.sesion.id_cliente)
  //   .pipe(catchError(this.handleError('getListUnidad', [])));
  // }

  // public registerUnidad (unidad) {
  //   return this.http.post<Response>(SERVER + 'unidad/', unidad)
  //   .pipe(catchError(this.handleError('registerUnidad', [])));
  // }

  public addPosition (idUnidad, position) {
    return this.http.post<number>(BASE_URL + 'unidad-posicion/?id_unidad='+idUnidad, position)
    .pipe(catchError(this.handleError('registerUnidad')));
  }

  public addPolygon (idUnidad, polygon) {
    return this.http.post<number>(BASE_URL + 'unidad-poligono/?id_unidad='+idUnidad, polygon)
    .pipe(catchError(this.handleError('registerUnidad')));
  }

  // public getUnidad (idUnidad) {
  //   return this.http.get<Response>(SERVER + 'unidad/'+idUnidad)
  //   .pipe(catchError(this.handleError('getUnidad', [])));
  // }

  public getUnidadPolygon (idUnidad) {
    return this.http.get<Response>(BASE_URL + 'unidad-poligono/?id_unidad='+idUnidad)
    .pipe(catchError(this.handleError('getUnidad')));
  }

  // public getUnidadProducts (idUnidad) {
  //   return this.http.get<Response>(SERVER + 'unidad-producto/?id_unidad='+idUnidad)
  //   .pipe(catchError(this.handleError('getUnidadProducts', [])));
  // }

  // public addUnidadProducts (products) {
  //   return this.http.post<any>(SERVER + 'unidad-producto/', products)
  //   .pipe(catchError(this.handleError('addUnidadProducts', [])));
  // }

  public updateUnidad (idUnidad, unidad) {
    return this.http.put<any>(BASE_URL + 'unidad/?id_unidad='+idUnidad, unidad)
    .pipe(catchError(this.handleError('updateUnidad')));
  }

  public deleteUnidad (idUnidad) {
    return this.http.delete<any>(BASE_URL + 'unidad/?id_unidad='+idUnidad)
    .pipe(catchError(this.handleError('deleteUnidad')));
  }

  //- ==================
  //- SERVICES IMAGEN
  //- ==================
  public getImages() {
    return this.http.get<any>(BASE_URL + 'cliente/image/')
    .pipe(catchError(this.handleError('getImages')));
  }

  // public signedS3(typeImage) {
  //   return this.http.get<Response>(SERVER + 'image/signed-request/?type_image='+typeImage)
  //   .pipe(catchError(this.handleError('signedS3', [])));
  // }

  public uploadImageS3(url, file) {
    return this.http.put<any>(url, file)
    .pipe(catchError(this.handleError('uploadFile')));
  }

  // public saveInfoImage(info) {
  //   return this.http.post<Response>(SERVER + 'image/info/', info)
  //   .pipe(catchError(this.handleError('saveInfoImage', [])));
  // }

  //- ==================
  //- SERVICES PRODUCTOS
  //- ==================
  // public getListProducts () {
  //   return this.http.get<Response>(SERVER + 'cliente/producto/')
  //   .pipe(catchError(this.handleError('getListUnidad', [])));
  // }

  public registerProduct (p) {
    return this.http.post<any>(BASE_URL + 'producto/', p)
    .pipe(catchError(this.handleError('registerUnidad')));
  }

  public getProduct (idProducto) {
    return this.http.get<Response>(BASE_URL + 'producto/detalle/?id_producto='+idProducto)
    .pipe(catchError(this.handleError('getProducto')));
  }

  public updateProduct (idProduct, p) {
    return this.http.put<any>(BASE_URL + 'producto/?id_producto='+idProduct, p)
    .pipe(catchError(this.handleError('updateProduct')));
  }

  public deleteProduct (id) {
    return this.http.delete<any>(BASE_URL + 'producto/?id_producto='+id)
    .pipe(catchError(this.handleError('deleteProduct')));
  }

  //- ==================
  //- CATEGORIAS DENTRO DE UNA UNIDAD
  //- ==================
  // public getListCategories () {
  //   return this.http.get<Response>(SERVER + 'categoria/?id_cliente='+this.sesion.sesion.id_cliente)
  //   .pipe(catchError(this.handleError('getListCategories')));
  // }

  // public registerCategory (cat: CategoriaProducto) {
  //   return this.http.post<CategoriaProducto>(BASE_URL + CATEGORIA, cat )
  //   .pipe(catchError(this.handleError('registerCategory')));
  // }

  public getCategory (id) {
    return this.http.get<Response>(BASE_URL + 'categoria/'+id)
    .pipe(catchError(this.handleError('getProducto' )));
  }

  public updateCategory (id, p) {
    return this.http.put<any>(BASE_URL + 'categoria/?id_categoria='+id, p )
    .pipe(catchError(this.handleError('updateProduct')));
  }

  //- ==================
  //- SERVICES PEDIDOS
  //- ==================
  public getListPedidos (idUnidad) {
    return this.http.get<any>(BASE_URL + 'pedido/?id_unidad='+idUnidad)
    .pipe(catchError(this.handleError('updateProduct')));
  }

  // public setStatusPedidos (idUnidad, estatus) {
  //   return this.http.put<any>(BASE_URL + 'pedido-estatus/?id='+idUnidad, {estatus})
  //   .pipe(catchError(this.handleError('setStatuPedidos')));
  // }

  // public getDataGraph (date = null, idUnidad = null) {
  //   date = date || new Date();
  //   idUnidad = idUnidad || this.sesion.sesion.id_unidad;
  //   return this.http.get<any>(SERVER + 'pedido-grafica-semana/?id_unidad='+1)
  //   .pipe(catchError(this.handleError('getDataGraph')));
  // }

  //- ==================
  //- OPERADOR
  //- ==================
  public getListOperadores () {
    return this.http.get<any>(BASE_URL + 'cliente/operador/')
    .pipe(catchError(this.handleError('updateProduct')));
  }

  public addOperador (correo_electronico: string, id_unidad:number, rol:number) {
    return this.http.post<any>(BASE_URL + 'cliente/operador/', {correo_electronico, id_unidad, rol})
    .pipe(catchError(this.handleError('addOperador')));
  }

  public deleteOperadorFromClient (id) {
    return this.http.delete<any>(BASE_URL + 'cliente/operador/?id_operador='+id)
    .pipe(catchError(this.handleError('siginSucursal')));
  }

  public getOperador (id) {
    return this.http.get<Response>(BASE_URL + 'operador/?id='+id)
    .pipe(catchError(this.handleError('getProducto')));
  }

  public updateOperador (id, obj) {
    return this.http.put<any>(BASE_URL + 'cliente/operador/?id_operador='+id, obj)
    .pipe(catchError(this.handleError('updateOperador')));
  }

  public asignarRepartidor (id_pedido, id_repartidor) {
    return this.http.put<any>(BASE_URL + 'pedido-repartidor/?id_pedido='+id_pedido, {id_repartidor})
    .pipe(catchError(this.handleError('asignarRepartidor')));
  }

  // public siginSucursal (idSucursal) {
  //   return this.http.get<any>(SERVER + 'login-unidad-operador/?id_unidad='+idSucursal)
  //   .pipe(map((response: any) => {
  //     if (response.code == 'SUCCESS') this.sesion.saveUser(response.data);
  //     return response;
  //   }))
  //   .pipe(catchError(this.handleError('siginSucursal')));
  // }

/*
* obtiene la lista de unidades a la que esta asigando el operador
*/
  public getListUnidadesOperador () {
    return this.http.get<any>(BASE_URL + 'list-unidad-operador/')
    .pipe(catchError(this.handleError('updateProduct')));
  }

  //- ==================
  //- NOTIFCACIONES
  //- ==================
  public subscribeNotification (idDevice) {
    return this.http.post<any>(BASE_URL + 'subscribe/', {id_device: idDevice})
    .pipe(catchError(this.handleError('suscribeNotification')));
  }

  public sendNotification (data) {
    return this.http.post<Response>(BASE_URL + 'send-notification/', data)
    .pipe(catchError(this.handleError('sendNotification')));
  }

  //- ==================
  //- CATEGORIA A LA QUE PERTENECE UNA UNIDAD
  //- ==================
  // public getListCategoriaUnidad () {
  //   return this.http.get<any>(BASE_URL + 'categoria-unidad/')
  //   .pipe(catchError(this.handleError('getListCategoria')));
  // }

  //- ==================
  //- OTROS
  //- ==================
  public getListRoles () {
    return this.http.get<any>(BASE_URL + 'roles/')
    .pipe(catchError(this.handleError('getListRoles')));
  }


  //- ==================
  //- HANDLE ERROR
  //- ==================
  handleError (operation = 'operation') {
    return (error: any) => {
      console.error(`[api.services] ${operation}: `);
      console.error(error);
      return throwError(error);
    }
  }
}





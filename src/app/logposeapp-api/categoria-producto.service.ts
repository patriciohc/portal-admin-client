import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { CategoriaProducto } from './../models/categoria-producto.model';
import { GenericResponse } from './../models/generic-response';

@Injectable({
    providedIn: 'root'
})
export class CategoriaProductoService extends BaseService {

    contextPath = 'categoria-producto/';

    constructor(public http: HttpClient) {
        super(http);
    }

    getListCategorias() {
        return this.get<Array<CategoriaProducto>>(this.contextPath);
    }

    delteCategoria(id: string) {
        return this.delete<GenericResponse>(this.contextPath, {params: id});
    }

    addCategoria(categoria: CategoriaProducto) {
        return this.post<CategoriaProducto>(this.contextPath, {body: categoria});
      }
    
    updateCategoria(categoria: CategoriaProducto) {
        return this.put<CategoriaProducto>(this.contextPath, {body: categoria, params: categoria.id});
      }

}
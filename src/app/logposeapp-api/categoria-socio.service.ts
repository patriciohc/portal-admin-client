import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { CategoriaSocio } from './../models/categoria-socio.model';

@Injectable({
    providedIn: 'root'
})
export class CategoriaSocioService extends BaseService {

    constructor(public http: HttpClient) {
        super(http);
    }

    getListCategorias() {
        return this.get<Array<CategoriaSocio>>('categoria-socio');
    }

}
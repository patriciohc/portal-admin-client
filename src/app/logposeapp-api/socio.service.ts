import { Request } from '../models/request';
import { GenericResponse } from '../models/generic-response';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Socio, SocioStyles } from '../models/socio.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SocioService extends BaseService {

  contextPath = 'socio';
  
  constructor (public http: HttpClient) {
    super(http);
  }

  public getSocio() {
    return this.get<Socio>(this.contextPath);
  }

  public updateSocio(socio: Socio) {
    let request: Request = {
        body: socio
    }
    return this.put<GenericResponse>(this.contextPath, request);
  }

  public updateStyles(styles: SocioStyles) {
    let request: Request = {
        body: styles
    }
    return this.put<SocioStyles>(this.contextPath + '/styles', request);
  }

  public createSocio(socio: Socio) {
    let request: Request = {
        body: socio
    }
    return this.post<Socio>(this.contextPath, request);
  }

}
import { Empleado } from '../models/empleado.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as empleadosActions from '../actions/empleado.actions';
import {RequestError} from '../models/request-error';

export interface EmpleadosState {
  isLoading: boolean;
  requestError: RequestError;
  empleados: Array<Empleado>;
}

const initialState: EmpleadosState = {
  isLoading: false,
  requestError: null,
  empleados: []
};

const _empleadosReducer = createReducer(
  initialState,
  on(empleadosActions.loadListEmpleados, (state) => ({
      ...state, isLoading: true
    })
  ),
  on(empleadosActions.addEmpleado, (state) => ({
      ...state, isLoading: true
    })
  ),
  on(empleadosActions.deleteEmpleado, (state) => ({
      ...state, isLoading: true
    })
  ),
  on(empleadosActions.setListEmpleados, (state, {empleados})  => ({
      ...state, isLoading: false, empleados
    })
  ),
  on(empleadosActions.requestEmpleadosFailure, (state, {requestError})  => ({
      ...state, isLoading: false, requestError
    })
  ),
  on(empleadosActions.addEmpleadoToList, (state, {empleado})  => ({
      requestError: null, isLoading: false, empleados: [...state.empleados, empleado]
    })
  ),
  on(empleadosActions.deleteEmpleadoToList, (state, {id})  => ({
    requestError: null, isLoading: false, empleados: state.empleados.filter(item => item.id != id)
  })
)
);

export function empleadosReducer(state: EmpleadosState | undefined, action: Action) {
    return _empleadosReducer(state, action);
}

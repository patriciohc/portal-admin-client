import { Action, createReducer, on } from '@ngrx/store';
import * as accountActions from '../actions/ticket.actions';
import { RequestError } from '../models/request-error';
import { Venta } from './../models/venta.model';

export interface TicketState {
    ticket: Venta;
}

const initialState: TicketState = {
    ticket: null
};

const _ticketReducer = createReducer(
    initialState,
    on(accountActions.newTicket, (state: TicketState, { venta }) => {
        return {...state, ticket: venta};
    }),
    on(accountActions.clearTicket, (state: TicketState) => {
        return { ...state, requestError: null, isLoading: true };
    })
);

export function ticketReducer(state: TicketState | undefined, action: Action) {
    return _ticketReducer(state, action);
}

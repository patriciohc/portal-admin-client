import { Action, createReducer, on } from '@ngrx/store';
import * as unidadesActions from '../actions/unidad.actions';
import { RequestError } from '../models/request-error';
import { Unidad } from '../models/unidad.model';

export interface UnidadesState {
    isLoading: boolean;
    requestError: RequestError;
    unidades: Array<Unidad>;
    newUnidad: Unidad;
}

const initialState: UnidadesState = {
    isLoading: false,
    requestError: null,
    unidades: [],
    newUnidad: null
};

const _unidadesReducer = createReducer(
    initialState,
    // request http
    on(unidadesActions.loadListUnidades, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.addUnidad, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.updateUnidad, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.deleteUnidad, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.addProductsToUnidad, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.addAllProductsToUnidad, (state) => ({
        ...state, isLoading: true
    })),
    on(unidadesActions.removeProductFromUnidad, (state) => ({
        ...state, isLoading: true
    })),
    // local action
    on(unidadesActions.setListUnidades, (state, { unidades }) => ({
        ...state, isLoading: false, unidades
    })),
    on(unidadesActions.updateLocalUnidad, (state, { unidad }) => {
        const unidades = state.unidades.map(item => item.id != unidad.id ? item : unidad)
        return {...state, isLoading: false, unidades}
    }),
    on(unidadesActions.localUpdateProductsUnidad, (state, { id, list }) => {
        const unidades = state.unidades.map(unidad => {
            if (unidad.id != id) { 
                return unidad;
            } else {
                return { ...unidad, productos: list }
            }
        });
        
        return {...state, isLoading: false, unidades}
    }),
    on(unidadesActions.localRemoveProductsUnidad, (state, { idUnidad, idProducto }) => {
        let unidades = state.unidades.map(unidad => {
            if (unidad.id != idUnidad) { 
                return unidad;
            } else {
                return { 
                    ...unidad,
                    productos: unidad.productos.filter(item => item.id != idProducto)
                }
            }
        });
        
        return {...state, isLoading: false, unidades}
    }),
    on(unidadesActions.addUnidadToList, (state, { unidad }) => ({
        ...state, requestError: null, isLoading: false, unidades: [...state.unidades, unidad], newUnidad: unidad
    })),
    on(unidadesActions.deleteUnidadToList, (state, { id }) => ({
        ...state, requestError: null, isLoading: false, unidades: state.unidades.filter(item => item.id != id)
    })),
    on(unidadesActions.requestFailure, (state, { requestError }) => ({
        ...state, isLoading: false, requestError
    })),
    on(unidadesActions.cleanNewUniad, (state) => ({
        ...state, newUnidad: null
    }))
);

export function unidadesReducer(state: UnidadesState | undefined, action: Action) {
    return _unidadesReducer(state, action);
}

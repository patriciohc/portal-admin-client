import { Action, createReducer, on } from '@ngrx/store';
import * as categoriaProductoActions from '../actions/categoria-producto.actions';
import {RequestError} from '../models/request-error';
import { CategoriaProducto } from '../models/categoria-producto.model';

export interface CategoriaProdcutoState {
  isLoading: boolean;
  requestError: RequestError;
  categorias: Array<CategoriaProducto>;
}

const initialState: CategoriaProdcutoState = {
  isLoading: false,
  requestError: null,
  categorias: []
};

const _categoriaProductoReducer = createReducer(
  initialState,
  on(categoriaProductoActions.loadListCategoriaProducto, (state: CategoriaProdcutoState) => {
    return {...state, isLoading: true};
  }),
  on(categoriaProductoActions.addNewCategoriaProducto, (state: CategoriaProdcutoState) => {
    return {...state, isLoading: true};
  }),
  on(categoriaProductoActions.setListCategoriaProducto, (state: CategoriaProdcutoState, {categorias}) => {
    return {categorias, requestError: null, isLoading: false};
  }),
  on(categoriaProductoActions.addLocalNewCategoriaProducto, (state: CategoriaProdcutoState, {categoria}) => {
    return {...state, categorias: [...state.categorias, categoria]};
  }),
  on(categoriaProductoActions.removeLocalCategoriaProducto, (state: CategoriaProdcutoState, {id}) => {
    return {...state, categorias: state.categorias.filter(item => item.id != id)};
  }),
  on(categoriaProductoActions.updateLocalCategoriaProducto, (state: CategoriaProdcutoState, {categoria}) => {
    return {...state, isLoading: false, categorias: state.categorias.map(item => item.id != categoria.id ? item : categoria)};
  }),
  on(categoriaProductoActions.requestFailure, (state: CategoriaProdcutoState, {requestError}) => {
    return {...state, isLoading: false, requestError};
  }),
  on(categoriaProductoActions.cleanError, (state: CategoriaProdcutoState) => {
    return {...state, isLoading: false, requestError: null};
  })
);

export function categoriaProductoReducer(state: CategoriaProdcutoState | undefined, action: Action) {
  return _categoriaProductoReducer(state, action);
}

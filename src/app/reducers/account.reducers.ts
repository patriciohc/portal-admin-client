import { Action, createReducer, on } from '@ngrx/store';
import * as accountActions from '../actions/account.actions';
import {Account} from '../models/account';
import {RequestError} from '../models/request-error';

export interface AccountState {
  isLoading: boolean;
  requestError: RequestError;
  account: Account;
}

const initialState: AccountState = {
  isLoading: false,
  requestError: null,
  account: null
};

const _accountReducer = createReducer(
  initialState,
  on(accountActions.setAccount, (state: AccountState, {account}) => {
    return {account, requestError: null, isLoading: false};
  }),
  on(accountActions.updateUsuario, (state: AccountState) => {
    return {...state, requestError: null, isLoading: true};
  }),
  on(accountActions.setLocalDataUsuario, (state: AccountState, {usuario}) => {
    const account = {...state.account, usuario}
    return {requestError: null, isLoading: false, account};
  }),
  on(accountActions.loadAccount, (state: AccountState) => {
    return {...state, isLoading: true};
  }),
  on(accountActions.loadAccountFailure, (state: AccountState, {requestError}) => {
    return {...state, isLoading: false, requestError};
  }),
  on(accountActions.cleanAccount, () => {
    return {...initialState};
  })
);

export function accountReducer(state: AccountState | undefined, action: Action) {
  return _accountReducer(state, action);
}

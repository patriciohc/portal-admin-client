import { Action, createReducer, on } from '@ngrx/store';
import * as messageActions from '../actions/message.actions';

export interface MessageState {
    level: string,
    text: string;
}

const initialState: MessageState = {
    level: null,
    text: null
};

const _messageReducer = createReducer(
    initialState,
    on(messageActions.newMessage, (state, {level, text}) => ({
        level, text
    })),
    on(messageActions.resetMessage, (state) => initialState)
);

export function messageReducer(state: MessageState | undefined, action: Action) {
    return _messageReducer(state, action);
}

import { Socio } from '../models/socio.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as socioActions from '../actions/socio.actions';
import {RequestError} from '../models/request-error';

export interface SocioState {
  isLoading: boolean;
  requestError: RequestError;
  socio: Socio;
  isNew: boolean;
}

const initialState: SocioState = {
  isLoading: false,
  requestError: null,
  socio: null,
  isNew: false
};

const _socioReducer = createReducer(
  initialState,
  on(socioActions.setSocio, (state, {socio, isNew}) => ({...state, isLoading: false, socio, isNew})),
  on(socioActions.loadSocio, state  => ({...state, isLoading: true})),
  on(socioActions.createSocio, state  => ({...state, isLoading: true })),
  on(socioActions.loadSocioFailure, (state, {requestError})  => ({...state, isLoading: false, requestError })),
  on(socioActions.updateSocio, state  => ({...state, isLoading: true, isNew: false })),
  on(socioActions.cleanSocio, state  => initialState),
  on(socioActions.updateStyles, state  => ({...state, isLoading: true})),
  on(socioActions.updateLocalStyles, (state, {styles})  => {
    const socio: Socio = {...state.socio, styles}
    return {...state, isLoading: false, socio};
  }),
  on(socioActions.updateSocioLocalData, (state, {socio})  => {
    const socioResult: Socio = {...state.socio, ...socio}
    return {...state, isLoading: false, socioResult, isNew: false};
  }),
  
);

export function socioReducer(state: SocioState | undefined, action: Action) {
    return _socioReducer(state, action);
}

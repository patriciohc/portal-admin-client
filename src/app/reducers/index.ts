import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {socioReducer, SocioState} from './socio.reducers';
import {accountReducer, AccountState} from './account.reducers';
import { TokenState, tokenReducer } from './token.reducers';
import { EmpleadosState, empleadosReducer } from './empleado.reducers';
import { UnidadesState, unidadesReducer } from './unidad.reducers';
import { productosReducer, ProdcutosState } from './producto.reducers';
import { MessageState, messageReducer } from './message.reducers';
import { CategoriaSocioState, categoriaSocioReducer } from './categoria-socio.reducers';
import { PedidosState, pedidosReducer } from './pedido.reducers';
import { CategoriaProdcutoState, categoriaProductoReducer } from './categoria-productos.reducers';
import { VentaState, ventaReducer } from './venta.reducers';
import { TicketState, ticketReducer } from './ticket.reducers';

export interface AppStore {
  'socio': SocioState;
  'token': TokenState;
  'account': AccountState;
  'empleados': EmpleadosState;
  'unidades': UnidadesState;
  'productos': ProdcutosState;
  'messages': MessageState;
  'categoriasSocio': CategoriaSocioState;
  'pedidos': PedidosState;
  'categoriasProducto': CategoriaProdcutoState;
  'ventas': VentaState;
  'ticket': TicketState;
}

export const reducers: ActionReducerMap<AppStore> = {
  'socio': socioReducer,
  'token': tokenReducer,
  'account': accountReducer,
  'empleados': empleadosReducer,
  'unidades': unidadesReducer,
  'productos': productosReducer,
  'messages': messageReducer,
  'categoriasSocio': categoriaSocioReducer,
  'pedidos': pedidosReducer,
  'categoriasProducto': categoriaProductoReducer,
  'ventas': ventaReducer,
  'ticket': ticketReducer
};

export const metaReducers: MetaReducer<AppStore>[] = !environment.production ? [] : [];

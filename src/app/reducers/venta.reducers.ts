import { Action, createReducer, on } from '@ngrx/store';
import * as ventaActions from '../actions/venta.actions';
import {RequestError} from '../models/request-error';
import { Venta } from './../models/venta.model';
import { ProductoVenta } from './../models/producto-venta';

export interface VentaState {
  isLoading: boolean; // esperando respuesta de la api
  requestError: RequestError; // errro al ejecutar http service
  ventaDetails: Venta;
  newVenta: Venta; // venta en mostrador
  reporteVentas: any;
  statusRecargaRequest: boolean,
}

const initialState: VentaState = {
  isLoading: false,
  requestError: null,
  ventaDetails: null,
  reporteVentas: null,
  newVenta: null,
  statusRecargaRequest: null
};

const _ventaReducer = createReducer(
  initialState,
  on(ventaActions.detailVenta, (state: VentaState) => {
    return {...state, isLoading: true};
  }),
  on(ventaActions.makeRecarga, (state: VentaState) => {
    return {...state, isLoading: true};
  }),
  on(ventaActions.setStatusRecargaRequest, (state: VentaState, {statusRecargaRequest}) => {
    return {...state, isLoading: false, statusRecargaRequest};
  }),
  on(ventaActions.clearDataRecarga, (state: VentaState) => {
    return {...state, isLoading: false, statusRecargaRequest: null};
  }),
  on(ventaActions.setReportVentas, (state: VentaState, {reporteVentas}) => {
    return {...state, isLoading: false, reporteVentas};
  }),
  // crea nueva venta de mostrador
  on(ventaActions.newLocalVenta, (state: VentaState, {unidadId}) => {
    const venta = new Venta(unidadId);
    return {...state, newVenta: venta};
  }),
  // agrega producto a la venta
  on(ventaActions.addProductoToVenta, (state: VentaState, {producto, cantidad}) => {
    let newVenta = JSON.parse(JSON.stringify(state.newVenta));
    let lista_productos = newVenta.lista_productos;
    const existent = lista_productos
      .find((item: ProductoVenta) => item.producto.id == producto.id);
    if (existent) {
        existent.cantidad += cantidad;
        existent.total = existent.producto.precio_publico * existent.cantidad;
        existent.iva = existent.total * producto.iva;
        existent.subtotal = existent.total - existent.iva;
    } else {
        const total = producto.precio_publico * cantidad;
        const iva = total * producto.iva;
        const subtotal = total - iva;
        newVenta.lista_productos.push({
            producto: producto,
            cantidad: cantidad,
            subtotal,
            total,
            iva,
            descuento: 0
        });
    }
    const totales = Venta.calculates(lista_productos)
    newVenta = {...newVenta, ...totales}
    return {...state, newVenta};
  }),
  // elimina producto de la venta
  on(ventaActions.removeProductToVenta, (state: VentaState, {productoId}) => {
    let newVenta = JSON.parse(JSON.stringify(state.newVenta))
    newVenta.lista_productos = newVenta.lista_productos.filter(item => item.producto.id != productoId);
    return {...state, newVenta};
  }),
  on(ventaActions.requestFailure, (state, { requestError }) => ({
    ...state, isLoading: false, requestError
  })),
  on(ventaActions.okRequestFailure, (state) => ({
    ...state, isLoading: false, requestError: null
  }))
);

export function ventaReducer(state: VentaState | undefined, action: Action) {
  return _ventaReducer(state, action);
}

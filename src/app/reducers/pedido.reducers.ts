import { Action, createReducer, on } from '@ngrx/store';
import * as pedidoActions from '../actions/pedido.actions';
import { RequestError } from '../models/request-error';
import { Venta } from './../models/venta.model';

export interface PedidosState {
    isLoading: boolean;
    requestError: RequestError;
    pedidos: Array<Venta>;
}

const initialState: PedidosState = {
    isLoading: false,
    requestError: null,
    pedidos: []
};

const _pedidosReducer = createReducer(
    initialState,
    on(pedidoActions.setListPedidos, (state, {pedidos}) => ({
         ...state, isLoading: false, pedidos
    })),
    on(pedidoActions.loadListPedidos, (state) => {
        return {...state, isLoading: true}
    }),
    on(pedidoActions.updateEstatusLocal, (state, {pedidoId, estatus}) => {
        const pedidos = state.pedidos.map(item => {
            if (item.id == pedidoId) {
                return {...item, estatus: estatus}
            } else {
                return item;
            }
        });
        return {...state, pedidos};
      }),
      on(pedidoActions.requestFailure, (state, { requestError }) => ({
        ...state, isLoading: false, requestError
    })),
);

export function pedidosReducer(state: PedidosState | undefined, action: Action) {
     return _pedidosReducer(state, action);
}

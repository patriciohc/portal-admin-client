import { Action, createReducer, on } from '@ngrx/store';
import * as refreshTokenActions from '../actions/token.actions';
import {RequestError} from '../models/request-error';


export interface TokenState {
  isLoading: boolean;
  requestError: RequestError;
  token: string;
  refreshToken: string;
}

const initialState: TokenState = {
  isLoading: false,
  requestError: null,
  token: null,
  refreshToken: null
}

/*
function getStateFromStorage(): RefreshTokenState {
  let token = localStorage.getItem(KEY_REFRESH_TOKEN);
  let typeAccount = localStorage.getItem(KEY_TYPE_ACCOUNT) as TypeAccount;
  if (token && typeAccount) {
    const refreshToken = {token, keepSession: true, typeAccount};
    return {isLoading: false, requestError: null, refreshToken};
  } else {
    token = sessionStorage.getItem(KEY_REFRESH_TOKEN);
    typeAccount = sessionStorage.getItem(KEY_TYPE_ACCOUNT) as TypeAccount;
    const refreshToken = {token: token, keepSession: false, typeAccount: typeAccount};
    return {isLoading: false, requestError: null, refreshToken};
  }
}
*/

/*
function removeFromLocalStorage() {
  localStorage.removeItem(KEY_REFRESH_TOKEN);
  localStorage.removeItem(KEY_TYPE_ACCOUNT);

  sessionStorage.removeItem(KEY_REFRESH_TOKEN);
  sessionStorage.removeItem(KEY_TYPE_ACCOUNT);
}
*/

const _tokenReducer = createReducer(
  initialState,
  on(refreshTokenActions.setRefreshToken, (state: TokenState, {refreshToken}) => {
    return {...state, refreshToken, isLoading: false, requestError: null};
  }),
  on(refreshTokenActions.loadRefreshToken, (state: TokenState) => {
    return {...state, isLoading: true};
  }),
  on(refreshTokenActions.setToken, (state: TokenState, {token}) => {
    return {...state, token, isLoading: false, requestError: null};
  }),
  on(refreshTokenActions.loadTokenFailure, (state: TokenState, {requestError}) => {
    return {...state, isLoading: false, requestError};
  }),
  on(refreshTokenActions.cleanToken, (state) => {
    return initialState;
  })
);

export function tokenReducer(state: TokenState | undefined, action: Action) {
  return _tokenReducer(state, action);
}

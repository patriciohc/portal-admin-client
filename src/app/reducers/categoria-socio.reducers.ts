import { Action, createReducer, on } from '@ngrx/store';
import * as categoriaActions from '../actions/categoria-socio.actions';
import {RequestError} from '../models/request-error';
import { CategoriaSocio } from './../models/categoria-socio.model';

export interface CategoriaSocioState {
  isLoading: boolean;
  requestError: RequestError;
  categorias: Array<CategoriaSocio>;
}

const initialState: CategoriaSocioState = {
  isLoading: false,
  requestError: null,
  categorias: []
};

const _categoriaSocioReducer = createReducer(
  initialState,
  on(categoriaActions.setListCategoriasSocio, (state: CategoriaSocioState, {categorias}) => {
    return {categorias, requestError: null, isLoading: false};
  }),
  on(categoriaActions.loadListCategoriasSocio, (state: CategoriaSocioState) => {
    return {...state, isLoading: true};
  }),
  on(categoriaActions.requestFailure, (state: CategoriaSocioState, {requestError}) => {
    return {...state, isLoading: false, requestError};
  })
);

export function categoriaSocioReducer(state: CategoriaSocioState | undefined, action: Action) {
  return _categoriaSocioReducer(state, action);
}

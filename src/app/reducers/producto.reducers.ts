import { Action, createReducer, on } from '@ngrx/store';
import * as productActions from '../actions/producto.actions';
import { RequestError } from '../models/request-error';
import { Producto } from './../models/producto.model';

export interface ProdcutosState {
    isLoading: boolean;
    requestError: RequestError;
    productos: Array<Producto>;
    newProducto: Producto;
}

const initialState: ProdcutosState = {
    isLoading: false,
    requestError: null,
    productos: [],
    newProducto: null
};

const _productoReducer = createReducer(
    initialState,
    on(productActions.loadListProductos, (state) => ({
        ...state, isLoading: true
    })),
    on(productActions.addProducto, (state) => ({
        ...state, isLoading: true
    })),
    on(productActions.deleteProducto, (state) => ({
        ...state, isLoading: true
    })),
    on(productActions.setListProductos, (state, { productos }) => ({
        ...state, isLoading: false, productos
    })),
    on(productActions.addProductoToList, (state, { producto }) => ({
        ...state, requestError: null, isLoading: false, productos: [...state.productos, producto], newProducto: producto
    })),
    on(productActions.updateProductoToList, (state, { producto }) => ({
        ...state, requestError: null, isLoading: false, newProducto: null, productos: state.productos.map(item => item.id != producto.id ? item : producto)
    })),
    on(productActions.deleteProductoToList, (state, { id }) => ({
        ...state, requestError: null, isLoading: false, productos: state.productos.filter(item => item.id != id)
    })),
    on(productActions.requestFailure, (state, { requestError }) => ({
        ...state, isLoading: false, requestError
    }))
    
);

export function productosReducer(state: ProdcutosState | undefined, action: Action) {
    return _productoReducer(state, action);
}

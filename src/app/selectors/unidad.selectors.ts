import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { UnidadesState } from './../reducers/unidad.reducers';


export const selectUnidadesState = (state: AppStore) => state.unidades;

export const selectUnidades = createSelector(
    selectUnidadesState,
    (state: UnidadesState) => state.unidades
);

export const selectUnidad = createSelector(
    selectUnidadesState,
    (state: UnidadesState, id: string) => state.unidades.find(unidad => unidad.id == id)
);

export const selectNewUnidad = createSelector(
    selectUnidadesState,
    (state: UnidadesState) => state.newUnidad
);

export const selectUnidadesLoading = createSelector(
    selectUnidadesState,
    (state: UnidadesState) => state.isLoading
);

export const selectError = createSelector(
    selectUnidadesState,
    (state: UnidadesState) => state.requestError
);

import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { VentaState } from '../reducers/venta.reducers';


export const selectVentasState = (state: AppStore) => state.ventas;

export const selectVentaDetails = createSelector(
    selectVentasState,
    (state: VentaState) => state.ventaDetails
);

export const selectReporteVentas = createSelector(
    selectVentasState,
    (state: VentaState) => state.reporteVentas
);

export const selectNewVenta = createSelector(
    selectVentasState,
    (state: VentaState) => state.newVenta
);

export const selectProductosNewVenta = createSelector(
    selectVentasState,
    (state: VentaState) => state.newVenta.lista_productos
);

export const selectStatusRecargaRequest = createSelector(
    selectVentasState,
    (state: VentaState) => state.statusRecargaRequest
);

export const selectIsLoaging = createSelector(
    selectVentasState,
    (state: VentaState) => state.isLoading
);

export const selectRequestError = createSelector(
    selectVentasState,
    (state: VentaState) => state.requestError
);

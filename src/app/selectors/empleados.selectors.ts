import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { EmpleadosState } from '../reducers/empleado.reducers';


export const selectEmpleadosState = (state: AppStore) => state.empleados;

export const selectEmpleados = createSelector(
    selectEmpleadosState,
    (state: EmpleadosState) => state.empleados
);

export const selectEmpleadosIsLoading = createSelector(
    selectEmpleadosState,
    (state: EmpleadosState) => state.isLoading
);

export const selectLoadEmpleadosError = createSelector(
    selectEmpleadosState,
    (state: EmpleadosState) => state.requestError
);

import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { ProdcutosState } from './../reducers/producto.reducers';


export const selectProdcutosState = (state: AppStore) => state.productos;

export const selectProductos = createSelector(
    selectProdcutosState,
    (state: ProdcutosState) => state.productos
);

export const selectProducto = createSelector(
    selectProdcutosState,
    (state: ProdcutosState, id: string) => state.productos.find(item => item.id == id)
);

export const selectNewProduto = createSelector(
    selectProdcutosState,
    (state: ProdcutosState) => state.newProducto
);

export const selectProductosLoading = createSelector(
    selectProdcutosState,
    (state: ProdcutosState) => state.isLoading
);

export const selectError = createSelector(
    selectProdcutosState,
    (state: ProdcutosState) => state.requestError
);

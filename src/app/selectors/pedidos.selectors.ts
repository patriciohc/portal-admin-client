import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { PedidosState } from '../reducers/pedido.reducers';


export const selectPedidosState = (state: AppStore) => state.pedidos;

export const selectPedidos = createSelector(
    selectPedidosState,
    (state: PedidosState) => state.pedidos
);

export const selectPedidosLoading = createSelector(
    selectPedidosState,
    (state: PedidosState) => state.isLoading
);


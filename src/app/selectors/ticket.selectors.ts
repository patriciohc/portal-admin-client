import { createSelector } from '@ngrx/store';
import { selectSocioState } from './socio.selectors';
import { selectUnidadesState } from './unidad.selectors';
import { AppStore } from '../reducers';
import { TicketState } from '../reducers/ticket.reducers';
import { SocioState } from '../reducers/socio.reducers';
import { UnidadesState } from '../reducers/unidad.reducers';
import { DataTicketPrint } from '../models/ticket.model';

export const selectTicketState = (state: AppStore) => state.ticket;

export const selectDataTicketPrint = createSelector(
    selectSocioState,
    selectUnidadesState,
    selectTicketState,
    (socio: SocioState, unidades: UnidadesState, ticket: TicketState): TicketState => {
        if (socio && unidades && ticket) {
            return ticket;
        }
    }
);
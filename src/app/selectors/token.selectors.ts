import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { TokenState } from '../reducers/token.reducers';

export const selectRefreshTokenState = (state: AppStore) => state.token;

export const selectRefreshToken = createSelector(
  selectRefreshTokenState,
  (state: TokenState) => state.refreshToken
);

export const selectToken = createSelector(
  selectRefreshTokenState,
  (state: TokenState) => state.token
);

export const selectRefreshTokenIsLoading = createSelector(
  selectRefreshTokenState,
  (state: TokenState) => state.isLoading
);

export const selectRefreshTokenError = createSelector(
  selectRefreshTokenState,
  (state: TokenState) => state.requestError
);

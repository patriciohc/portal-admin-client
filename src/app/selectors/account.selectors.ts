import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import {AccountState} from '../reducers/account.reducers';
import { Account } from '../models/account';


export const selectAccountState = (state: AppStore) => state.account;

export const selectAccount = createSelector(
  selectAccountState,
  (state: AccountState) => state.account
);

export const selectUsuario = createSelector(
  selectAccountState,
  (state: AccountState) => state.account ? state.account.usuario : null 
);

export const selectAccountIsLoading = createSelector(
  selectAccountState,
  (state: AccountState) => state.isLoading
);

export const selectAccountError = createSelector(
  selectAccountState,
  (state: AccountState) => state.requestError
);

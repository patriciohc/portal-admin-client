import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import { CategoriaSocioState } from './../reducers/categoria-socio.reducers';


export const selectCategoriasSocioState = (state: AppStore) => state.categoriasSocio;

export const selectCategoriasSocio = createSelector(
    selectCategoriasSocioState,
  (state: CategoriaSocioState) => state.categorias
);

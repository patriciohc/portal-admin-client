import {AppStore} from '../reducers';
import {createSelector} from '@ngrx/store';
import {SocioState} from '../reducers/socio.reducers';


export const selectSocioState = (state: AppStore) => state.socio;

export const selectSocio = createSelector(
  selectSocioState,
  (state: SocioState) => state.socio
);

export const selectSocioLoading = createSelector(
  selectSocioState,
  (state: SocioState) => state.isLoading
);

export const selectSocioIsNew = createSelector(
  selectSocioState,
  (state: SocioState) => state.isNew
);

export const selectLoadSocioError = createSelector(
  selectSocioState,
  (state: SocioState) => state.requestError
);

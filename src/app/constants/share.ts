 /**
  * key for to save refresh token in localstorage
  */
 export const KEY_REFRESH_TOKEN = 'logposeapp.adminclient.refreshtoken';

 /**
  * key for to save type account in localstorage
  */
 export const KEY_TYPE_ACCOUNT = 'logposeapp.adminclient.typeaccount';

 /**
  * ket for to save port for printer service
  */
 export const KEY_PRINTER_SERVICE_PORT = 'logposeapp.printer.serviceport';

 /**
  * path for logo 
  */
 export const PATH_LOGO = '/socio';

 export const FILE_LOGO = PATH_LOGO + '/logo.jpg';

 /**
  * path for products
  */
 export const PATH_PRODUCTS = '/socio/productos';

 /**
  * path for categories
  */
 export const PATH_CATEGORIAS = '/socio/categorias'

 /**
  * path for PATH_PROMOCIONAL images
  */
 export const PATH_PROMOCIONAL = '/socio/promocional'

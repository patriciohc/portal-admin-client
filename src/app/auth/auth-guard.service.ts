import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Account } from '../models/account';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../reducers';
import { selectAccount } from '../selectors/account.selectors';
import { selectToken } from '../selectors/token.selectors';

@Injectable()
export class AuthGuardService implements CanActivate  {

  isAuthenticated: boolean;
  account: Account;

  constructor(
    public router: Router,
    private store: Store<AppStore>
  ) {
    this.store.pipe(select(selectToken))
      .subscribe(token => {
        this.isAuthenticated = token !== null;
      });

    this.store.pipe(select(selectAccount))
      .subscribe(account => {
        this.account = account;
      });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const roles = route.data['roles'] as Array<string>;
    if (!this.isAuthenticated) {
      this.router.navigate(['login']);
      return false;
    } else if (!roles) {
      return true;
    } else {
      const rol: any = this.account ? this.account.perfil : '';
      if (roles.includes(rol)) {
        return true;
      } else {
        this.router.navigate(['pedido']);
        return false;
      }
    }
  }

}

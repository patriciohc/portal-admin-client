import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../reducers';
import { selectToken } from '../selectors/token.selectors';


@Injectable()
export class HttpInterceptorService implements HttpInterceptor  {

  private token: string = null;

  constructor (private store: Store<AppStore>) {
    this.store.pipe(select(selectToken))
      .subscribe(token => {
        this.token = token;
      });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = new URL(request.url);
    if (url.host === 'media0-delivery.s3.us-east-2.amazonaws.com') {
      console.log('request amazon s3');
      const headers = request.headers;
      // headers.set('Content-Type', );
      headers.set('Content-Type', 'x-amz-acl');
      headers.set('Content-Type', 'public-read');
      request = request.clone({ headers });
      return next.handle(request);
    } else {
      const auth = request.headers.get('Authorization');
      if (this.token && !request.headers.get('Authorization')) {
        request = request.clone({
          headers: request.headers.set('Authorization', 'Bearer ' + this.token)
        });
      }
      return next.handle(request);
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { OneSignalService } from './services/onesignal.service';
import { LoadingService } from './services/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Store, select } from '@ngrx/store';
import { loadSocio } from './actions/socio.actions';
import { loadListEmpleados } from './actions/empleado.actions';
import { AppStore } from './reducers';
import { selectRefreshToken, selectToken } from './selectors/token.selectors';
import { selectAccount, selectAccountIsLoading } from './selectors/account.selectors';
import { selectDataTicketPrint } from './selectors/ticket.selectors';
import * as accountActions from './actions/account.actions';
import * as tokenActions from './actions/token.actions';
import { Account, PERFIL_SOCIO } from './models/account';
import { AngularFireAuth } from '@angular/fire/auth'
import { loadListUnidades } from './actions/unidad.actions';
import { loadListProductos } from './actions/producto.actions';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { loadListCategoriasSocio } from './actions/categoria-socio.actions';
import { loadListCategoriaProducto } from './actions/categoria-producto.actions';
import { TicketState } from './reducers/ticket.reducers';
import { MatDialog } from '@angular/material/dialog';
import { ModalTicketComponent } from './components/modal-ticket/modal-ticket.component';
import { timer, Observable, concat } from 'rxjs';
import { addEventListenerVisibility } from './utils/utils';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    title = 'app';
    textLoading: string;
    timerSource = timer(1000 * 60, 1000 * 60);
    lastTokenUpdate: number;

    constructor(
        private oneSignal: OneSignalService,
        private loading: LoadingService,
        private ngxSpinner: NgxSpinnerService,
        private store: Store<AppStore>,
        private afauth: AngularFireAuth,
        private snackBar: MatSnackBar,
        private dialog: MatDialog,
    ) {
        this.initServiceWorker();
        this.initLoading();
        this.oneSignal.initOneSignal();

        this.store.dispatch(loadListCategoriasSocio());

        /**
         * listen avtive tab browser
         */
        addEventListenerVisibility((visible: boolean) => {
            if (visible) {
                this.checkToken();
            }
        });

        /** 
        * Refresca el token para no tomar un token caducado 
        */
        this.timerSource.subscribe(() => {
            this.checkToken();
        });


        /**
         * escucha cuando se a autenticado a firebase y carga el token a redux.
         * este token es el que se utiliza para la peticiones a la api de logpose
         */
        this.afauth.authState.subscribe(async user => {
            if (user) {
                const token = await this.afauth.auth.currentUser.getIdToken();
                this.lastTokenUpdate = Date.now();
                this.store.dispatch(tokenActions.setToken({ token }));
            }
        })

        /**
         * escucha cuando se actuliza el refresh token que viene de la api de logpose,
         * y se autentica a firebase
         */
        this.store.pipe(select(selectRefreshToken))
            .subscribe(async (refreshToken: string) => {
                if (refreshToken) {
                    try {
                        await this.afauth.auth.signInWithCustomToken(refreshToken);
                        const token = await this.afauth.auth.currentUser.getIdToken();
                        this.lastTokenUpdate = Date.now();
                        this.store.dispatch(tokenActions.setToken({ token }));
                    } catch (error) {
                        var requestError = { statusCode: -1, message: '' };
                        this.store.dispatch(tokenActions.loadTokenFailure({ requestError }))
                    }
                }
            });

        /**
         * cada ves que se actualiza el token(utlizado para peticiones a la api de logpose)
         * se recarga la cuenta
         */
        this.store.pipe(select(selectToken))
            .subscribe(async (token: string) => {
                if (token) {
                    setTimeout(() => {
                        this.store.dispatch(accountActions.loadAccount());
                    }, 100);
                }
            });

        this.store.pipe(select(selectAccount))
            .subscribe((account: Account) => {
                if (account) {
                    if (account.accountIsCreated) {
                        this.store.dispatch(loadListProductos());
                        if (account.perfil === PERFIL_SOCIO) {
                            this.store.dispatch(loadSocio());
                            this.store.dispatch(loadListEmpleados());
                            this.store.dispatch(loadListUnidades());
                            this.store.dispatch(loadListCategoriaProducto());
                            this.oneSignal.updateUserId();
                        }
                    }
                }
            });

        this.store.select('messages').subscribe(({ level, text }) => {
            if (text) {
                let config = new MatSnackBarConfig();
                config.verticalPosition = 'top';
                config.horizontalPosition = 'end';
                config.duration = 2000;
                this.snackBar.open(text, 'Cerrar', config);
            }
        });

        this.store.pipe(select(selectDataTicketPrint))
            .subscribe((ticketState: TicketState) => {
                if (ticketState && ticketState.ticket) {
                    console.log('nuevo ticket ', ticketState);
                    this.dialog.open(ModalTicketComponent, {
                        width: '360px',
                        data: ticketState.ticket
                    });
                }
            });

    }

    ngOnInit() {
        this.store.pipe(select(selectAccountIsLoading)).subscribe(isLoading => {
            if (isLoading) {
                this.loading.show();
            } else {
                this.loading.hide();
            }
        });
    }

    initLoading() {
        this.loading.eventLoading.subscribe(data => {
            this.textLoading = data ? data.text: 'Cargando...';
            if (data.show) {
                this.ngxSpinner.show();
            } else {
                this.ngxSpinner.hide();
            }
        });
    }

    private async checkToken() {
        if (this.lastTokenUpdate) {
            let now = Date.now();
            if ((now - this.lastTokenUpdate) > 500000) {
                console.log('update token', Date());
                const token = await this.afauth.auth.currentUser.getIdToken();
                this.lastTokenUpdate = Date.now();
                this.store.dispatch(tokenActions.setToken({ token }));
            }
        }
    }

    private initServiceWorker() {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/service-worker.js')
                .then(function (register) {
                    console.log('Service Worker Registered');
                });
        }
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRecargasComponent } from './modal-recargas.component';

describe('ModalRecargasComponent', () => {
  let component: ModalRecargasComponent;
  let fixture: ComponentFixture<ModalRecargasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRecargasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRecargasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { InfoRecargas, Recarga } from './../../models/recarga';
import { Unidad } from '../../models/unidad.model';
import { Socio } from '../../models/socio.model';
import { LoadingService } from './../../services/loading.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { AppStore } from './../../reducers/index';
import { select, Store } from '@ngrx/store';
import { selectSocio } from '../../selectors/socio.selectors';
import { VentaService } from '../../logposeapp-api/venta.service';
import { makeRecarga } from '../../actions/venta.actions';
import { VentaState } from './../../reducers/venta.reducers';
import { selectStatusRecargaRequest } from '../../selectors/venta.selectors';
import { selectIsLoaging } from './../../selectors/venta.selectors';

class Data {
  infoRecargas: Array<InfoRecargas>;
  unidad: Unidad;
}

@Component({
  selector: 'app-modal-recargas',
  templateUrl: './modal-recargas.component.html',
  styleUrls: ['./modal-recargas.component.css']
})
export class ModalRecargasComponent implements OnInit {

  nameCompanySelected: string;
  companySelected: InfoRecargas;
  montoCodigoSelected: string;
  message: string;
  telefono1: string;
  telefono2: string;
  pin: string;
  infoRecargas: Array<InfoRecargas>;

  private unidad: Unidad;
  private socio$: Observable<Socio>;
  private statusRecarga$: Observable<boolean>;
  private ventaIsloading$: Observable<boolean>;

  private socio: Socio;

  constructor(
    private ventaService: VentaService,
    private loading: LoadingService,
    private store: Store<AppStore>,
    public dialogRef: MatDialogRef<ModalRecargasComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Data
  ) {
    this.infoRecargas = data.infoRecargas;
    this.unidad = data.unidad;
    this.socio$ = this.store.pipe(select(selectSocio));
    this.statusRecarga$ = this.store.pipe(select(selectStatusRecargaRequest))
    this.ventaIsloading$ = this.store.pipe(select(selectIsLoaging));
    this.montoCodigoSelected = '';
    
    this.socio$.subscribe(socio => {
      if (socio) this.socio = socio;
    });

    this.statusRecarga$.subscribe(status => {
      if (status === true) {
        this.dialogRef.close();
      }
    });

    this.ventaIsloading$.subscribe(isLoading => {
      if (isLoading) {
        this.loading.show();
      } else {
        this.loading.hide();
      }
    });
  }

  ngOnInit() { }

  onChangeCompany() {
    this.companySelected = this.infoRecargas
      .find(item => item.compania == this.nameCompanySelected);
    this.montoCodigoSelected = '';
    if (!this.companySelected) {
      this.nameCompanySelected = '';
    }
  }

  hacerRecarga() {
    if (!this.companySelected) {
      this.message = 'Seleccione una compania';
    } else if (!this.telefono1 || !this.telefono2 || !this.montoCodigoSelected || !this.pin) {
      this.message = 'Llene todos los campos solicitados';
    } else if (!/^[0-9]{10}$/.test(this.telefono1)) {
      this.message = 'El numero telefonico es incorrecto'; 
    } else if (this.telefono1 != this.telefono2) {
      this.message = 'La confirmacion del telefono es incorrecta';
    } else {
      this.message = '';
      this.sendRequest();
    }
  }

  private async sendRequest() {
    let recarga = this.buildObjectRecarga();
    this.store.dispatch(makeRecarga({ recarga }));
    try {
      

    } catch (error) {
      console.error(error);
      this.dialogRef.close({success: false, message: 'Error desconocido'});
    } finally {
      // this.loading.hide();
    }
  }

  private getDataPrint(data: any) {
    data.fecha = moment().format('YYYY-MM-DD');
    return {
      socio: {razon_social: this.socio.razon_social},
      unidad: {direccion: this.unidad.direccion, telefono: this.unidad.telefono},
      data: data
    }
  }

  private buildObjectRecarga (): Recarga {
    let recarga = new Recarga();
    recarga.compania = this.companySelected.compania;
    recarga.telefono = this.telefono1;
    recarga.pin = this.pin;
    let serviceSelected = this.companySelected.servicios
      .find(item => {
        let producto = this.containsProduct(item.productos, this.montoCodigoSelected);
        if (producto != undefined) {
          recarga.producto = producto;
          return true;
        } else {
          return false;
        }
      });
    recarga.is_paquete = serviceSelected.is_paquete;
    recarga.unidad_id = this.unidad.id;
    return recarga;
  }

  private containsProduct(
    list: Array<{nombre: string, montoCodigo: string}>, montoCodigo:string
  ) {
    return list.find(item => item.montoCodigo == montoCodigo);
  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSelectUnidadComponent } from './modal-select-unidad.component';

describe('ModalSelectUnidadComponent', () => {
  let component: ModalSelectUnidadComponent;
  let fixture: ComponentFixture<ModalSelectUnidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSelectUnidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSelectUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

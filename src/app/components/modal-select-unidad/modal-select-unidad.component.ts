import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Unidad } from '../../models/unidad.model';

@Component({
  selector: 'app-modal-select-unidad',
  templateUrl: './modal-select-unidad.component.html',
  styleUrls: ['./modal-select-unidad.component.css']
})
export class ModalSelectUnidadComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    list: Array<Unidad>
    route: string;
  }) { }

  ngOnInit() {
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PrinterService } from '../../services/printer.service';
import { DataTicketPrint } from '../../models/ticket.model';
import { Venta, TipoVenta } from './../../models/venta.model';
import { Observable } from 'rxjs';
import { Socio } from '../../models/socio.model';
import { AppStore } from './../../reducers/index';
import { Store, select } from '@ngrx/store';
import { selectSocio } from '../../selectors/socio.selectors';
import { TicketProducto } from './../../models/ticket.model';
import { RecargaElectronica } from '../../models/producto-especial.model';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-ticket',
  templateUrl: './modal-ticket.component.html',
  styleUrls: ['./modal-ticket.component.css']
})
export class ModalTicketComponent implements OnInit {

    public isRecarga: boolean;

    public socio$: Observable<Socio>;

    private soicio: Socio;
  
    constructor(
        public dialogRef: MatDialogRef<ModalTicketComponent>,
        @Inject(MAT_DIALOG_DATA) public venta: Venta,
        private printerService: PrinterService,
        private store: Store<AppStore>,
    ) {
        this.isRecarga = venta.tipo == TipoVenta.ESPECIAL;
        this.socio$ = this.store.pipe(select(selectSocio));

        this.socio$.subscribe(socio => this.soicio = socio);
    }
  
    ngOnInit() { }

    async printTicket() {
        let dataTicket = new DataTicketPrint();
        dataTicket.fecha = this.venta.fecha as string;
        dataTicket.no_ticket = this.venta.no_ticket;
        dataTicket.total = this.venta.total;
        dataTicket.socio = {
                razon_social: this.soicio.razon_social
        };
        dataTicket.unidad = {
            direccion: this.venta.unidad.direccion,
            telefono: this.venta.unidad.telefono
        };
        if (this.venta.tipo == TipoVenta.REGULAR) {
            dataTicket.tipo = 'regular';
            dataTicket.items = this.venta.lista_productos.map(item => {
                let prod = new TicketProducto();
                prod.cantidad = item.cantidad;
                prod.total = item.total;
                prod.producto = item.producto.nombre;
                return prod;
            });
        } else if (this.venta.producto_especial.producto.nombre == 'RECARGA_ELECTRONICA') {
            dataTicket.tipo = 'recarga';
            dataTicket.items = this.venta.producto_especial.info as RecargaElectronica;
        }
        try {
            await this.printerService.printerTicket(dataTicket);
        } catch (error) {
            console.log(error);
            Swal.fire('', 'La impresora no esta disponible');
        }
        
    }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialogs',
  templateUrl: './dialogs.component.html',
  styleUrls: ['./dialogs.component.css']
})
export class DialogsComponent implements OnInit {
  public typeModal: string;
  public display: string = 'none';
  private title: string;
  private message: string;
  constructor() { }

  ngOnInit() {
  }

  public showLoading(message: string) {
    this.typeModal = 'iSloading';
    this.display = 'flex';
    this.message = message;
  }

  public showMessage(title: string, message: string) {
    this.typeModal = 'isMessage';
    this.display = 'flex';
    this.title = title;
    this.message = message;
  }

  public close() {
    this.display = 'none';
  }


}

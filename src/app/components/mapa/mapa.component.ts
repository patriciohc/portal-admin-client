/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { Position } from '../../models/position';

import Swal from 'sweetalert2';

declare let google: any;


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  @Output()
  changePosition: EventEmitter<Position> = new EventEmitter<Position>();


  showButtonsEditArea: boolean = false;
  showButtonsEditPosition: boolean = false;
  modoEdicion: boolean = false;

  @ViewChild('mapInicio', {static: true})
  private mapElement: ElementRef;
  @ViewChild("search", {static: true})
  private searchElementRef: ElementRef;
  private map: any;
  private polygon: Array<any> = [];
  private newPolygon: Array<any> = null;
  private polygonMap: any = null;
  private _position: any = null;
  private marker: any = null;

  constructor() { }

  ngOnInit() {
    var self = this;
    let latitude: number = 20.6867936;
    let longitude: number = -100.6286227;
    let zoom: number = 9;

    let latLng = new google.maps.LatLng(latitude, longitude);

    let mapOptions = {
      center: latLng,
      zoom: zoom,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.polygonMap = new google.maps.Polyline({
      path: this.polygon,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2,

    });
    this.polygonMap.setMap(this.map);
    this.initAutocomplete();
    this.initEventsMap();

    this.setPosition(this._position);
    this.addMarker(this._position);
  }

  @Input()
  set position(pos: Position) {
    if (pos && pos.lat && pos.lng) {
      this._position = new google.maps.LatLng(pos.lat, pos.lng);
    }
  }

  get position(): Position {
    return new Position(this._position.lat(), this._position.lng())
  }

  private initAutocomplete() {
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      types: ["geocode"],
      componentRestrictions: {country: 'mx'}
    });
    autocomplete.addListener("place_changed", () => {
      let place: google.maps.places.PlaceResult = autocomplete.getPlace();
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      let latLng = place.geometry.location
      this.setPosition(latLng)
    });
  }

  setPosition (latLng) {
    if (latLng) {
      this.map.panTo(latLng);
      this.map.setZoom(14);
    }
  }

  private initEventsMap() {
    var self = this;
    this.map.addListener('click', function(e) {
      if (self.newPolygon && !self.modoEdicion) {
        self.newPolygon.push({lat: e.latLng.lat(), lng:e.latLng.lng()});
        self.polygonMap.setPath(self.newPolygon);
      } else if (false) {

      }
    });
  }

// eventons area de reparto
  public onClickAreaReparto() {
    this.showButtonsEditArea = true;
    this.showButtonsEditPosition = false;
    this.onClickEditarPoligono();
  }

  public onClickCancelar() {
    this.polygonMap.setPath(this.polygon);
    this.showButtonsEditArea = false;
    this.newPolygon = null;
    this.polygonMap.setEditable(false);
  }

  public onClickAceptar() {
    this.onClickTerminarEdicion();
    this.showButtonsEditArea = false;
    this.newPolygon = null;
  }

  public onClickCerrarPoligno() {
    this.newPolygon.push(this.newPolygon[0]);
    this.polygon = JSON.parse(JSON.stringify(this.newPolygon))
    this.polygonMap.setPath(this.polygon);
    this.newPolygon = null;
  }

  public onClickClearPolygon() {
    this.newPolygon = null;
    this.modoEdicion = false;
    this.polygon = [];
    this.polygonMap.setPath(this.polygon);
    this.polygonMap.setEditable(false);
  }

  public onClickAgregarPoligono() {
    if (!this.newPolygon) {
      this.newPolygon =  JSON.parse(JSON.stringify(this.polygon));
    }
  }

  public onClickEditarPoligono() {
    this.modoEdicion = true;
    this.newPolygon =  JSON.parse(JSON.stringify(this.polygon));
    this.polygonMap.setPath(this.newPolygon);
    this.polygonMap.setEditable(true);
    this.map.addListener('click', function(e) {});
  }

  private onClickTerminarEdicion () {
    this.polygon = [];
    let polygon = this.polygonMap.getPath().getArray();
    for (let i = 0; i < polygon.length; i++) {
      this.polygon.push({lat: polygon[i].lat(), lng: polygon[i].lng()})
    }
    this.polygonMap.setPath(this.polygon);
    this.newPolygon = null;
    this.modoEdicion = false;
    this.polygonMap.setEditable(false);
    this.showButtonsEditArea = false;
    this.newPolygon = null;
  }

  public onClickRestablecerPoligono() {
    this.newPolygon = JSON.parse(JSON.stringify(this.polygon));
    this.polygonMap.setPath(this.newPolygon);
  }

// eventos ubicacion unidad
  public onClickPosition() {
    this.showButtonsEditPosition = true;
    this.showButtonsEditArea = false;
    this.removeMarker();
  }

  private removeMarker() {
    if (this.marker) {
      this.marker.setMap(null);
      this.marker = null;
    }
  }

  public addMarker(position) {
    if (position) {
      this.marker = new google.maps.Marker({
        position: position,
        map: this.map,
        title: 'Ubicacion'
      });
    }
  }

  public onClickAceptarPosition () {
    this.removeMarker();
    this._position = this.map.getCenter();
    this.addMarker(this._position);
    this.showButtonsEditPosition = false;
    this.changePosition.emit(this.position);
  }

  public onClickCancelarPosition () {
    if (this._position) {
      this.addMarker(this._position);
    }
    this.showButtonsEditPosition = false;
  }

  public getData() {
    var invalid = true;
    var position = null;
    /*if (this.position) {
      position = {lat: this.position.lat(), lng: this.position.lng()}
    }*/
    if ((this._position && this.polygon.length > 3) && position) invalid = false;
    return {
      invalid,
      position,
      polygon: this.polygon
    };
  }

  public setData(data) {
    this.polygon = data.polygon;
    if (this.polygon && this.polygon.length) {
      this.polygonMap.setPath(this.polygon);
    }
    // this.marker.setPosition(this.position);
    // this.map.panTo(latLng)
    // this.map.setZoom(18)
  }

  public setCurrentPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        let position = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        this.map.panTo(position);
        this.map.setZoom(18);
      },
      (error) => {
        console.log(error);
        Swal.fire('', 'La geolocalización no es compatible con este navegador');
      });
    } else {
        Swal.fire('', 'La geolocalización no es compatible con este navegador');
    }
  }
}

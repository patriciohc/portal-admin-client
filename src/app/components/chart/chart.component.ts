import { Component, OnInit, Input, ViewChild, OnChanges, Output, EventEmitter } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  @ViewChild('canvas', {static: true}) canvas;
  @Output() clickbar: EventEmitter<any> = new EventEmitter();
  @Input() type: string;

  private chart;
  private config: any;

  constructor() { }

  ngOnInit() {
    this.config = {
        type: this.type || 'bar',
        data: {
          labels: [], // ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'],
          datasets: []
        },
        options: {
          scales: {
            yAxes: [{
              ticks: { beginAtZero:true }
            }]
          },
          events: ['click'],
          onClick: this.callback.bind(this)
        }
      }
      this.chart = new Chart(this.canvas.nativeElement, this.config);
  }

  public reload(labelsChar, datasetsChar) {
    this.config.data.labels = labelsChar;
    this.config.data.datasets = datasetsChar;
    this.chart.update();
  }

  public callback (event, i) {
    var e = i[0];
    if (!e) return;
    var x_value = this.config.data.labels[e._index];
    var y_value = this.config.data.datasets[0].data[e._index];
    this.clickbar.emit({x_value, y_value});
  }

}

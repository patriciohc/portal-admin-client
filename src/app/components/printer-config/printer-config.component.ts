import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PrinterService } from '../../services/printer.service';
import { Store } from '@ngrx/store';
import { AppStore } from '../../reducers';
import * as mssageActions from '../../actions/message.actions';


@Component({
  selector: 'app-printer-config',
  templateUrl: './printer-config.component.html',
  styleUrls: ['./printer-config.component.css']
})
export class PrinterConfigComponent implements OnInit {

  formSetting: FormGroup;
  servicePort: string;
  activeService = false;
  activePrinter = false;
  userConfig: any;
  printerList: Array<any>;
  currentUserConfig: {printerId: string};

  constructor(
    private formBuilder: FormBuilder, 
    private printerService: PrinterService,
    private store: Store<AppStore>,
  ) { 
    this.userConfig = this.formBuilder.group({
      'printerId': ['0', Validators.compose([Validators.required])]
    });
    this.initServicePrinter();
  }

  ngOnInit() { }

  async initServicePrinter() {
    this.servicePort = this.printerService.getPort();
    await this.testConecction();
    this.getPrinterStatus();
    this.getPrinterConfig();
    this.getPrinterList();
  }

  async testConecction () {
    if (this.servicePort != '' && this.servicePort != null && !isNaN(this.servicePort as any)) {
      this.printerService.savePort(this.servicePort);
      this.activeService = await this.printerService.testConecction();
      if (this.activeService) {
        this.sendMessage('Conexion exitosa');
      } else {
        this.sendMessage('Conexion fallida', 'error')
      }
    } else {
      this.sendMessage('Puerto de impresion no valido');
    }
  }

  async getPrinterStatus() {
    if (this.activeService) {
      let response = await this.printerService.getPrinterStatus();
      if (response) this.activePrinter = response.status;
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }
  }

  async getPrinterConfig() {
    if (this.activeService) {
      this.currentUserConfig = await this.printerService.getUserConfig();
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }
  }

  async getPrinterList() {
    if (this.activeService) {
      this.printerList = await this.printerService.getListPrinters();
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }
  }

  async saveConfig() {
    var data = this.userConfig.value;
    if (data.printerId == '0') {
      this.sendMessage('Seleccion una impresora', 'error');
      return;
    }
    if (this.activeService) {
      await this.printerService.saveUserConfig(data);
      this.getPrinterConfig();
      this.initPrinter();
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }
  }

  async testPrinter() {
    if (this.activeService) {
      this.printerService.printerTest();
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }    
  }

  async initPrinter() {
    if (this.activeService) {
      await this.printerService.initPrinter();
      this.getPrinterStatus();
    } else {
      this.sendMessage('La conexion no esta activa', 'error');
    }
  }


  private sendMessage(text: string, level = 'info') {
    this.store.dispatch(mssageActions.newMessage({level: level, text: text}))
  }

}

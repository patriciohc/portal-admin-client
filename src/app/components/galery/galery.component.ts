import { Component, OnInit, Output, EventEmitter, AfterViewInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { v4 as uuid } from 'uuid';

import { convertFileToUrl, getSizeImg } from '../../utils/utils';
import { sleep } from '../../utils/utils';
import echo from '../../utils/lazy-loading';
import { SizeImage } from '../../models/image';
import { ImageEditorComponent, DataImageEditor } from '../image-editor/image-editor.component';
import { ImageService } from '../../services/images.service';
import { LoadingService } from '../../services/loading.service';

import Swal from 'sweetalert2';

const STD_SIZE = {
	width: 680,
	height: 408
};
const ASPECT_RATIO = 0.6;


@Component({
  selector: 'app-galery',
  templateUrl: './galery.component.html',
  styleUrls: ['./galery.component.css']
})
export class GaleryComponent implements OnInit, AfterViewInit {

  // @Output() clickOnItemImage = new EventEmitter<any>();
  @Input() type: string;

  @Output() onSave = new EventEmitter<any>();

  private lastImage: number = 5;
  private _images: Array<any> = [];

  translate: any = 24;
  isLoadingDeleteImg: boolean = false;

  constructor (
    public dialog: MatDialog,
    private imageService: ImageService,
    private loading: LoadingService,
  ) { }

  ngOnInit() {
    echo.init({
      offset: 100,
      throttle: 250,
      unload: false
    });
  }

  async ngAfterViewInit() {
    await sleep(1000)
  }

/* PUBLIC FUNCTION */
  public async uploadImage(event) {
    const files = event.srcElement.files;
    const inputFile = files[0];
    if(inputFile == null) {
      return;
    }

    if (inputFile.size > 3145728) {
      return Swal.fire('', 'seleccione una imagen menor a 3MB');
    }

    var sizeImage: SizeImage = await getSizeImg(inputFile);
    if (sizeImage.width < 600 || sizeImage.height < 360) {
      return Swal.fire('', 'seleccione una imagen de tamaño minimo 600 X 360');
    }
    let source = await convertFileToUrl(inputFile);  

    event.target.value = '';

    const dataEditor: DataImageEditor = {
      source, sizeImage,
      stdSize: STD_SIZE,
      aspectRatio: ASPECT_RATIO,
      finalSize: {width: 600, height: 360}
    }

    const dialogRef = this.dialog.open(ImageEditorComponent, {
      // width: '250px',
      data: dataEditor,
      panelClass: 'editor-modal'
    });

    dialogRef.afterClosed().subscribe(async ({file}) => {
      if (file) {
        try {
          let imgName = uuid() + '.jpg';
          let path = this.imageService.getPathByType(this.type);
          let imgPath = path + '/' + imgName;
          this.loading.show();
          let success = await this.imageService.saveImage(imgPath, file);
          if (success) {
            this.loading.hide();
            this.onSave.emit({imgName});
          } else {
            return Swal.fire('', 'Error al guargar la imagen intente mas tarde');
          }
        } catch (err) {
          this.loading.hide();
          return Swal.fire('', 'Error al guargar la imagen intente mas tarde');
        }
      }
    });
  }

  set images(images) {
    this._images = images;
  }

  get images() {
    return this._images;
  }

/* EVENTS UI */
/*  onClickImage(url) {
    this.clickOnItemImage.emit(url);
  }

  doneUploadImage(image) {
    this.images.push(image);
    this.modal.close();
    this.clickOnItemImage.emit(image.url);
  }
*/
  nextImage() {
    let limit = (this.images.length * 120) - 320;
    this.translate = this.translate <= -limit ? -limit : this.translate - 120;
    echo.debounceOrThrottle();
  }

  backImage() {
    this.translate = this.translate >= 24 ? 24 : this.translate + 120;
    echo.debounceOrThrottle();
  }

  async deleteImg(event, img) {
    event.preventDefault();
    let query = {id: img.id, key: img.key}
    this.isLoadingDeleteImg = true;
    // let response = await this.api.delete(API_IMAGEN, {query}).toPromise();
    // let response = await this.repositoryImage.delete(img.id);
    var index = this._images.indexOf(img);
    if (index > -1) {
      this._images.splice(index, 1);
    }
    this.isLoadingDeleteImg = false;
    echo.debounceOrThrottle();
  }

}

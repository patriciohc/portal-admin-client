import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { Column } from './../../models/table';
import { normaliza } from './../../utils/utils';

export class EmitDataTable {
  keyAction: string;
  row: any;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnChanges {

  @Output() events: EventEmitter<EmitDataTable> = new EventEmitter();
  @Input() columns: Array<Column>;
  @Input() checks: boolean;
  @Input() name: string;

  dataUI: Array<any>;
  private _textSearch: string;
  private _page: number;
  private _pageSize: number;
  private _data: Array<any>;
  private localStorageKey: string;
  // number of pages
  public numberPages: number = 1;
  // data current in pages
  public pageData: Array<any> = [];
  public modeEdition: boolean = true;

  length = 0;
  pageSizeOptions: number[] = [5, 10, 20, 30];

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor() {}

  @Input()
  set data(data: Array<any>) {
    this._data = JSON.parse(JSON.stringify(data));
  }

  get data(): Array<any> {
    return this._data;
  }

  ngOnInit() {
    if (this.name) {
      this.localStorageKey = 'logpose.adminclient.table.' + this.name;
    }
  }

  ngOnChanges() {
    if (!this.data) return;
    this.length = this.data.length;
    if (this.data.length === 0) {
      this.pageData = [];
      this.numberPages = 1;
    } else {
      this.onKeydownSearch('Enter');
    }
  }

  private showDataPage() {
    this.numberPages = this.length / this.pageSize;
    this.page = this.page > this.numberPages ? 0: this.page;
    let init = this.pageSize * this.page;
    if (init > this.dataUI.length) {
      this.pageData = [];
      this.length = 0;
    } else {
      let end = this.pageSize * (this.page + 1);
      this.pageData = this.dataUI.slice(init, end);
      this.length = this.dataUI.length;
    }
  }

  onChangePaginator(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.page = event.pageIndex;
    this.showDataPage();
  }

  onClickAction(keyAction: string, row: any) {
    this.events.emit({keyAction, row});
  }

  checkItem(row: any) {
    row.checked = !row.checked;
  }

  checkAllItem(event) {
    for (let i = 0; i < this.pageData.length; i++) {
      this.pageData[i].checked = event.target.checked
    }
  }

  public getRowsChecked() {
    return this.data.filter(item => !!item.checked)
  }

  public generateHTML(row: any, keys: string|Array<string>, icon: string) {
    icon = icon ? `<i class="material-icons left">${icon}</i>` : '';
    let content: string;
    if (typeof(keys) == 'string') {
      content = this.getProperty(row, keys);
    } else if (Array.isArray(keys)) {
      for (var i = 0; i < keys.length; i++) {
        content += row[keys[i]] + ' ';
      }
    }
    if (content) {
      return '<span class="table-td-icon">' + icon + content + '</span>';
    } else {
      return '';
    }
  }

  getProperty(obj: any, keys: string) {
    var keysSplited = keys.split('.');
    var property = obj;
    if (typeof(property) == 'string') {
      return property;
    }
    for (let i = 0; i < keysSplited.length; i++) {
      let key = keysSplited[i];
      if (property && typeof(property) == 'object') {
        property = property[key];
      }
    }
    return property;
  }

  getTextFromCatalog(row: any, key: string, catalog: Array<{id:string, nombre: string}>) {
    let value = row[key];
    if (value === undefined || value == null) {
      return '';
    }
    const id = typeof(value) == 'object' ? value['id'] : value;
    let categoria = catalog.find(item => item.id == id);
    return categoria ? categoria.nombre : '';
  }

  public range(init: number, end: number) {
    var array = [];
    for (var i = init; i < end; i++) {
      array.push(i)
    }
    return array;
  }


  onKeydownSearch (keyDown: string): void {
    if (!this.textSearch) {
      this.dataUI = this.data;
      this.showDataPage();
    } else {
      if (keyDown == 'Enter') {
        this.dataFilterToDataUI();
      }
    }
  }

  dataFilterToDataUI() {
    if (this.data && this.data.length) {
      let text = normaliza(this.textSearch);
      let data = this.data.filter(item => {
        for (let key in item) {
          const metaData = this.columns.find(item => item.keyReferenceToRow == key);
          if (!metaData) {
            continue;
          }
          if (metaData.type == 'string') {
            if (typeof item[key] == 'string' && this.hasText(item[key], text)) {
              return true;
            }
          } else if (metaData.type == 'catalog') {
            var keyCatalog;
            if (typeof item[key] == 'string') {
              keyCatalog = item[key];
            } else if (typeof item[key] == 'object') {
              keyCatalog = item[key].id;
            }
            const itemCatalog = metaData.dataCatalog.find(item => item.id == keyCatalog);
            if (itemCatalog.nombre && this.hasText(itemCatalog.nombre, text)) {
              return true;
            }
          }
        }
        return false;
      });
      this.dataUI = data;
      this.showDataPage();
    }
  }

  /**
   * verifica si var1 contienen a var 2, var1 debe ser una cadena normalizada
   */
  private hasText(var1: string, var2: string): boolean {
    var1 = normaliza(var1);
    return var1.indexOf(normaliza(var2)) != -1;
  }


  /***** GET AND SET METHODS */

  set textSearch(txt: string) {
    this._textSearch = txt;
    if (this.localStorageKey) {
      sessionStorage.setItem(this.localStorageKey + '.txtsearch', txt);
    } else {
      this._textSearch = txt;
    }
  }

  get textSearch(): string {
    if (this.localStorageKey) {
      let txt = sessionStorage.getItem(this.localStorageKey + '.txtsearch');
      return txt ? txt : '';
    } else {
      return this._textSearch ? this._textSearch : '';
    }
  }

  set page(page: number) {
    this._page = page;
    if (this.localStorageKey) {
      sessionStorage.setItem(this.localStorageKey + '.page', page.toString());
    } else {
      this._page = page;
    }
  }

  get page(): number {
    if (this.localStorageKey) {
      let page: any = sessionStorage.getItem(this.localStorageKey + '.page')
      return !isNaN(page) ? parseInt(page): 0;
    } else {
      return this._page ? this._page: 0;
    }
  }

  set pageSize(pageSize: number) {
    this._pageSize = pageSize;
    if (this.localStorageKey) {
      sessionStorage.setItem(this.localStorageKey + '.pagesize', pageSize.toString());
    } else {
      this._pageSize = pageSize;
    }
  }

  get pageSize(): number {
    if (this.localStorageKey) {
      const pageSize: any = sessionStorage.getItem(this.localStorageKey + '.pagesize')
      return pageSize && !isNaN(pageSize) ? parseInt(pageSize) : 5;
    } else {
      return this._pageSize ? this._pageSize : 5;
    }
  }

}

import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// services
import { ApiService } from '../../logposeapp-api/api.service';
// models
import { Unidad } from '../../models/unidad.model';
// components
import { GaleryComponent } from '../galery/galery.component';

@Component({
  selector: 'app-form-unidad',
  templateUrl: './form-unidad.component.html',
  styleUrls: ['./form-unidad.component.css']
})
export class FormUnidadComponent implements OnInit {

  @ViewChild('galery', {static: true})
  public galery: GaleryComponent;
  public mainImage = null;
  public formRegistro: FormGroup;
  public listCategoriasUnidad: Array<any> = [];

  constructor(
    private formBuilder: FormBuilder,
    private services: ApiService
  ) {
    this.initFormRegistro();
  }

  ngOnInit() {
    // this.getImagesFromService();
    // this.getListCategoriesFromService();
  }

  private initFormRegistro() {
    this.formRegistro = this.formBuilder.group({
      'nombre' : ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)])
      ],
      //'estado' : ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
      'telefono' : ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      'direccion' : ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      'hora_apetura' : ['', Validators.required],
      'hora_cierre' : ['', Validators.required],
      'servicio_domicilio': [0]
    });
  }

  public getUnidad(): {value: Unidad, invalid: boolean} {
    for (let key in this.formRegistro.controls) {
      this.formRegistro.controls[key].markAsTouched();
    }
    var unidad = this.formRegistro.value;
    return {
      value: unidad,
      invalid: this.formRegistro.invalid
    }
  }

  @Input()
  set unidad(unidad: Unidad) {
    if (unidad != undefined) {
      this.setFormResgitro(unidad);
    }
  }

  private setFormResgitro(unidad: Unidad) {
    this.formRegistro.controls['nombre'].setValue(unidad.nombre);
    this.formRegistro.controls['telefono'].setValue(unidad.telefono);
    this.formRegistro.controls['direccion'].setValue(unidad.direccion);
    this.formRegistro.controls['hora_apetura'].setValue(unidad.hora_apetura);
    this.formRegistro.controls['hora_cierre'].setValue(unidad.hora_cierre);
    this.formRegistro.controls['servicio_domicilio'].setValue(unidad.servicio_domicilio);
  }

  // private setValueSelect(unidad) {
  //   var self = this;
  //   setTimeout(function() {
  //     if (self.selectCategorias) {
  //       var cats = self.listCategoriasUnidad.filter((item) => {
  //         return unidad.categoria.indexOf(item.id) != -1
  //       })
  //       var names = cats.map(item => item.nombre)
  //       self.selectCategorias.input.value = names.join(", ")
  //     }
  //   }, 1000)
  // }

  // private getImagesFromService() {
  //   this.services.getImages()
  //   .subscribe(result => {
  //     this.galery.images = result;
  //   });
  // }

  // public setMainImage(url: string) {
  //   this.mainImage = url;
  // }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// SERVICES
import { Account, PERFIL_SOCIO } from '../../models/account';
import { Unidad } from '../../models/unidad.model';
import { MatDialog } from '@angular/material/dialog';
import { ModalSelectUnidadComponent } from '../modal-select-unidad/modal-select-unidad.component';
// import swal from 'sweetalert2';
import {select, Store} from '@ngrx/store';
import {selectAccount} from '../../selectors/account.selectors';
import {AppStore} from '../../reducers';
import { cleanToken } from '../../actions/token.actions';
import { AngularFireAuth } from '@angular/fire/auth'
import { selectUnidades } from '../../selectors/unidad.selectors';
import { Observable } from 'rxjs';
import { Socio } from './../../models/socio.model';
import { selectSocio } from '../../selectors/socio.selectors';
import { cleanAccount } from './../../actions/account.actions';
import { cleanSocio } from '../../actions/socio.actions';
import { PedidoService } from '../../logposeapp-api/pedido.service';
import { timer } from 'rxjs';

import Swal from 'sweetalert2';

type Permission = 'read' | 'write';

class ListPermission {
  productos: Array<Permission> | boolean;
  unidades: Array<Permission> | boolean;
  trabajadores: Array<Permission> | boolean;
  pedidos: Array<Permission> | boolean;
  ventas: Array<Permission> | boolean;
  hasNotification: Array<Permission> | boolean;
  chat: Array<Permission> | boolean;
  reportes: Array<Permission> | boolean;
}

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  active = 0;
  permission: ListPermission;
  account$: Observable<Account>;
  socio$: Observable<Socio>;
  timer$: Observable<number>;


  socio: Socio = null;
  totalPedidos: number = null;

  private listUnidades: Array<Unidad>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private store: Store<AppStore>,
    private afauth: AngularFireAuth,
    private pedidoService: PedidoService
  ) {

    this.timer$ = timer(2000, 60000);

    this.account$ = this.store.pipe(select(selectAccount));
    this.socio$ = this.store.pipe(select(selectSocio));

    this.store.pipe(select(selectUnidades)).subscribe(unidades => {
      if (unidades !== undefined) { this.listUnidades = unidades; }
    });

    this.socio$.subscribe(socio => {
      this.socio = socio;
    });
  }

  /**
   *----------------------------------------------------------------------------
   * PUBLIC METHODS
   *----------------------------------------------------------------------------
   */

  async ngOnInit() {
    this.timer$.subscribe(() => {
      if (this.socio$) {
        this.pedidoService.getTotalPedidos().subscribe((data: any) => {
          this.totalPedidos = data.total;
        });
      }
    });
  }

  logout() {
    this.afauth.auth.signOut();
    this.store.dispatch(cleanToken());
    this.store.dispatch(cleanAccount());
    this.store.dispatch(cleanSocio());
    
    this.router.navigate(['/login']);
  }

  async selectUnidad(url: string) {
    if (this.listUnidades && this.listUnidades.length > 0) {
      if (this.listUnidades.length === 1) {
        this.router.navigate([url + this.listUnidades[0].id]);
      } else {
        this.openModalSelectUnidad(url);
      }
    } else {
      Swal.fire('', 'No hay sucursales registradas, dirijase a la seccion de administracion para agregar una sucursal', 'warning');
    }
  }

  /**
   *----------------------------------------------------------------------------
   * PRIVATE METHODS
   *----------------------------------------------------------------------------
   */

  // private initPermission (account?: Account) {
  //   let all = false;
  //   if (account && account.perfil === PERFIL_SOCIO) {
  //     all = true;
  //   }
  //   this.permission = {
  //     productos: all,
  //     unidades: all,
  //     trabajadores: all,
  //     pedidos: all,
  //     ventas: all,
  //     hasNotification: all,
  //     chat: all,
  //     reportes: all,
  //   };
  // }

  private openModalSelectUnidad (url: string) {
    this.dialog.open(ModalSelectUnidadComponent, {
      data: {list: this.listUnidades, route: url}
    });
  }

}

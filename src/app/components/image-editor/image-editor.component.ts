import {
	Component,
	OnInit,
	ViewChild,
	ElementRef,
	EventEmitter,
	Output,
	Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SizeImage } from '../../models/image';


export class DataImageEditor { 
	source: string; 
	sizeImage: SizeImage;
	stdSize: SizeImage;
	finalSize: SizeImage;
	aspectRatio: number;
}


@Component({
	selector: 'app-image-editor',
	templateUrl: './image-editor.component.html',
	styleUrls: ['./image-editor.component.css']
})
export class ImageEditorComponent implements OnInit {

	@ViewChild('image', { static: true }) img: ElementRef;
	@Output() done = new EventEmitter<any>();
	// @Output() cancel = new EventEmitter<any>();

	source: string;
	sizeImage: SizeImage;
	type: string;
	stdSize: SizeImage;
	aspectRatio: number;

	styles: any = {};
	stylesSelector: any = {};
	labelImage: string = '';
	isLoading: boolean = false;
	errorMessage: string;

	private sizeContent: { width: number, height: number };
	private selector: { width: number, height: number, posX?: number, posY?: number };
	private lastDeltaX: number = 0;
	private lastDeltaY: number = 0;
	private factorEscala: number;

	constructor(
		public dialogRef: MatDialogRef<ImageEditorComponent>,
		@Inject(MAT_DIALOG_DATA) private data: DataImageEditor,
	) {
		this.source = this.data.source;
		this.sizeImage =this.data.sizeImage;
		this.stdSize = this.data.stdSize;
		this.aspectRatio = this.data.aspectRatio;
		this.calculates();
	}

	ngOnInit() { }


	// 1600 X 1200
	private calculates() {
		// se ajusta al mayor tamaño ya sea ancho o alto
		if (this.sizeImage.width > this.sizeImage.height && this.calculateHeigth() <= this.stdSize.height) {
			this.sizeContent = {
				width: this.stdSize.width,
				height: this.calculateHeigth()
			}

			this.selector = {
				width: this.sizeContent.height / this.aspectRatio,
				height: this.sizeContent.height,
				posX: 0,
				posY: -(this.sizeContent.height + 6)
			}

		} else {
			this.sizeContent = {
				width: this.calculateWidth(),
				height: this.stdSize.height
			}

			this.selector = {
				width: this.sizeContent.width,
				height: this.sizeContent.width * this.aspectRatio,
				posX: 0,
				posY: -(this.sizeContent.height + 6)
			}

		}

		this.styles = {
			width: this.sizeContent.width + 'px',
			height: this.sizeContent.height + 'px'
		}

		this.stylesSelector = {
			width: this.selector.width + 'px',
			height: this.selector.height + 'px',
			transform: `translate(${this.selector.posX}px, ${this.selector.posY}px)`
		}
	}

	private calculateHeigth() {
		this.factorEscala = this.stdSize.width / this.sizeImage.width;
		return this.sizeImage.height * this.factorEscala;
	}

	private calculateWidth() {
		this.factorEscala = this.stdSize.height / this.sizeImage.height;
		return this.sizeImage.width * this.factorEscala;
	}

	private cutAndResizeImage(): Promise<any> {
		let canvas: any = document.createElement('canvas');
		let ctx = canvas.getContext('2d', { alpha: false });
		canvas.width = this.data.finalSize.width;
		canvas.height = this.data.finalSize.height;
		let posY = (this.selector.posY + (this.sizeContent.height + 6)) / this.factorEscala;
		let posX = this.selector.posX / this.factorEscala;
		ctx.drawImage(
			this.img.nativeElement,
			posX,
			posY,
			this.selector.width / this.factorEscala,
			this.selector.height / this.factorEscala,
			0, 0, this.data.finalSize.width, this.data.finalSize.height
		);

		return new Promise(resolve => {
			canvas.toBlob(function (blob) {
				resolve(blob)
			}, "image/jpeg");
		})
	}

	dragSelector(event) {
		let realDeltaX = event.deltaX - this.lastDeltaX;
		let realDeltaY = event.deltaY - this.lastDeltaY;
		let limitX = this.sizeContent.width - this.selector.width;
		let limitInfY = -(this.sizeContent.height + 6)
		let limitSupY = limitInfY + (this.sizeContent.height - this.selector.height)

		if (event.isFinal) {
			this.lastDeltaX = 0;
			this.lastDeltaY = 0;
			return;
		} else {
			this.lastDeltaX = event.deltaX;
			this.lastDeltaY = event.deltaY;
		}

		if (event.deltaX > 0) {
			this.selector.posX = this.selector.posX >= limitX ? limitX : this.selector.posX += realDeltaX;
		} else {
			this.selector.posX = this.selector.posX <= 0 ? 0 : this.selector.posX += realDeltaX;
		}

		if (event.deltaY > 0) {
			this.selector.posY = this.selector.posY >= limitSupY ? limitSupY : this.selector.posY += realDeltaY;
		} else {
			this.selector.posY = this.selector.posY <= limitInfY ? limitInfY : this.selector.posY += realDeltaY;
		}

		this.stylesSelector.transform = `translate(${this.selector.posX}px, ${this.selector.posY}px)`
	}

	async onClickAceptar() {
		this.isLoading = true;
		this.errorMessage = '';
		let file = await this.cutAndResizeImage();
		
		this.dialogRef.close({file});
		
		/*try {
			let imgName = uuid() + '.jpg';
			let path = this.imageService.getPathByType(this.type);
			let imgPath = path + '/' + imgName;
			let success = await this.imageService.saveImage(imgPath, file);
			if (success) {
				this.isLoading = false;
				this.dialogRef.close({imgName});
			} else {
				this.errorMessage = 'Error al guargar la imagen intente mas tarde';
			}
		} catch (err) {
			this.isLoading = false;
			this.errorMessage = 'Error al guargar la imagen intente mas tarde';
		}*/
	}

	onClickCancelar() {
		this.errorMessage = '';
		this.dialogRef.close({file: null});
	}

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from '../../logposeapp-api/api.service';
import { API_USUARIO } from '../../logposeapp-api/api-routes';
import { Response } from '../../models/models';
@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  isLoading: boolean;
  success: boolean;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(async(params: ParamMap) => {
      this.sendRequesConfirmEmail(params.get('token'))
    })
  }

  private async sendRequesConfirmEmail(token) {
    try {
      let response = await this.api.get(API_USUARIO.confirmEmail + token, {}).toPromise() as Response;
      this.isLoading = false;
      this.success = response.success;
    } catch (error) {
      this.success = false;
    }
  }

}

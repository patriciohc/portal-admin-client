import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router }  from '@angular/router';
// models
import { Producto } from '../../models/producto.model';
import { LoadingService } from './../../services/loading.service';
import { Column, Catalog } from './../../models/table';
import { CategoriaProducto } from '../../models/categoria-producto.model';
import { AppStore } from './../../reducers/index';
import * as productosSelectors from '../../selectors/producto.selectors';
import { Store, select } from '@ngrx/store';
import * as productosActions from '../../actions/producto.actions';
import * as csv from 'csvtojson';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  columnsTable: Array<Column>;
  listProductsfilter: Array<Producto>;
  textSearch: string;
  @ViewChild('inputfile', {static: true}) inputFile: ElementRef;
  listProducts: Array<Producto>;

  private listCategories: Array<CategoriaProducto>;

  constructor(
    private loading: LoadingService,
    private router: Router,
    private store: Store<AppStore>
  ) {}

  ngOnInit() {

    this.store.pipe(select(productosSelectors.selectProductos)).subscribe(list => {
      if (list) {
        this.listProducts = list;
      }
    });

    this.store.pipe(select(productosSelectors.selectProductosLoading)).subscribe(isLoading => {
      if (isLoading) {
        this.loading.show();
      } else {
        this.loading.hide();
      }
    });
  
    this.store.pipe(select(productosSelectors.selectError)).subscribe(error => {
      if (error) {
        console.error(error);
        if (error['statusCode'] == 500) {
          Swal.fire('', 'Occurio un erro en el serividor, contacte a soporte', 'error');
        } else if (error['statusCode'] == 400) {
          Swal.fire('', 'Solicitud invalidad, verifique la informacion', 'error');
        }
      }
    });

    this.store.select('categoriasProducto').subscribe(categoriasState => {
      if (categoriasState.categorias) {
        this.listCategories = categoriasState.categorias;
        this.initColumnsTable();
      }
    });

  }

  private initColumnsTable() {
    this.columnsTable = [
      {
        text: 'Codigo',
        type: 'string',
        keyReferenceToRow: 'codigo'
      }, {
        text: 'Nombre',
        type: 'string',
        keyReferenceToRow: 'nombre'
      }, {
        text: 'Descripcion',
        type: 'string',
        keyReferenceToRow: 'descripcion'
      }, {
        text: 'Precio',
        type: 'string',
        keyReferenceToRow: 'precio_publico'
      }, {
        text: 'Categoria',
        type: 'catalog',
        keyReferenceToRow: 'categoria',
        dataCatalog: (this.listCategories as Array<Catalog>)
      }, {
        text: 'Editar',
        type: 'action',
        dataAction: {
          key: 'editar',
          class: '',
          icon: 'edit'
        }
      }, {
        text: 'Eliminar',
        type: 'action',
        dataAction: {
          key: 'eliminar',
          class: 'red',
          icon: 'delete'
        }
      }
    ]
  }

  public async actionsTable(data) {
    switch (data.keyAction) {
      case 'editar':
        this.router.navigate(['/admin/productos/editar/' + data.row.id])
        break;
      case 'eliminar':
        let response = await Swal.fire({
          title: `¿Estas seguro que quieres eliminar el producto ${data.row.nombre}?`,
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Eliminar',
          cancelButtonText: 'Cancelar'
        } as any);
        if (!response.value) return;
        this.store.dispatch(productosActions.deleteProducto({id: data.row.id}));
        break;
    }
  }

  uploadFile() {
    this.inputFile.nativeElement.click();
  }


  handleFileInput(files: Array<File>) {
    const reader = new FileReader();
    reader.onload = async (event: any) => {
      let {listProducts, existents} = await this.getListProductosFromFile(event.target.result);
      try {
        // await this.repository.saveBulk(listProducts);
        Swal.fire('','Se guardo correctamente','success');
      } catch (erro) {
        Swal.fire('', 'Ocurrio un errro, pongase en contacto con soperte', 'error');
      }
    }
    reader.onerror = error => console.log(error);
    reader.readAsText(files[0])
  }

  /**
   * Get list products from csv file
   */
  private async getListProductosFromFile(result: string): Promise<any> {
    var listProducts = [];
    var existents = [];
    return new Promise ((resolve, reject) => {
      csv()
      .fromString(result)
      .subscribe((line)=> {
        if (this.isProductValid(line) &&
          !this.verifyExistDuplicate(line, listProducts)
        ) {
          let product = {
            codigo: line.codigo,
            nombre : line.nombre,
            descripcion: line.descripcion,
            imagen: line.imagen,
            precio: line.precio,
            precio_publico: line.precio_publico,
            categoria: this.getCategoria(line.categoria)
          }
          listProducts.push(product);
        } else {
          existents.push(line);
        }
      }, (error) => reject(error),
      () => resolve({listProducts, existents}))
    });
  }

  private isProductValid(product: Producto) {
    return product.codigo &&
      product.nombre &&
      product.precio_publico &&
      product.precio;
  }

  private verifyExistDuplicate(line: any, list: Array<Producto>): boolean {
    let index: number;
    index = list.findIndex(item =>
      item.codigo == line.codigo ||
      item.nombre == line.nombre);
    if (index == -1) {
      index = this.listProducts.findIndex(item =>
        item.codigo == line.codigo ||
        item.nombre == line.nombre);
    }
    return index != -1;
  }

  private getCategoria(categoryName: string) {
    let cat = this.listCategories.find(item => item.nombre == categoryName);
    if (cat) {
      return cat;
    } else if (this.listCategories && this.listCategories.length) {
      return this.listCategories[0]; // toma la primera categoria por default
    } else {
      return undefined;
    }
  }

}

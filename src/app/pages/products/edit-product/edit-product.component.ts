import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute }  from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// models
import { CategoriaProducto } from '../../../models/categoria-producto.model';
import { Producto } from '../../../models/producto.model';

import { sleep } from '../../../utils/utils';
import { LoadingService } from '../../../services/loading.service';
import { Store, select } from '@ngrx/store';
import { AppStore } from './../../../reducers/index';
import * as productosActions from '../../../actions/producto.actions';
import * as productosSelectors from '../../../selectors/producto.selectors';
import { ImageService } from './../../../services/images.service';
import { selectProductosLoading } from './../../../selectors/producto.selectors';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  public idProduct: string;

  public nameImage: string;
  public urlImage: string;
  public listCategoriesProduct: Array<CategoriaProducto>;
  public selectCategoria: any;

  public formRegistro: FormGroup = this.formBuilder.group({
    'nombre' : ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
    'codigo' : ['', Validators.compose([Validators.required, Validators.maxLength(200)])],
    'precio' : [''],
    'precio_publico' : ['', Validators.required],
    'id_categoria' : ['', Validators.required],
    'descripcion': ['', Validators.required],
    'activo': [true],
    'palabras_clave': ['']
  });

  constructor(
    private loading: LoadingService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppStore>,
    private imageService: ImageService
  ) {

    this.store.select('categoriasProducto').subscribe(categoriasState => {
      if (categoriasState.categorias) this.listCategoriesProduct = categoriasState.categorias;
    });

  }

  async ngOnInit() {
    this.idProduct = this.route.snapshot.params['id'];

    this.store.pipe(select(productosSelectors.selectProductos)).subscribe(list => {
      if (list && this.idProduct) {
        let producto = list.find(item => item.id == this.idProduct);
        if (producto) this.setDataFormControls(producto);
      }
    });
  }

  /**
   * carga el producto con el idProduct en el formulario
   */
  private setDataFormControls(data: Producto) {
    if (Array.isArray(data.imagen) && data.imagen.length) {
      this.setMainImage({imgName: data.imagen[0]});
    }
    var formUnidad = {
      'nombre': data.nombre, 
      'codigo': data.codigo,
      'precio': data.precio ? data.precio: null,
      'precio_publico': data.precio_publico ? data.precio_publico: null,
      'id_categoria' : data.categoria ? data.categoria.id: null,
      'descripcion': data.descripcion ? data.descripcion: '',
      'palabras_clave': '',
      'activo': data.activo //data.palabras_clave
    };
    this.formRegistro.setValue(formUnidad, {emitEvent: true});
  }

  public async saveProduct() {
    let producto = this.getDataControls();
    if (!producto) return;
    this.store.dispatch(productosActions.addProducto({producto}));
    this.router.navigate(['admin/productos']);
  }

  public async updateProduct() {
    let producto: any = this.getDataControls();
    if (!producto) return;
    this.store.dispatch(productosActions.updateProducto({producto}));
    this.router.navigate(['admin/productos']);
  }

  private getDataControls(): Producto {
    if(this.formRegistro.invalid) {
      Swal.fire('', 'Los campos marcados con * son requeridos');
      return null;
    }
    let producto = this.formRegistro.value;
    producto.categoria = {id: producto.id_categoria};
    if (this.nameImage) {
      producto.imagen = [this.nameImage];
    }
    producto.id = this.idProduct;
    return new Producto(producto);
  }

  public setMainImage({imgName}) {
    this.nameImage = imgName;
    this.urlImage = this.imageService.getFullPathImg('productos', imgName);
  }

  async generarCodigo() {
    await sleep(100)
    let code = (Date.now()).toString()
    code = code.substring(code.length - 13, code.length)
    this.formRegistro.controls['codigo'].setValue(code);
  }

}

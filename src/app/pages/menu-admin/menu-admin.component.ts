import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Unidad } from '../../models/unidad.model';
import { MatDialog } from '@angular/material/dialog';
import { ModalSelectUnidadComponent } from './../../components/modal-select-unidad/modal-select-unidad.component';
import { AppStore } from './../../reducers/index';
import { Store, select } from '@ngrx/store';
import { selectUnidades } from '../../selectors/unidad.selectors';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-menu-admin',
  templateUrl: './menu-admin.component.html',
  styleUrls: ['./menu-admin.component.css']
})
export class MenuAdminComponent implements OnInit {

  private listUnidades: Array<Unidad>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private store: Store<AppStore>,
  ) { }

  ngOnInit() {
    this.store.pipe(select(selectUnidades)).subscribe(unidades => {
      if (unidades !== undefined) { this.listUnidades = unidades; }
    });
  }


  async showReports() {
    if (this.listUnidades && this.listUnidades.length > 0) {
      if (this.listUnidades.length == 1) {
        this.router.navigate(['/admin/resporte-ventas-sitio/' + this.listUnidades[0].id]);
      } else {
        this.openModalSelectUnidad();
      }
    } else {
      Swal.fire('', 'No hay sucursales registradas', 'warning');
    }
  }

  private openModalSelectUnidad () {
    this.dialog.open(ModalSelectUnidadComponent, {
      data: {list: this.listUnidades, route: '/admin/resporte-ventas-sitio/'}
    });
  }

}

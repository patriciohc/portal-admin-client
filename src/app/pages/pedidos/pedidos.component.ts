import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
// services
import { OneSignalService } from '../../services/onesignal.service';

import { sleep } from '../../utils/utils'
import { Column, Catalog } from './../../models/table';
import { AppStore } from '../../reducers';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Venta, TipoVenta, TipoEntrega, TipoHoraEntrega } from '../../models/venta.model';
import { selectAccount } from '../../selectors/account.selectors';
import { Account } from '../../models/account';
import { selectPedidos, selectPedidosLoading } from '../../selectors/pedidos.selectors';
import { Empleado } from '../../models/empleado.model';
import { Unidad } from '../../models/unidad.model';
import { selectEmpleados } from '../../selectors/empleados.selectors';
import { ServicioTaxi } from './../../models/servicio-taxi.model';
import * as pedidosActions from './../../actions/pedido.actions';
import { EstatusVenta } from '../../models/venta.model';
import { UnidadService } from '../../logposeapp-api/unidad.service';


import Swal from 'sweetalert2';
import { LoadingService } from '../../services/loading.service';

@Component({
    selector: 'app-pedidos',
    templateUrl: './pedidos.component.html',
    styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit, AfterViewInit {

    private months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
    private status = ['En espera', 'Aceptado', 'En ruta', 'Entregado']
    // @ViewChild('tableUnidades', { static: true })
    @ViewChild('mapEntrega', { static: false })
    private mapEntrega: ElementRef;
    public listPedidos: Array<any> = [];
    private map: any;

    public showFormResgistroSucursal: boolean = false;
    public detailPedido: Venta = null;
    public idUnidad = '';
    public hasAssignments: boolean = false;
    public showSelectUnidad: boolean = false;
    public idRepartidor: any = -1;

    public account$: Observable<Account>;
    public pedidos$: Observable<Array<Venta>>;
    public empleados$: Observable<Array<Empleado>>;
    public unidades$: Observable<Array<Unidad>>;

    private isLoading: boolean;
    private pedidos: Array<Venta>;
    private empleados: Array<Empleado>;

    public infoEntrega = '';
    public horaEntrega = '';
    public isActivo: boolean;
    public unidadId: string;

    private catalogEstatus: Array<Catalog> = [
        {
            id: EstatusVenta.ESPERANDO,
            nombre: 'Esperando'
        }, {
            id: EstatusVenta.ACEPTADO,
            nombre: 'Aceptado'
        }, {
            id: EstatusVenta.EN_PROGRESO,
            nombre: 'En Progreso'
        }, {
            id: EstatusVenta.FINALIZADO,
            nombre: 'Finalizado'
        }, {
            id: EstatusVenta.CANCELADO,
            nombre: 'Cancelado'
        }
    ]

    public headPedidos: Array<Column>;

    constructor(
        private store: Store<AppStore>,
        private route: ActivatedRoute,
        private oneSignal: OneSignalService,
        private unidadService: UnidadService,
        private loading: LoadingService,
    ) {
        this.initColumns();
        this.account$ = this.store.pipe(select(selectAccount));
        this.empleados$ = this.store.pipe(select(selectEmpleados));

        this.route.params.subscribe(params => {
            this.unidadId = params['id'];
            this.pedidos$ = this.store.pipe(select(selectPedidos));
            this.listenPedidos();
            this.store.dispatch(pedidosActions.loadListPedidos({unidadId: this.unidadId}));

            this.unidadService.isActivo(this.unidadId).subscribe((response) => {
                this.isActivo = response.data;
            });

        });

        this.empleados$.subscribe(empleados => {
            this.empleados = empleados;
        });

        this.oneSignal.notifications.subscribe((event: any) => {
            if (event) {
                const unidadId = this.route.snapshot.params['id'];
                if (event.data['unidadId'] == unidadId) {
                    this.store.dispatch(pedidosActions.loadListPedidos({unidadId}));
                }
            }
        });

        this.store.pipe(select(selectPedidosLoading)).subscribe(isLoading => {
            this.isLoading = isLoading;
            if (isLoading) {
                this.loading.show();
              } else {
                this.loading.hide();
              }
        });
    }

    changeStatus() {
        if (this.isActivo) {
            this.unidadService.activar(this.unidadId).subscribe();
        } else {
            this.unidadService.desactivar(this.unidadId).subscribe()
        }
    }

    private initColumns () {
        this.headPedidos = [
            {
                text: 'No. ticket',
                type: 'string',
                keyReferenceToRow: 'no_ticket'
            }, {
                text: 'Usuario',
                type: 'string',
                keyReferenceToRow: 'comprador.nombre'
            }, {
                text: 'Estatus',
                type: 'catalog',
                keyReferenceToRow: 'estatus',
                dataCatalog: this.catalogEstatus
            }, {
                text: 'Fecha',
                type: 'string',
                keyReferenceToRow: 'fecha'
            }, {
                text: 'Detalles',
                type: 'action',
                dataAction: {
                    key: 'ver',
                    class: '',
                    icon: 'more'
                }
            }
        ]
    }

    listenPedidos() {
        this.pedidos$.subscribe(pedidos => {
            if (pedidos) {
                this.listPedidos = pedidos.map(item => {
                    let fecha = this.timestampFormat(item.fecha + 'Z');
                    let estatus_html = this.status[item.estatus];
                    return { ...item, estatus_html, fecha };
                });
                this.updateSelectPedidoDetails();
            }
        });
    }

    updateSelectPedidoDetails() {
        if (this.detailPedido != null) {
            this.detailPedido = this.listPedidos.find(item => item.id == this.detailPedido.id)
        }
    }

    timestampFormat(timestamp) {
        var date = new Date(timestamp);
        var fecha = this.months[date.getMonth()] + '-' + date.getDate();
        fecha += ' ' + date.getHours() + ':' + date.getMinutes();
        return fecha;
    }

    async ngOnInit() {
        let _startY;
        const inbox = document.querySelector('#pedidos-view');
      
        inbox.addEventListener('touchstart', (e: any) => {
          _startY = e.touches[0].pageY;
        }, {passive: true});
      
        inbox.addEventListener('touchmove', (e: any) => {
          const y = e.touches[0].pageY;
          if (document.scrollingElement.scrollTop === 0 && y > _startY && !this.isLoading) {
            console.log('updating...');
            const unidadId = this.route.snapshot.params['id'];
            this.store.dispatch(pedidosActions.loadListPedidos({unidadId}));
          }
        }, {passive: true});

        // // si no es cliente, buscara las unidades a las que esta asignado el operador
        // if (!sesion.id_cliente) {
        //   var result = await this.services.getListUnidadesOperador().toPromise()
        //   this.showListUnidadesAssigned(result)
        // } else if (sesion.rol == 5) { // es cliente entonces se muestra la lista de unidades
        //   this.showSelectUnidad = true;
        //   this.hasAssignments = true;
        //   try {
        //     await this.getListUnidad();
        //     if (this.idUnidad) await this.getListPedidos();
        //     // if (selects.length == 1) self.selectCategorias = selects[0];
        //     this.getListRepartidores();
        //   } catch (err) {
        //     console.error(err);
        //   }
        //   // setTimeout(this.makeChar.bind(this), 1000);
        // }

        // this.sw.notifications.subscribe(result => {
        //   this.getListPedidos();
        // });

    }

    async ngAfterViewInit() {
        await sleep(1000);
        // var el = document.querySelectorAll('app-pedidos select');
        // var selects = window.M_initSelects(el);
    }

    private showListUnidadesAssigned(lista) {
        if (lista.length == 0) {
            this.hasAssignments = false;
            return;
        }
        this.hasAssignments = true;
        // if (lista.length == 1) {
        //   this.services.siginSucursal(lista[0].id_unidad)
        //   .subscribe((result: any) => {
        //     this.idUnidad = result.id_unidad;
        //     this.showSelectUnidad = false;
        //     this.getListPedidos()
        //     console.log(result)
        //   })
        // }
    }

    public filterPedidos() {
        this.listPedidos = this.idUnidad == '' ? this.pedidos : this.pedidos.filter(item => item.unidad.id == this.idUnidad);
    }

    public actionsTable(data) {
        switch (data.keyAction) {
            case 'ver':
                this.detailPedido = data.row;
                this.infoEntrega = '';
                this.horaEntrega = '';
                if (this.detailPedido.tipo == TipoVenta.REGULAR) {
                    const infoEntrega = this.detailPedido.informacion_entrega;
                    if (infoEntrega.tipo_entrega == TipoEntrega.MOSTRADOR) {
                        this.infoEntrega = 'Entrega en mostrador';
                    } else if (infoEntrega.tipo_entrega == TipoEntrega.DOMICILIO) {
                        this.infoEntrega = 'Entrega en domicilio';
                        setTimeout(() => {
                            this.showMap(infoEntrega);
                        }, 500);
                    }

                    if (infoEntrega.tipo_hora_entrega == TipoHoraEntrega.LO_ANTES_POSIBLE) {
                        this.horaEntrega = 'Entregar los antes posible';
                    } else if (infoEntrega.tipo_hora_entrega == TipoHoraEntrega.HORA_PROGRAMADA) {
                        this.horaEntrega = 'Entregar a las ' + infoEntrega.hora_entrega;
                    }

                } else if (this.detailPedido.tipo == TipoVenta.ESPECIAL) {
                    if (this.detailPedido.producto_especial.producto.nombre == 'SERVICIO_TAXI') {
                        let info = this.detailPedido.producto_especial.info as ServicioTaxi;
                        setTimeout(() => {
                            this.showMap(info.direccion_origen);
                        }, 500);
                    }
                }
                
                break;
        }
    }

    public onClickSetState(status: EstatusVenta) {
        this.store.dispatch(pedidosActions.updateEstatus({pedidoId: this.detailPedido.id, estatus: status}))
    }

    public asignarRepartidor() {
        let repartidor = null;
        if (this.idRepartidor != "-1") {
            Swal.fire('', 'Actualmente no se soporta la asignacion de repartidor');
            // repartidor = this.empleados.find(empleado => empleado.id == this.idRepartidor);
        }
        this.store.dispatch(pedidosActions.updateEstatus({pedidoId: this.detailPedido.id, estatus: EstatusVenta.EN_PROGRESO}))
        // this.pedidoService.asignarRepartidor(this.detailPedido.id, repartidor)
    }

    private showMap(pos) {
        let latLng = new google.maps.LatLng(pos.lat, pos.lng);

        let mapOptions = {
            center: latLng,
            zoom: 16,
            disableDefaultUI: true
        }
        this.map = new google.maps.Map(this.mapEntrega.nativeElement, mapOptions);

        let marker = new google.maps.Marker({
            position: latLng,
            map: this.map,
            title: 'Entrega'
        });
        this.map.panTo(latLng)
    }

}

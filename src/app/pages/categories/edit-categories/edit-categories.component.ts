import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute }  from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// models
import { CategoriaProducto } from '../../../models/categoria-producto.model';
// components
import { LoadingService } from '../../../services/loading.service';
import { AppStore } from './../../../reducers/index';
import { Store } from '@ngrx/store';
import {
  addNewCategoriaProducto,
  updateCategoriaProducto
} from '../../../actions/categoria-producto.actions';
import { ImageService } from './../../../services/images.service';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.css']
})
export class EditCategoriesComponent implements OnInit {

  public idCategoria: any;

  public nameImage: string;
  public urlImage: string;
  public formRegistro: FormGroup;

  constructor(
    private store: Store<AppStore>,
    private loading: LoadingService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private imageService: ImageService
  ) {
    
  }

  ngOnInit() {
    this.formRegistro = this.formBuilder.group({
      'nombre' : ['', Validators.compose([Validators.required, Validators.minLength(4),  Validators.maxLength(40)])],
      'descripcion': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });
    this.idCategoria = this.route.snapshot.params['id'];
    this.store.select('categoriasProducto').subscribe(categoriaState => {
      if (this.idCategoria && categoriaState.categorias) {
        let category = categoriaState.categorias.find(item => item.id == this.idCategoria);
        this.setCategoryToForm(category);
      }

      if (categoriaState.requestError) {
        if (categoriaState.requestError['error']) {
          Swal.fire('', categoriaState.requestError['error'], 'error');
        } else {
          Swal.fire('', 'Ocurrio un error, verifique su conexion, o pongase en contacto con soporte', 'error');
        }
      }
    });


  }

  setCategoryToForm(category: CategoriaProducto) {
    if (category.imagen) this.setMainImage({imgName: category.imagen});
    this.formRegistro.controls['nombre'].setValue(category.nombre);
    this.formRegistro.controls['descripcion'].setValue(category.descripcion);
  }

  private getData(): CategoriaProducto {
    let obj = this.formRegistro.value
    if(obj.invalid) return null;
    obj.imagen = this.nameImage;
    obj.id = this.idCategoria;
    return new CategoriaProducto(obj);
  }

  public clickOnCancel() {
    this.router.navigate(['/admin/categorias']);
  }

  public clickOnUpdate() {
    let categoria = this.getData();
    if (!categoria) return;
    this.store.dispatch(updateCategoriaProducto({categoria}));
    this.router.navigate(['/admin/categorias']);
  }

  public clickOnRegister() {
    let categoria = this.getData();
    if (!categoria) return;
    this.store.dispatch(addNewCategoriaProducto({categoria}));
    this.router.navigate(['/admin/categorias'])
  }

  public setMainImage({imgName}) {
    this.nameImage = imgName
    this.urlImage = this.imageService.getFullPathImg('categorias', imgName);
  }

}

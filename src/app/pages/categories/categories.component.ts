import { Component, OnInit, ViewChild } from '@angular/core';
import { Router }  from '@angular/router';
// models
import { CategoriaProducto } from '../../models/categoria-producto.model';
import { Column } from './../../models/table';
import { Store } from '@ngrx/store';
import { AppStore } from './../../reducers/index';
import { removeCategoriaProducto, cleanError } from './../../actions/categoria-producto.actions';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public headUnidades: Array<Column> = [
    {
      text: 'Nombre',
      type: 'string',
      keyReferenceToRow: 'nombre'
    }, {
      text: 'Descripcion',
      type: 'string',
      keyReferenceToRow: 'descripcion'
    }, {
      text: 'Editar',
      type: 'action',
      dataAction: {
        key: 'editar',
        class: '',
        icon: 'edit'
      }
    }, {
      text: 'Eliminar',
      type: 'action',
      dataAction: {
        key: 'eliminar',
        class: 'red',
        icon: 'delete'
      }
    }
  ]

  public list: Array<CategoriaProducto>;

  constructor(
    private store: Store<AppStore>,
    private router: Router
  ) {
    this.store.select('categoriasProducto').subscribe(categoriasState => {
      if (categoriasState.categorias) {
        this.list = categoriasState.categorias
      }

      if (categoriasState.requestError) {
        if (categoriasState.requestError['message']) {
          Swal.fire('', categoriasState.requestError['message'], 'error');
        } else {
          Swal.fire('', 'Ocurrio un error, verifique su conexion, o pongase en contacto con soporte', 'error');
        }
        this.store.dispatch(cleanError());
      }

    });
  }

  ngOnInit() { }

  public async actionsTable(data) {
    switch (data.keyAction) {
      case 'editar':
        this.router.navigate(['/admin/categoria/editar/' + data.row.id])
        break;
      case 'eliminar':
        this.store.dispatch(removeCategoriaProducto({id: data.row.id}));
        break;
    }
  }

}

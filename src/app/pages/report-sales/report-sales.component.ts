import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from '../../components/chart/chart.component';
import * as moment from 'moment/';
import { Column } from './../../models/table';
import { VentaService } from '../../logposeapp-api/venta.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from './../../services/loading.service';
import { headVentas, headProductos, headRecargas } from './config-tables';
import { EmitDataTable } from './../../components/table/table.component';
import { Unidad } from '../../models/unidad.model';
import { Socio } from '../../models/socio.model';
import { MatDialog } from '@angular/material/dialog';
import { AppStore } from './../../reducers/index';
import { Store, select } from '@ngrx/store';
import { selectUnidad } from '../../selectors/unidad.selectors';
import { Observable } from 'rxjs';
import { selectSocio } from '../../selectors/socio.selectors';
import { Venta, TipoVenta } from './../../models/venta.model';
import { selectVentaDetails } from '../../selectors/venta.selectors';
import { selectReporteVentas } from './../../selectors/venta.selectors';
import { loadReportVentas } from './../../actions/venta.actions';
import * as ticketActions from '../../actions/ticket.actions';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-report-sales',
  templateUrl: './report-sales.component.html',
  styleUrls: ['./report-sales.component.css']
})
export class ReportSalesComponent implements OnInit {

    totalRangeVentas = 0;
    dateInit: Date;
    dateEnd: Date;
    unidad: Unidad;
    dateSelected: string;
    totaRecargas = 0;
    @ViewChild('barchar', {static: true} ) barchar: ChartComponent;
    // @ViewChild('barline') barline: ChartComponent;

    listVentasByDay: Array<any> = [];
    listVentasByDayProducts: Array<any> = [];
    listVentasByDayRecargas: Array<any> = [];
    // config tables
    headVentas: Array<Column> = headVentas;
    headProductos: Array<Column> = headProductos;
    headRecargas: Array<Column> = headRecargas;

    private socio: Socio;

    public socio$: Observable<Socio>;
    public detailVenta$: Observable<Venta>;
    public reporteVentas$: Observable<any>;


    constructor(
        private ventaService: VentaService,
        private route: ActivatedRoute,
        private loading: LoadingService,
        private store: Store<AppStore>,
    ) {
        this.socio$ = this.store.pipe(select(selectSocio));
        this.detailVenta$ = this.store.pipe(select(selectVentaDetails));
        this.reporteVentas$ = this.store.pipe(select(selectReporteVentas));

        this.socio$.subscribe(socio => {
          if (socio) this.socio = socio;
        });

        this.reporteVentas$.subscribe(report => {
            if (report != null) {
                const labelsChar = [];
                const datasetsChar = [{
                     label: 'Ventas',
                     data: [],
                     backgroundColor: 'rgba(54, 162, 235, 0.3)'
                }];
                this.totalRangeVentas = 0;
                for (let i = 0; i < report.length; i++) {
                    this.totalRangeVentas += report[i].total;
                    const data = report[i];
                    const dateFormat = moment(data.fecha_venta).format('YYYY MMM DD');
                    labelsChar.push(dateFormat);
                    datasetsChar[0].data.push(data.total);
                }
                this.barchar.reload(labelsChar, datasetsChar);
            }
        });
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            const init = moment().subtract(30, 'days');
            const end = moment().add(2, 'days');
            this.dateInit = init.toDate();
            this.dateEnd = end.toDate();
            const unidadId = this.route.snapshot.params['id'];
    
            this.store.pipe(select(selectUnidad, unidadId)).subscribe(unidad => {
                if (unidad != undefined) {
                    this.unidad =unidad
                    this.onClickBuscar();
                }
            });
            this.listVentasByDay = [];
            this.listVentasByDayProducts = [];
            this.listVentasByDayRecargas = [];
        });
    }

    async onClickBuscar () {
        const dateInit = moment(this.dateInit);
        const dateEnd = moment(this.dateEnd);
        this.store.dispatch(loadReportVentas({
            unidadId: this.unidad.id,
            initDate:  dateInit.format('YYYY-MM-DD'),
            endDate: dateEnd.format('YYYY-MM-DD')
        }));
    }

    async onClickBar(data) {
        const fecha = moment(data.x_value, 'YYYY MMM DD');
        this.dateSelected = fecha.format('MMMM DD YYYY');
        try {
            this.loading.show();
            await this.getListDayVentas(fecha.format('YYYY-MM-DD'));
            await this.getListVentasDayProducts(fecha.format('YYYY-MM-DD'));
            await this.getListVentasDayRecargas(fecha.format('YYYY-MM-DD'));
        } catch (error) {
            Swal.fire('', 'Ocurrio un error intente mas tarde', 'error');
        } finally {
            this.loading.hide();
        }
    }

    async actionsTableRecarga(data: EmitDataTable) {
        if (data.keyAction == 'ver') {
            this.store.dispatch(ticketActions.newTicket({venta: data.row}))
        }
    }

    async actionsTableVentas(data: EmitDataTable) {
        if (data.keyAction == 'ver') {
            this.store.dispatch(ticketActions.newTicket({venta: data.row}))
        }
    }

    private getDataPrint(data: any) {
        data.fecha = moment().format('YYYY-MM-DD');
        return {
            socio: {razon_social: this.socio.razon_social},
            unidad: {direccion: this.unidad.direccion, telefono: this.unidad.telefono},
            data: data
        };
    }

    private async getListDayVentas (fecha: string) {
        try {
            const report = await this.ventaService
                .getSaleByDay(this.unidad.id, fecha, TipoVenta.REGULAR);
            this.listVentasByDay = report;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    private async getListVentasDayProducts (fecha: string) {
        try {
            const report = await this.ventaService.getProductsByDay(this.unidad.id, fecha);
            this.listVentasByDayProducts = report;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    private async getListVentasDayRecargas (fecha: string) {
        try {
            const report = await this.ventaService
                .getSaleByDay(this.unidad.id, fecha, TipoVenta.ESPECIAL);
            this.listVentasByDayRecargas = [];
            this.totaRecargas = 0;
            for (let i = 0; i < report.length; i++) {
                let item = report[i];
                if (item.producto_especial.producto.nombre == 'RECARGA_ELECTRONICA') {
                    this.listVentasByDayRecargas.push(item);
                    this.totaRecargas += item.total;
                }
                // const info = Object.assign({fecha: item.fecha}, item.producto_especial.info);
                // this.listVentasByDayRecargas.push(info);
            }
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    async getTopProducts () {
      // let fecha1 = this.dateInit.nativeElement.value;
      // let fecha2 = this.dateEnd.nativeElement.value;
      // let query = {by: 'top-products', fecha1, fecha2 };
      // let response: any = await this.api.get(API_VENTA, {query}).toPromise();
      // if (response.code == 'ERROR') return;
      // var labelsChar = [];
      // var datasetsChar = []
      // var series = response.data.groupBy('nombre');
      // for (let key of series) {
      //   let serie = series[key];
      //   var item = {
      //       label: key,
      //       data: [],
      //       backgroundColor: 'rgba(54, 162, 235, 0.3)'
      //   };
      //   for (let i = 0; i < serie.length; i++) {

      //     // labelsChar.push(data.fecha);
      //     datasetsChar[0].data.push(serie[i].total_vendidos)
      //   }
      //   datasetsChar.push(item)
      // }
      // this.barline.reload(labelsChar, datasetsChar)

      // console.log(JSON.stringify(response.data))
    }

}

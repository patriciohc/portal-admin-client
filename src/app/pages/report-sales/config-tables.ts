import { Column } from './../../models/table';


export const headVentas: Array<Column> = [
    {
      text: 'No. Tikect',
      type: 'string',
      keyReferenceToRow: 'no_ticket'
    }, {
      text: 'Fecha',
      type: 'string',
      keyReferenceToRow: 'fecha'
    }, {
      text: 'Total',
      type: 'string',
      keyReferenceToRow: 'total'
    }, {
      text: 'Ver ticket',
      type: 'action',
      dataAction: {
        key: 'ver',
        class: '',
        icon: 'print'
      }
    }
];


export const headProductos: Array<Column> = [
    {
      text: 'Codigo',
      type: 'string',
      keyReferenceToRow: 'codigo'
    }, {
      text: 'Nombre',
      type: 'string',
      keyReferenceToRow: 'nombre'
    }, {
      text: 'Descripcion',
      type: 'string',
      keyReferenceToRow: 'descripcion'
    }, {
      text: 'Precio',
      type: 'string',
      keyReferenceToRow: 'precio_publico'
    }, {
      text: 'Total Vendidos',
      type: 'string',
      keyReferenceToRow: 'total_vendidos'
    }
];

  export const headRecargas: Array<Column> = [
    {
      text: 'Compania',
      type: 'string',
      keyReferenceToRow: 'producto_especial.info.compania'
    }, {
      text: 'Fecha',
      type: 'string',
      keyReferenceToRow: 'fecha'
    }, {
      text: 'Folio',
      type: 'string',
      keyReferenceToRow: 'producto_especial.info.folio'
    }, {
      text: 'Telefono',
      type: 'string',
      keyReferenceToRow: 'producto_especial.info.telefono'
    }, {
      text: 'Descripcion',
      type: 'string',
      keyReferenceToRow: 'producto_especial.info.descripcion'
    }, {
      text: 'Ver ticket',
      type: 'action',
      dataAction: {
        key: 'ver',
        class: '',
        icon: 'print'
      }
    }
];
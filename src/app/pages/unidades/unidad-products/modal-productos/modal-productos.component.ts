import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { CategoriasProdcutoStore } from '../../../../store/categorias-producto.store';
import { Catalog } from './../../../../models/table';
import { Unidad } from '../../../../models/unidad.model';
import { Producto } from '../../../../models/producto.model';
import { TableComponent } from './../../../../components/table/table.component';
import { AppStore } from '../../../../reducers/index';
import { Store, select } from '@ngrx/store';
import * as productosSelectors from '../../../../selectors/producto.selectors';

@Component({
  selector: 'app-modal-productos',
  templateUrl: './modal-productos.component.html',
  styleUrls: ['./modal-productos.component.css']
})
export class ModalProductosComponent implements OnInit {

  listProducts: Array<Producto> = [];
  columnsTable: Array<any>;

  @ViewChild('tableAddProducts', {static: true})
  tableProducts: TableComponent;

  constructor(
    // private categoriasProdcutoStore: CategoriasProdcutoStore,
    public dialogRef: MatDialogRef<ModalProductosComponent>,
    private store: Store<AppStore>,
    @Inject(MAT_DIALOG_DATA) public unidad: Unidad
  ) {
  }

  ngOnInit() {
    this.initColumnsTable();
    this.initObservers();
  }

  private initColumnsTable() {
    this.columnsTable = [
      {
        text: 'Codigo',
        keyReferenceToRow: 'codigo',
        type: 'string'
      }, {
        text: 'Nombre',
        keyReferenceToRow: 'nombre',
        type: 'string'
      }, {
        text: 'Descripcion',
        keyReferenceToRow: 'descripcion',
        type: 'string'
      }, {
        text: 'Precio',
        keyReferenceToRow: 'precio_publico',
        type: 'string'
      }/*, {
        text: 'Categoria',
        type: 'catalog',
        keyReferenceToRow: 'categoria',
        dataCatalog: (listCategoris as Array<Catalog>)
      }, {
        text: 'Estatus',
        keyReferenceToRow: 'estatus',
        type: 'string'
      }*/
    ]
  }

  private initObservers() {
    // this.categoriasProdcutoStore.onChanges.subscribe(list => {
    //   if (list) this.initColumnsTable(list);
    // });

    this.store.pipe(select(productosSelectors.selectProductos)).subscribe(list => {
      if (list) {
        this.listProducts = list.filter((prod) => {
          let index = this.unidad.productos.findIndex(item => item.id == prod.id);
          return index == -1;
        });
      }
    });
  }

  acept() {
    let items = this.tableProducts.getRowsChecked();
    this.dialogRef.close(items);
  }

}

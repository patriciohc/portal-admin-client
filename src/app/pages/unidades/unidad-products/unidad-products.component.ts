import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute }  from '@angular/router';
// models
import { Unidad } from '../../../models/unidad.model';
// components
import { Column } from '../../../models/table';
// import { CategoriasProdcutoStore } from '../../../store/categorias-producto.store';
import { Producto } from '../../../models/producto.model';
import { Catalog } from '../../../models/table';
import { MatDialog } from '@angular/material/dialog';
import { ModalProductosComponent } from './modal-productos/modal-productos.component';
import { LoadingService } from './../../../services/loading.service';
import { AppStore } from './../../../reducers/index';
import { selectUnidad, selectError, selectUnidadesLoading, selectUnidades } from '../../../selectors/unidad.selectors';
import { Store, select } from '@ngrx/store';
import * as unidadesActions from '../../../actions/unidad.actions';
import * as productosSelectors from '../../../selectors/producto.selectors';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-unidad-products',
  templateUrl: './unidad-products.component.html',
  styleUrls: ['./unidad-products.component.css']
})
export class UnidadProductsComponent implements OnInit {

  public productsUnidad: Array<any> = [];
  public listProducts: Array<any> = [];
  public unidades: Array<Unidad> = [];
  public unidadSelected: Unidad;
  
  // UI
  categories: Array<any>;

  public headUnidades: Array<Column>  = [];

  constructor(
    private loading: LoadingService,
    private dialog: MatDialog,
    // private categoriasProdcutoStore: CategoriasProdcutoStore,
    private route: ActivatedRoute,
    private store: Store<AppStore>
  ) {
  }

  ngOnInit() {

    let idUnidad = this.route.snapshot.params['id'];

    this.store.pipe(select(selectUnidadesLoading)).subscribe(isLoading => {
      if (isLoading) {
        this.loading.show();
      } else {
        this.loading.hide();
      }
    });

    this.store.pipe(select(selectUnidad, idUnidad)).subscribe(unidad => {
      if (unidad) {
        this.unidadSelected = unidad;
        this.fillListProductsUnidad();
      }
    });

    this.store.pipe(select(selectError)).subscribe(error => {
      if (error) {
          console.error(error);
          Swal.fire('', 'Ocurio un error, pongase en contacto con soporte por favor', 'error');
      }
    });

    this.store.select('categoriasProducto').subscribe(categoriasState => {
      if (categoriasState.categorias) {
        this.initColumnsTable(categoriasState.categorias);
      }
    });

    this.store.pipe(select(productosSelectors.selectProductos)).subscribe(list => {
      if (list) {
        this.listProducts = list;
        this.fillListProductsUnidad();
      }
    });
  }

  openDialogAddProducts(): void {
    const dialogRef = this.dialog.open(ModalProductosComponent, {
      data: this.unidadSelected
    });

    dialogRef.afterClosed().subscribe(list => {
      this.onClickAddProducts(list);
    });
  }

  private initColumnsTable(listCategoris: Array<any>) {
    this.headUnidades = [
      {
        text: 'Codigo',
        keyReferenceToRow: 'codigo',
        type: 'string'
      }, {
        text: 'Nombre',
        keyReferenceToRow: 'nombre',
        type: 'string'
      }, {
        text: 'Descripcion',
        keyReferenceToRow: 'descripcion',
        type: 'string'
      }, {
        text: 'Precio',
        keyReferenceToRow: 'precio_publico',
        type: 'string'
      }, /*, {
        text: 'Categoria',
        type: 'catalog',
        keyReferenceToRow: 'categoria',
        dataCatalog: (listCategoris as Array<Catalog>)
      }, {
        text: 'Estatus',
        keyReferenceToRow: 'estatus',
        type: 'string'
      },*/ {
        text: 'Quitar',
        type: 'action',
        dataAction: {
          key: 'quitar',
          class: 'red',
          icon: 'delete'
        }
      }
    ]
  }

  private fillListProductsUnidad() {
    if (this.unidadSelected && this.listProducts) {
      if (!this.unidadSelected.productos) {
        return;
      }
      this.productsUnidad = this.listProducts.filter((prod) => {
        let index = this.unidadSelected.productos.findIndex(item => item.id == prod.id);
        return index != -1;
      });
      console.log(this.productsUnidad);
      /*this.productsUnidad = list.map(item => {
        item.estatus = item.estatus ? 'Activo' : 'Inactivo';
        return item
      });*/
    }
  }

  public actionsTable(data) {
    switch (data.keyAction) {
      case 'quitar':
        let dataRemve = {idUnidad: this.unidadSelected.id, idProducto: data.row.id}
        this.store.dispatch(unidadesActions.removeProductFromUnidad(dataRemve));
        break;
    }
  }

  async onClickAddProducts(list: Array<any>) {
    if (list && list.length > 0) {
        list = list.map(item => ({id: item.id}));
        this.store.dispatch(
          unidadesActions.addProductsToUnidad({id: this.unidadSelected.id, list})
        );
    }
  }

  onClickAddAllProducts() {
    this.store.dispatch(
      unidadesActions.addAllProductsToUnidad({idUnidad: this.unidadSelected.id})
    );
    /*try {
      this.loading.show();
      let prodAdds = await this.unidadRepository
        .addAllProductsToUnidad(this.unidadSelected.id);
      if (Array.isArray(prodAdds)) {
        this.unidadSelected.productos = prodAdds;
        this.unidadesStore.updateById(this.unidadSelected.id, this.unidadSelected);
      }
    } catch (error) {
      Swal.fire('', 'Ocurrio un error intente mas tarde', 'error');
      console.error(error);
    } finally {
      this.loading.hide();
    }*/
  }

}

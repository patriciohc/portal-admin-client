import { Component, OnInit, ViewChild } from '@angular/core';
import { TableComponent } from '../../components/table/table.component';
import { Router } from '@angular/router';
// models
import { Unidad } from '../../models/unidad.model';

import { Column } from '../../models/table';
import { AppStore } from './../../reducers/index';
import { selectUnidades, selectError } from '../../selectors/unidad.selectors';
import { Store, select } from '@ngrx/store';
import * as unidadesActions from '../../actions/unidad.actions';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-unidades',
    templateUrl: './unidades.component.html',
    styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit {

    @ViewChild('tableUnidades', { static: true }) tableUnidades: TableComponent;
    public showFormResgistroSucursal: boolean = false;
    public list: Array<Unidad>;

    headUnidades: Array<Column> = [
        {
            text: 'Nombre',
            type: 'string',
            keyReferenceToRow: 'nombre'
        }, {
            text: 'Direccion',
            type: 'string',
            keyReferenceToRow: 'direccion'
        }, {
            text: 'Hora Apertura',
            type: 'string',
            keyReferenceToRow: 'hora_apetura'
        }, {
            text: 'Hora Cierre',
            type: 'string',
            keyReferenceToRow: 'hora_cierre'
        }, {
            text: 'Editar',
            type: 'action',
            dataAction: {
                key: 'editar',
                class: '',
                icon: 'edit'
            }
        }, {
            text: 'Eliminar',
            type: 'action',
            dataAction: {
                key: 'eliminar',
                class: 'red',
                icon: 'delete'
            }
        }
    ];

    constructor(
        private router: Router,
        private store: Store<AppStore>,
    ) { }

    ngOnInit() {
        this.store.pipe(select(selectUnidades)).subscribe(unidades => {
            if (unidades) this.list = unidades;
        });

        this.store.pipe(select(selectError)).subscribe(error => {
            if (error) {
                console.error(error);
                Swal.fire('', 'Ocurio un error, pongase en contacto con soporte por favor', 'error');
            }
        });
    }

    public async actionsTable(data: any) {
        switch (data.keyAction) {
            case 'editar':
                this.router.navigate(['/admin/unidades/editar/' + data.row.id])
                break;
            case 'eliminar':
                let response = await Swal.fire({
                    title: '¿Estas seguro que quieres eliminar la unidad?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar'
                } as any);
                if (!response.value) return;
                this.store.dispatch(unidadesActions.deleteUnidad({id: data.row.id}));
                Swal.fire(`se elimino correctamente`);
                break;
        }
    }


}

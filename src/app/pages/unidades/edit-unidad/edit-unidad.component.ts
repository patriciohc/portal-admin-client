import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute }  from '@angular/router';
import { Store, select } from '@ngrx/store';

import { FormUnidadComponent } from '../../../components/form-unidad/form-unidad.component';
import { MapaComponent } from '../../../components/mapa/mapa.component';
import { UnidadProductsComponent } from '../unidad-products/unidad-products.component';
import { Unidad } from '../../../models/unidad.model';
import { LoadingService } from '../../../services/loading.service';
import { Position } from '../../../models/position';
import { AppStore } from './../../../reducers/index';
import { selectUnidad, selectError } from '../../../selectors/unidad.selectors';
import * as unidadesActions from '../../../actions/unidad.actions';
import { selectUnidadesLoading } from './../../../selectors/unidad.selectors';

import QRCode from 'qrcode';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-unidad',
  templateUrl: './edit-unidad.component.html',
  styleUrls: ['./edit-unidad.component.css']
})
export class EditUnidadComponent implements OnInit {
  
  public showForm = 'info-general';
  public unidad: Unidad;
  public position: Position;

  @ViewChild('formRegistroUnidad', {static: true})
  private formRegistro: FormUnidadComponent;
  @ViewChild('mapa', {static: true})
  private mapa: MapaComponent
  @ViewChild('unidadProducts', {static: true})
  private unidadProducts: UnidadProductsComponent;
  public idUnidad: any;
  public urlCode: string;

  constructor(
    private loading: LoadingService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppStore>,
  ) { }

  ngOnInit() {
    this.idUnidad = this.route.snapshot.params['id'];
    this.createQrImage();

    this.store.pipe(select(selectUnidad, this.idUnidad)).subscribe(unidad => {
      this.unidad = unidad;
      this.position = new Position(this.unidad.lat, this.unidad.lng);
    });

    this.store.pipe(select(selectError)).subscribe(error => {
      if (error) {
          console.error(error);
          Swal.fire('', 'Ocurio un error, pongase en contacto con soporte por favor', 'error');
      }
    });

    this.store.pipe(select(selectUnidadesLoading)).subscribe(isLoading => {
      if (isLoading) {
        this.loading.show();
      } else {
        this.loading.hide();
      }
    });

  }

  public downloadQr () {
    if (!this.urlCode) {
      Swal.fire('', 'Error al generar el codigo qr');
      return;
    }

    const link = document.createElement('a');
    link.href = this.urlCode;
    link.download = 'sucursal-qr.png';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  public showInfoGral() {
    // this.showForm = 'info-general';
    // let params = this.idUnidad
    
    // this.services.get(API_UNIDAD, {params})
    // .subscribe((result: any) => {
    //   this.unidad = result;
    //   setTimeout(()=> {
    //     this.formRegistro.setUnidad(this.unidad);
    //   }, 1000)
    // })
  }

  public showInfoGeo() {
    // this.showForm = 'mapa';
    // this.services.getUnidadPolygon(this.idUnidad)
    // .subscribe((result) => {
    //   console.log(result)
    //   this.mapa.setData({
    //     position: {lng: this.unidad.lng, lat: this.unidad.lat},
    //     polygon: result
    //   })
    // })
  }

  public showInfoProd() {
    this.showForm = 'add-products';
  }

  // public async saveChange() {
  //   switch(this.showForm) {
  //     case 'info-general':
  //       this.saveInfoGeneral();
  //       break;
  //     case 'mapa':
  //       this.saveDataGeo()
  //       break;
  //   }
  // }

  async saveInfoGeneral () {
    let data = this.formRegistro.getUnidad();
    if (data.invalid) return;
    let unidad = new Unidad(data.value);
    unidad.id = this.idUnidad;
    this.store.dispatch(unidadesActions.updateUnidad({unidad}));
  }

  async saveDataGeo() {
    var result;
    if (!this.idUnidad) return;
    let dataGeo = this.mapa.getData();

    // if (dataGeo.polygon.length > 0) {
    //   result = await this.services.addPolygon(this.idUnidad, dataGeo.polygon)
    //   if (result.code == 'SUCCESS') {
    //     return Swal.fire('Se actualizo la informacion')
    //   } else {
    //     Swal.fire('Ocurrio en un error, intente mas tarde')
    //     throw result.data;
    //   }
    // }
  }

  async updatePosition(position: Position) {
    const unidad = {...this.unidad, ...position};
    delete unidad.productos;
    delete unidad.socio;
    this.store.dispatch(unidadesActions.updateUnidad({unidad}))
  }


  private async createQrImage() {
    this.urlCode = await QRCode.toDataURL(this.idUnidad,
      {
        version: '',
        errorCorrectionLevel: 'M',
        margin: 4,
        scale: 4,
        width: 20,
        color: {
          dark: '#000',
          light: '#FFF'
        }
      });
  }

}

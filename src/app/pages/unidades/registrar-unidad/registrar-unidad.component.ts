import { Component, OnInit, ViewChild } from '@angular/core';
import { Router }  from '@angular/router';
// components
import { FormUnidadComponent } from '../../../components/form-unidad/form-unidad.component';
// services
// import { EntityService } from '../../../services/entity.service';
import { LoadingService } from '../../../services/loading.service';
// models
import { Unidad } from '../../../models/unidad.model';
import { AppStore } from './../../../reducers/index';
import { selectNewUnidad, selectError, selectUnidadesLoading } from '../../../selectors/unidad.selectors';
import { Store, select } from '@ngrx/store';
import * as unidadesActions from '../../../actions/unidad.actions';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-unidad',
  templateUrl: './registrar-unidad.component.html',
  styleUrls: ['./registrar-unidad.component.css']
})
export class RegistrarUnidadComponent implements OnInit {

  @ViewChild('formRegistroUnidad', {static: true})
  formRegistro: FormUnidadComponent;
  showForm = 'info-general';

  constructor(
    private loading: LoadingService,
    // private entityService: EntityService,
    private router: Router,
    private store: Store<AppStore>
  ) { 

    this.store.pipe(select(selectUnidadesLoading)).subscribe(isLoading => {
      if (isLoading) {
        this.loading.show();
      } else {
        this.loading.hide();
      }
    });

    this.store.pipe(select(selectNewUnidad)).subscribe(unidad => {
      if (unidad) {
        Swal.fire(
          'Se guardo correctamente',
          'La unidad se guardo correctamente, agregue productos a esta unidad',
          'success'
        );
        this.store.dispatch(unidadesActions.cleanNewUniad());
        this.router.navigate(['/admin/unidades/editar/' + unidad.id]);
      }
    });

    this.store.pipe(select(selectError)).subscribe(error => {
      if (error) {
          console.error(error);
          Swal.fire('', 'Ocurio un error, pongase en contacto con soporte por favor', 'error');
      }
    });
  }

  ngOnInit() {}

  async saveInfoGeneral () {
    let data = this.formRegistro.getUnidad();
    if (data.invalid) return;
      let unidad = new Unidad(data.value)
      this.store.dispatch(unidadesActions.addUnidad({unidad}))
  }

  cancelar() {}

}

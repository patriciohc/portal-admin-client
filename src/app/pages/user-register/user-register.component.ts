import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// services
import { ApiService } from '../../logposeapp-api/api.service';
import { API_USUARIO } from '../../logposeapp-api/api-routes';
import { LoadingService } from './../../services/loading.service';
// models
import { Request } from '../../models/request';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit {

  public formRegistro: FormGroup;

  constructor(
    private loading: LoadingService,
    private formBuilder: FormBuilder,
    private router: Router,
    private services: ApiService) { }

  ngOnInit() {
    const patterForPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
    this.formRegistro = this.formBuilder.group({
      'nombre' : [
        '',
        Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      ],
      'apellidos' : [
        '',
        Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(100)])
      ],
      'correo_electronico' : [
        '',
        Validators.compose([Validators.required, Validators.email])
      ],
      'telefono' : [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern(/^[1-9]\d*$/)
        ])
      ],
      'password': [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          Validators.pattern(patterForPass)
        ])
      ]
    });
  }

  public async onClickRegister() {
    if (this.formRegistro.invalid) {
      Swal.fire('Algunos campos son requeridos');
      return;
    }
    const user = this.formRegistro.value;
    const request = new Request();
    request.body = user;
    this.loading.show();
    try {
      const result = await this.services.post(API_USUARIO.toString(), request).toPromise();
      Swal.fire('¡Gracias por registrarse!');
      this.clear();
      this.router.navigate(['login']);
      console.log(result);
    } catch (error) {
      console.error(error);
      if (error['error']) {
        Swal.fire(error['error']);
      } else {
        Swal.fire('Ouccurrio un error al guardar la informacion, contacte a soporte');
      }
    } finally {
      this.loading.hide();
    }
  }

  private clear() {
    this.formRegistro.reset();
  }

  public clickOnCancel() {
    this.router.navigate(['/productos']);
  }

}

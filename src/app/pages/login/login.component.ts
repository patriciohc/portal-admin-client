import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {Store, select, createSelector} from '@ngrx/store';
import { AppStore } from '../../reducers';
import { loadRefreshToken } from '../../actions/token.actions';
import { Account, PERFIL_EMPLEADO, PERFIL_SOCIO } from '../../models/account';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from '../../services/loading.service';
import {selectRefreshTokenError, selectRefreshTokenIsLoading} from '../../selectors/token.selectors';
import {RequestError} from '../../models/request-error';
import { selectAccount } from '../../selectors/account.selectors';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // UI properties
  typeLogin = 'OPERADOR';
  formLogin: FormGroup;

  constructor(
    private router: Router,
    private store: Store<AppStore>,
    private loading: LoadingService,
    private fb: FormBuilder
  ) {

    this.formLogin = this.fb.group({
      'correo_electronico' : ['', Validators.compose([Validators.email, Validators.required])],
      'password' : ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      'typeAccount': ['socio'],
      'keepSession': [false]
    });

    this.store.pipe(select(selectRefreshTokenIsLoading))
      .subscribe((isLoading: boolean) => {
        if (isLoading) {
          this.loading.show();
        } else {
          this.loading.hide();
        }
      });

    this.store.pipe(select(selectRefreshTokenError))
      .subscribe((error: RequestError) => {
        if (error) {
          if (error.statusCode === 401) {
            Swal.fire('', 'El nombre de usuaro o contraseña son incorrectos' , 'warning');
          } else {
            console.error(error);
            Swal.fire('', 'Ocurrio un error intente mas tarde porfavor' , 'error');
          }
        }
      });

    this.store.pipe(select(selectAccount))
      .subscribe((account: Account) => {
        if (account) this.redirect(account);
      });

  }

  async ngOnInit() { }

  public async login() {
    if (!this.formLogin.valid) {
      Swal.fire('', 'Escriba el nombre de usuario y contraseña');
      return;
    }

    this.store.dispatch(loadRefreshToken(this.formLogin.value));
  }

  onChangeSelct(){ }

  private redirect (account: Account) {
    if (account.perfil === PERFIL_EMPLEADO &&
       (!account.accountIsCreated || account.perfil === PERFIL_EMPLEADO)
    ) {
      // this.router.navigate(['/homet']);
    } else if (account.perfil === PERFIL_SOCIO && !account.accountIsCreated) {
      this.router.navigate(['/configuraciones']);
    } else if (account.accountIsCreated ||
               account.perfil === PERFIL_SOCIO
    ) {
      this.router.navigate(['/admin/unidades']);
    }
  }

}

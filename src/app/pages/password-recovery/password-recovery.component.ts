import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap }  from '@angular/router';
import { ApiService } from '../../logposeapp-api/api.service';
import { API_USUARIO } from '../../logposeapp-api/api-routes';
import { LoadingService } from './../../services/loading.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent implements OnInit {

  // ui properties
  email: string;
  mode: string = 'password_recovery';
  password1: string;
  password2: string;
  formReset: FormGroup;
  token: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api: ApiService,
    private route: ActivatedRoute,
    private loading: LoadingService
  ) { }

  ngOnInit() {
    const patterForPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
    this.formReset = this.fb.group({
      'password1': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(patterForPass)])],
      'password2': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(patterForPass)])]
    });

    this.token = this.route.snapshot.params['token']
    if (this.token) {
      this.mode = 'reset_password';
    }
    /*((params: ParamMap) => {
      this.token = params.get('token');
      if (this.token) {
        this.mode = 'reset_password';
      }
    });*/
  }

  async onClickRecovery () {
    let body = {correo_electronico: this.email};
    try {
      this.loading.show('Enviando correo...');
      let response: any = await this.api.post(
        API_USUARIO.passwordRecovery,
        {body}).toPromise();
      if (response.success) {
        this.mode = 'send_mail';
      }
    } catch (error) {
      console.log(error);
    } finally {
      this.loading.hide();
    }
  }

  onClickCancel() {
    this.router.navigate(['/login']);
  }

  async onClickResetPassword () {
    if (!this.formReset.valid) return;
    if (typeof this.token != 'string') {
      Swal.fire('', 'El enlace ha caducado', 'error');
      return;
    }
    let password1 = this.formReset.controls['password1'].value;
    let password2 = this.formReset.controls['password2'].value;
    if (password1 != password2) return;
    let body = { contraseña: password2, token: this.token };
    try {
      this.loading.show('Cambiando contraseña...');
      let response: any = await this.api.post(API_USUARIO.passwordReset, {body}).toPromise();  
      if (response.success) {
        this.router.navigate(['/login']);
      } else {
        Swal.fire('', 'El enlace ha caducado', 'error');
      }
    } catch (error) {
      Swal.fire('', 'El enlace ha caducado', 'error');
    } finally {
      this.loading.hide();
    }
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router }  from '@angular/router';
// services
import { ApiService } from '../../logposeapp-api/api.service';
// models
import { Unidad } from '../../models/unidad.model';
import { Column } from './../../models/table';
import { Store } from '@ngrx/store';
import { AppStore } from '../../reducers';
import * as actionsEmpleados from '../../actions/empleado.actions';
import { selectEmpleados } from './../../selectors/empleados.selectors';
import { Empleado } from './../../models/empleado.model';

import Swal from 'sweetalert2';

const regExpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  public headUnidades: Array<Column>;

  public listEmpleados: Array<Empleado>;
  public listUnidades: Array<Unidad>;
  public listRoles: Array<any>;

  public correo_electronico: string;
  public rol: number;
  public idUnidad: number;

  constructor(
    private store: Store<AppStore>,
    private services: ApiService,
    private router: Router
  ) {
    this.headUnidades = [
      { 
        text: 'Nombre',
        type: 'string',
        keyReferenceToRow: ['nombre', 'apellidos']
      }, {
        text: 'Correo Electronico',
        type: 'string',
        keyReferenceToRow: 'correo_electronico'
      }, {
        text: 'Eliminar',
        type: 'action',
        dataAction: {
          key: 'eliminar',
          class: 'red',
          icon: 'delete'
        }
      }
    ]
  
    this.store.select(selectEmpleados)
      .subscribe(empleados => {
        this.listEmpleados = empleados;
      });
  }

  async ngOnInit() {

  }

  /*private getListRoles() {
    return new Promise<Array<any>>((resolve, reject) => {
      this.services.getListRoles()
      .subscribe(result => {
        resolve(result);
      })
    })
  }*/

  public actionsTable(data) {
    switch (data.keyAction) {
      case 'update':
        /*this.services.updateOperador(data.row.id, data.row)
        .subscribe(result => {
        })*/
        break;
      case 'eliminar':
        console.log(data.row)
        this.store.dispatch(actionsEmpleados.deleteEmpleado({id: data.row.id}))
        break;
    }
  }

  public clickOnAgregarEmpleado() {
    let isEmail = regExpEmail.test(this.correo_electronico);
    if (!isEmail) {
      return Swal.fire('', '¡El correo electronico no es valido!')
    }
    this.store.dispatch(actionsEmpleados.addEmpleado({
      correo_electronico: this.correo_electronico
    }))
  }

}

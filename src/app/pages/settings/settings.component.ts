import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { Observable } from 'rxjs';

import { SocioSettings, Settings } from '../../models/socio-settings.model';
import { AppStore } from '../../reducers';
import { selectAccount, selectAccountIsLoading, selectAccountError } from '../../selectors/account.selectors';
import { Account } from '../../models/account';
import { Socio } from '../../models/socio.model';
import { selectSocio, selectSocioIsNew, selectSocioLoading } from '../../selectors/socio.selectors';
import { LoadingService } from '../../services/loading.service';
import { RequestError } from '../../models/request-error';
import { updateSocio, createSocio, updateStyles } from '../../actions/socio.actions';
import { CategoriaSocio } from '../../models/categoria-socio.model';
import { reloadRefreshToken } from './../../actions/token.actions';
import { selectCategoriasSocio } from '../../selectors/categoriaSocio.selectors';
import { SocioSettingService } from '../../logposeapp-api/socio-settings.service';
import { newMessage } from '../../actions/message.actions';
import { ImageService } from './../../services/images.service';
import { FILE_LOGO } from '../../constants/share';
import { convertFileToUrl, getSizeImg } from '../../utils/utils';
import { SizeImage } from '../../models/image';
import { DataImageEditor, ImageEditorComponent } from '../../components/image-editor/image-editor.component';


import Swal from 'sweetalert2';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

    providers: Array<any> = ['Vende Saldo'];
    formSetting: FormGroup;
    formGeneralConfig: FormGroup;
    setting: Settings;
    modeEdit: boolean;
    socio: Socio;
    account: Account
    logoImg: { url: string | SafeUrl, file: File };
    promocionalUrlImg: string;
    promocionalNameImg: string;
    listCategoriasSocio$: Observable<Array<CategoriaSocio>>;
    account$: Observable<Account>;
    socio$: Observable<Socio>;

    constructor(
        private sanitizer: DomSanitizer,
        private socioSttingService: SocioSettingService,
        private formBuilder: FormBuilder,
        private store: Store<AppStore>,
        private loading: LoadingService,
        private imageService: ImageService,
        public dialog: MatDialog,
    ) {

        this.logoImg = {
            url: null,
            file: null
        };

        this.listCategoriasSocio$ = this.store.pipe(select(selectCategoriasSocio));
        this.account$ = this.store.pipe(select(selectAccount));
        this.socio$ = this.store.pipe(select(selectSocio));
    }

    ngOnInit() {

        this.formSetting = this.formBuilder.group({
            'provider': ['', Validators.compose([Validators.required])],
            'usuario': ['', Validators.compose([Validators.required])],
            'password': ['', Validators.compose([Validators.required])],
            'pin': ['', Validators.required]
        });

        this.formGeneralConfig = this.formBuilder.group({
            'nombre_empresa': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            'descripcion': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            // 'palabras_clave': ['', Validators.compose([Validators.minLength(8)])],
            'categorias': [[], Validators.required],
            'has_punto_venta': [false, Validators.required],
            'has_facturacion': [false, Validators.required],
            'has_solicitud_servicio': [false, Validators.required],

            'razon_social': ['', Validators.compose([Validators.minLength(4)])],
            'rfc': ['', Validators.compose([Validators.minLength(12)])],
            'direccion_fiscal': ['', Validators.compose([Validators.minLength(4)])]

        });

        this.account$.subscribe((account: Account) => {
            if (account) {
                this.account = account;
                this.init();
            }
        });

        this.socio$
            .subscribe((socio) => {
                if (socio) {
                    this.socio = socio;
                    this.init();
                }
            });

        this.store.pipe(select(selectSocioIsNew)).subscribe(isNew => {
            if (isNew) {
                this.store.dispatch(reloadRefreshToken());
            }
        });

        this.store.pipe(select(selectAccountIsLoading))
            .subscribe((isLoading: boolean) => {
                if (isLoading) {
                    this.loading.show();
                } else {
                    this.loading.hide();
                }
            });

        this.store.pipe(select(selectSocioLoading))
            .subscribe((isLoading: boolean) => {
                if (isLoading) {
                    this.loading.show();
                } else {
                    this.loading.hide();
                }
            });

        this.store.pipe(select(selectAccountError))
            .subscribe((error: RequestError) => {
                if (error) {
                    if (error.statusCode === 401) {
                        Swal.fire('', 'El nombre de usuaro o contraseña son incorrectos', 'warning');
                    } else {
                        console.error(error);
                        Swal.fire('', 'Ocurrio un error intente mas tarde porfavor', 'error');
                    }
                }
            });

    }

    private init() {
        if (this.account && this.account.accountIsCreated && this.socio) {
            this.initSettingsForms(this.socio.id);
            const fullPath = this.imageService.getFullPathImg('logo', 'logo.jpg');
            this.logoImg.url = this.socio.logo_is_saved ? fullPath : 'assets/logo.png';
            
            if (this.socio.styles && this.socio.styles.imagen_promocional) {
                this.promocionalUrlImg = this.imageService.getFullPathImg(
                    'promocional', this.socio.styles.imagen_promocional
                );
            }

            this.setValuesFormGeneralConfig(this.socio);
        }
    }

    private async initSettingsForms(socioId: string) {
        const settings: any = await this.socioSttingService.getSettings(socioId).toPromise();
        this.setting = settings.find(item => item.nombre === 'recargas');
        if (this.setting) {
            this.modeEdit = false;
            this.formSetting.patchValue({ provider: this.setting.info.provider.name });
        }
    }

    setValuesFormGeneralConfig(socio: Socio) {
        for (let key in socio) {
            if (key == 'categorias') {
                continue;
            }
            const value = socio[key];
            const hasControl = this.formGeneralConfig.controls.hasOwnProperty(key)
            if (value !== null && value !== undefined && hasControl) {
                this.formGeneralConfig.controls[key].setValue(value);
            }
        }
        const categorias = socio.categorias.map(c => c.id);
        this.formGeneralConfig.controls['categorias'].setValue(categorias);
    }

    async onSaveSettings() {
        let form = this.formSetting.value;
        let access = form.usuario + '|' + form.password;
        let settings = {
            nombre: 'recargas',
            info: {
                provider: form.provider,
                keys: { access }
            }
        }
        let data = new SocioSettings();
        data.pin = form.pin;
        data.settings = settings;
        try {
            if (this.setting && this.setting.id) {
                await this.socioSttingService.updateSettings(this.setting.id, data).toPromise();
            } else {
                await this.socioSttingService.createSettings(data).toPromise();
            }
            this.modeEdit = false;
            this.store.dispatch(newMessage({ text: 'Se guardo correctamente' }));
        } catch (error) {
            this.store.dispatch(newMessage({ text: 'Ocurrio un error ' }));
            console.error(error);
        }
    }

    public async setLogoImg(target) {
        const inputFile = target.files[0];
        if(inputFile == null) {
          return;
        }
    
        if (inputFile.size > 3145728) {
          return Swal.fire('', 'seleccione una imagen menor a 3MB');
        }
    
        var sizeImage: SizeImage = await getSizeImg(inputFile);
        if (sizeImage.width < 250 || sizeImage.height < 200) {
          return Swal.fire('', 'seleccione una imagen de tamaño minimo 250 X 200');
        }
        let source = await convertFileToUrl(inputFile); 
    
        target.value = '';
    
        const dataEditor: DataImageEditor = {
          source, sizeImage,
          stdSize: new SizeImage(250, 200),
          aspectRatio: 0.8,
          finalSize: new SizeImage(250, 200)
        }
    
        const dialogRef = this.dialog.open(ImageEditorComponent, {
          data: dataEditor,
          panelClass: 'editor-modal'
        });
    
        dialogRef.afterClosed().subscribe(async ({file}) => {
          if (file) {
            this.logoImg.file = file;
            this.logoImg.url = this.sanitizer.bypassSecurityTrustUrl(
                URL.createObjectURL(file)
            );
          }
        });
    }

    onSavePromocionalImg({imgName}) {
        this.promocionalNameImg = imgName;
        this.promocionalUrlImg = this.imageService.getFullPathImg('promocional', imgName);
    }

    async saveStyles() {
        if (this.promocionalNameImg == null) {
            Swal.fire('', 'No ha seleccionado ninguna imagen promocional, es necesaria para que te pudan ver tus clientes');
        } else {
            const styles = {imagen_promocional: this.promocionalNameImg};
            this.store.dispatch(updateStyles({styles}));
        }
    }

    async saveGeneralConfig() {
        if (this.formGeneralConfig.invalid) {
            Swal.fire('', 'Algunos campos son requeridos');
            return;
        }

        if (!this.logoImg.url) {
            let config: any = {
                title: '',
                text: 'No has seleccionado un logo, es nesesario para que te puedan ver tus clientes ¿quiere continuar?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }
            let response = await Swal.fire(config);
            if (!response.value) return;
        }

        const formValue = this.getDataGeneralConfig();

        let socio = {
            ...formValue,
            categorias: formValue.categorias.map(c => ({ id: c }))
        };
        
        for (let key in socio) {
            if (socio[key] === null || socio[key] === undefined) {
                delete socio[key];
            }
        }

        if (this.logoImg.file) {
            socio['logo_is_saved'] = await this.imageService.saveImage(FILE_LOGO, this.logoImg.file);
        }

        if (this.account.accountIsCreated) {
            socio['id'] = this.socio.id;
            this.store.dispatch(updateSocio({ socio }));
        } else {
            this.store.dispatch(createSocio({ socio }));
        }
    }

    getDataGeneralConfig() {
        const data = {...this.formGeneralConfig.value};
        if (!data['razon_social']) {
            delete data['razon_social'];
        }

        if (!data['rfc']) {
            delete data['rfc'];
        }

        if (!data['direccion_fiscal']) {
            delete data['direccion_fiscal'];
        }

        return data;
    }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Usuario } from './../../models/usuario';
import { AppStore } from './../../reducers/index';
import { Store, select } from '@ngrx/store';
import { selectUsuario } from './../../selectors/account.selectors';
import { SafeUrl } from '@angular/platform-browser';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFireStorage } from '@angular/fire/storage';
import { updateUsuario } from '../../actions/account.actions';
import { environment as env } from '../../../environments/environment';


import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  formProfile: FormGroup;
  usuario$: Observable<Usuario>;
  profileImg: {url: string | SafeUrl, file: File};

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppStore>,
    private sanitizer: DomSanitizer,
    private afStorage: AngularFireStorage,
  ) {

    this.formProfile = this.formBuilder.group({
      'id': [''],
      'nombre': ['', Validators.compose([Validators.required])],
      'apellidos': ['', Validators.compose([Validators.required])],
      'telefono': ['', Validators.compose([Validators.required])],
      'correo_electronico': [{value: null, disabled: true}]
    });

    this.profileImg = {
      url: 'assets/account_72x72.png',
      file: null
    };

    this.usuario$ = this.store.pipe(select(selectUsuario));
  }

  ngOnInit() {
    this.usuario$.subscribe(usuario => {
      if (usuario) {
        this.formProfile.setValue({
          id: usuario.id,
          nombre: usuario.nombre,
          apellidos: usuario.apellidos,
          telefono: usuario.telefono,
          correo_electronico: usuario.correo_electronico
        });
        const fullPath = this.getFullPathImg(usuario.id);
        this.profileImg.url = usuario.foto_is_saved ? fullPath : 'assets/account_72x72.png';
      }
    });

  }

  setProfileImg(target) {
    this.profileImg.file = target.files[0];
    this.profileImg.url = this.sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(target.files[0])
    );
  }

  async saveInfoUsuario() {
    if (this.formProfile.invalid) {
      return;
    }

    let formValue = this.formProfile.value;
    formValue['correo_electronico'] = this.formProfile.controls['correo_electronico'].value;
    const usuarioId = formValue.id

    if (!this.profileImg.file) {
      let config: any = {
        title: '',
        text: 'No ha seleccionado una foto de perfil, ¿quiere continuar?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }
      let response = await Swal.fire(config);
      if (!response.value) return;
    }

    let foto_is_saved = false;
    if (this.profileImg.file) {
      const fullPath = '/' + usuarioId + '/usuario/foto.jpg';
      foto_is_saved = await this.saveImage(fullPath);
    }
    const usuario = {
      ...formValue,
      foto_is_saved,
    };
    this.store.dispatch(updateUsuario({usuario}));
}

getFullPathImg(usuarioId: string) {
 return env.bucket + usuarioId + '%2Fusuario%2Ffoto.jpg?alt=media';
}


async saveImage (fullPath: string) {
  try {
    const result = await this.afStorage.upload(fullPath , this.profileImg.file);
    if (result.state != 'success') {
      Swal.fire('', 'Ocurrio un error al guardar la imagen', 'warning');
      return false;
    } else {
      return true;
    }
  } catch (error) {
    Swal.fire('', 'Ocurrio un error al guardar la imagen', 'warning');
    console.log(error);
    return false;
  }
}

}

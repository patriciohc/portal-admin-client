import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// services
import { ApiService } from '../../logposeapp-api/api.service';

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})
export class PushNotificationComponent implements OnInit {

  public hasNotifications: true;
  public formNotification: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService) { }

  ngOnInit() {
    this.formNotification = this.formBuilder.group({
      'titulo' : ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
      'mensaje' : ['', Validators.required],
      'url' : ['', Validators.required]
    });
  }


  public send() {
    if (this.formNotification.invalid) {
      return;
    } else {
      var data = this.formNotification.value;
      this.api.sendNotification(data)
      .subscribe((response: any) => {
        console.log(response);
      })
    }

  }

}

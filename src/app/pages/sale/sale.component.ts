import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Producto } from '../../models/producto.model';
import { Unidad } from '../../models/unidad.model';
import { ActivatedRoute } from '@angular/router';
import { ProductoVenta } from '../../models/producto-venta';
import { ModalRecargasComponent } from '../../components/modal-recargas/modal-recargas.component';
import { MatDialog } from '@angular/material/dialog';
import { LocalStorage } from 'angular-web-storage';
import { InfoRecargas } from '../../models/recarga';
import { selectUnidad, selectError } from '../../selectors/unidad.selectors';
import { Store, select } from '@ngrx/store';
import { AppStore } from './../../reducers/index';
import * as productosSelectors from '../../selectors/producto.selectors';
import * as ventaSelectors from '../../selectors/venta.selectors';
import * as productosActions from '../../actions/producto.actions';
import * as ventaActions from './../../actions/venta.actions';
import { newLocalVenta, addProductoToVenta } from '../../actions/venta.actions';
import { VentaService } from './../../logposeapp-api/venta.service';
import { Venta } from './../../models/venta.model';
import { Observable } from 'rxjs';
import { LoadingService } from './../../services/loading.service';

import Swal from 'sweetalert2';

@Component({
    selector: 'app-sale',
    templateUrl: './sale.component.html',
    styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {

    @LocalStorage('logpose.adminclient.inforecargas')
    private infoRecargas: InfoRecargas[];

    @ViewChild('txtProduct', { static: true }) txtProduct: ElementRef;
    @ViewChild('txtQuantity', { static: true }) txtQuantity: ElementRef;

    private unidadId: string;
    private isLoading$: Observable<boolean>;
    public venta$: Observable<Venta>;
    public listProductosVenta$: Observable<Array<ProductoVenta>>;

    predictProducts: Array<Producto> = [];
    listProducts: Array<Producto> = [];
    textSearch: string;
    unidadSelected: Unidad;
    indexActiveProduct: number = 0;
    sale: Venta;
    quantity: number = 1;


    constructor(
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private store: Store<AppStore>,
        private ventaService: VentaService,
        private loading: LoadingService,
    ) {
        this.venta$ = this.store.pipe(select(ventaSelectors.selectNewVenta));
        this.listProductosVenta$ = this.store.pipe(select(ventaSelectors.selectProductosNewVenta));
    }

    ngOnInit() {
        this.isLoading$ = this.store.pipe(select(ventaSelectors.selectIsLoaging))
        this.unidadId = this.route.snapshot.params['id'];
        let listProducts: Array<Producto>;

        this.store.pipe(select(productosSelectors.selectProductos)).subscribe(list => {
            if (list) {
                listProducts = list;
                this.initListProducts(listProducts);
            }
        });

        this.store.pipe(select(selectUnidad, this.unidadId)).subscribe(unidad => {
            this.unidadSelected = unidad;
            this.initListProducts(listProducts);
        });

        this.store.pipe(select(ventaSelectors.selectRequestError)).subscribe(error => {
            if (error) {
                Swal.fire('No se pudo realizar la recarga', error.message, 'error');
                this.store.dispatch(ventaActions.okRequestFailure());
            }
        });

        this.venta$.subscribe(venta => {
            this.sale = venta;
        });

        this.isLoading$.subscribe(isLoading => {
            if (isLoading) {
                this.loading.show();
              } else {
                this.loading.hide();
              }
        });

        this.store.dispatch(newLocalVenta({ unidadId: this.unidadId }));
    }

    private initListProducts(listProducts: Array<any>) {
        if (this.unidadSelected && this.unidadSelected.productos && listProducts) {
            for (let i = 0; i < this.unidadSelected.productos.length; i++) {
                let index = listProducts.findIndex(item => {
                    return item.id == this.unidadSelected.productos[i].id;
                });
                if (index != -1) {
                    this.listProducts.push(listProducts[index]);
                }
            }
        }
    }

    async onKeyDown(event) {
        var key = event.keyCode;
        switch (key) {
            case 40: //'ArrowDown'
                if (this.indexActiveProduct < this.predictProducts.length - 1) {
                    this.indexActiveProduct += 1;
                }
                break;
            case 38: //'ArrowUp':
                if (this.indexActiveProduct > 0) {
                    this.indexActiveProduct -= 1;
                }
                break;
        }
        let element: any = document.querySelector('li.active');
        if (element) element.scrollIntoView();
    }

    /**
     * keyup en input de busqueda de producto
     * @param event 
     */
    async onKeyUpInput(event) {
        if (!this.textSearch || this.textSearch.length < 2) return;
        var key = event.keyCode;
        switch (key) {
            case 13: //'Enter':
                if (this.predictProducts.length > 0) {
                    this.txtQuantity.nativeElement.focus();
                    this.txtQuantity.nativeElement.select();
                } else {
                    console.log('no se encontraron productos');
                }
                break;
            default:
                let text: any = this.textSearch.toLowerCase();
                if (!isNaN(text)) {
                    text = parseInt(text).toString();
                }
                this.predictProducts = this.listProducts.filter(item => {
                    return item.nombre.toLowerCase().includes(text)
                        || item.codigo.toLowerCase().includes(text);
                });
        }
    }

    async onEnterQuantity() {
        if (!this.predictProducts.length) return;
        var producto = this.predictProducts[this.indexActiveProduct];
        if (!producto) {
            this.txtProduct.nativeElement.focus();
            return;
        }
        this.store.dispatch(addProductoToVenta({ producto, cantidad: this.quantity }))
        // this.sale.addProductToSale(activeProduct, this.quantity);
        this.quantity = 1
        this.textSearch = '';
        this.predictProducts = [];
        this.indexActiveProduct = 0;
        this.txtProduct.nativeElement.focus();
    }

    onClickUpdateList(event) {
        event.preventDefault();
        this.store.dispatch(productosActions.loadListProductos())
        // this.productosStore.updateListProductosFromService();
    }

    async onClickFinishSale(): Promise<void> {
        if (this.sale.lista_productos.length == 0) {
            Swal.fire('No se han agregado productos');
            return;
        }
        var response: any = await Swal.fire({
            title: 'Cantidad recibida',
            input: 'text',
            showCancelButton: true,
            inputValidator: (cantidad) => {
                let number = parseFloat(cantidad);
                if (isNaN(number)) return 'dato no valido';
                if (this.sale.total > number) {
                    return 'La cantidad recibida debe ser mayor a ' + this.sale.total;
                }
            }
        });
        if (response.dismiss) return;
        let cambio = parseFloat(response.value) - this.sale.total;
        Swal.fire('Cambio: $' + cambio);
        this.store.dispatch(ventaActions.saveVenta({ venta: this.sale }));
        this.store.dispatch(newLocalVenta({ unidadId: this.unidadId }));
    }

    onClickCancelSale() {
        this.quantity = 1
        this.textSearch = '';
        this.predictProducts = [];
        this.indexActiveProduct = 0;
        this.store.dispatch(newLocalVenta({ unidadId: this.unidadId }));
        this.txtProduct.nativeElement.focus();
    }

    onClickLista(index: number) {
        this.indexActiveProduct = index;
        if (this.predictProducts.length > 0) {
            this.txtQuantity.nativeElement.focus();
            this.txtQuantity.nativeElement.select();
        } else {
            console.log('no se encontraron productos');
        }
    }

    onClickFullscreen() {
        var e: any = document.querySelector('body');
        if (e.webkitRequestFullScreen) {
            e.webkitRequestFullScreen();
        } else if (e.mozRequestFullScreen) {
            e.mozRequestFullScreen();
        }
    }

    removeItem(item: ProductoVenta) {
        this.store.dispatch(ventaActions.removeProductToVenta({ productoId: item.producto.id }))
    }

    async openModalRecargaElectronica() {
        if (!this.infoRecargas) {
            const { value: pin } = await Swal.fire({
                title: 'Pin de recargas',
                input: 'password',
                inputPlaceholder: ''
            });
            if (pin) {
                try {
                    let info: any = await this.ventaService.getInfoRecargas(pin as string);
                    if (info) {
                        this.infoRecargas = info;
                        this.openModalRecaras();
                    }
                } catch (error) {
                    console.error(error);
                    if (error.status && error.status == 400) {
                        Swal.fire('', error.error, 'error');
                    } else {
                        Swal.fire('', 'Ocurrio un error desconocido', 'error');
                    }
                }
            }
        } else {
            this.openModalRecaras();
        }
    }

    openModalRecaras() {
        const dialog = this.dialog.open(ModalRecargasComponent, {
            width: '400px',
            disableClose: true,
            data: {
                infoRecargas: this.infoRecargas,
                unidad: this.unidadSelected
            }
        });

        dialog.afterClosed().subscribe(result => {
            this.store.dispatch(ventaActions.clearDataRecarga());
        });
    }

}


import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { UnidadesComponent } from './pages/unidades/unidades.component';
import { RegistrarUnidadComponent } from './pages/unidades/registrar-unidad/registrar-unidad.component';
import { EditUnidadComponent } from './pages/unidades/edit-unidad/edit-unidad.component';
import { ProductsComponent } from './pages/products/products.component';
import { EditProductComponent } from './pages/products/edit-product/edit-product.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { EditCategoriesComponent } from './pages/categories/edit-categories/edit-categories.component';
import { PedidosComponent } from './pages/pedidos/pedidos.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { UserRegisterComponent } from './pages/user-register/user-register.component';
import { PushNotificationComponent } from './pages/push-notification/push-notification.component';
import { PasswordRecoveryComponent } from './pages/password-recovery/password-recovery.component';
import { MenuAdminComponent } from './pages/menu-admin/menu-admin.component';
import { SaleComponent } from './pages/sale/sale.component';
import { ReportSalesComponent } from './pages/report-sales/report-sales.component';
import { ConfirmEmailComponent } from './pages/confirm-email/confirm-email.component';
import { HomeEmployeeComponent } from './pages/home-employee/home-employee.component';

import { AuthGuardService } from './auth/auth-guard.service';

import { ROLES } from './config';
import { SettingsComponent } from './pages/settings/settings.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AboutComponent } from './pages/about/about.component';
import { AvisoPrivacidadComponent } from './pages/aviso-privacidad/aviso-privacidad.component';

const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent,
  }, {
    path: 'aviso_privacidad',
    component: AvisoPrivacidadComponent,
  }, {
    path: 'login',
    component: LoginComponent,
  }, {
    path: 'recuperar-contrasena',
    component: PasswordRecoveryComponent,
  }, {
    path: 'recuperar-contrasena/:token',
    component: PasswordRecoveryComponent,
  }, {
    path: 'confirmar-correo/:token',
    component: ConfirmEmailComponent
  }, {
    path: 'homet',
    canActivate: [AuthGuardService],
    component: HomeEmployeeComponent
  }, {
    path: 'perfil',
    canActivate: [AuthGuardService],
    component: ProfileComponent,
  }, {
    path: 'configuraciones',
    component: SettingsComponent,
    canActivate: [AuthGuardService],
    data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
  }, {
    path: 'admin',
    component: MenuAdminComponent,
    children: [
      {
        path: 'unidades',
        component: UnidadesComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'unidades/registrar',
        component: RegistrarUnidadComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'unidades/editar/:id',
        component: EditUnidadComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'productos',
        component: ProductsComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]},
      }, {
        path: 'productos/editar',
        component: EditProductComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'productos/editar/:id',
        component: EditProductComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'categorias',
        component: CategoriesComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'categoria/editar',
        component: EditCategoriesComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'categoria/editar/:id',
        component: EditCategoriesComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'usuarios',
        component: UsuariosComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      }, {
        path: 'resporte-ventas-sitio/:id',
        component: ReportSalesComponent,
        canActivate: [AuthGuardService],
        data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
      },
      {path: '**', redirectTo: 'unidades', pathMatch: 'full'},
    ]
  }, {
    path: 'pedidos/:id',
    component: PedidosComponent,
    canActivate: [AuthGuardService],
    data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE, ROLES.OPERADOR_UNIDAD, ROLES.SIN_ROL]}
  }, {
    path: 'registrar-usuario',
    component: UserRegisterComponent,
    data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE]}
  }, {
    path: 'push-notification',
    component: PushNotificationComponent,
    canActivate: [AuthGuardService],
    data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE, ROLES.OPERADOR_UNIDAD]}
  }, {
    path: 'venta/:id',
    component: SaleComponent,
    canActivate: [AuthGuardService],
    data: {roles: [ROLES.CLIENTE, ROLES.ADMIN_CLIENTE, ROLES.OPERADOR_UNIDAD]}
  },
  {path: '**', redirectTo: 'login', pathMatch: 'full'},
];


// - Updated Export
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(routes);

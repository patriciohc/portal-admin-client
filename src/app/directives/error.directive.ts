import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[appError]'
})
export class ErrorDirective {

  @Input() textForPattern: string;

  constructor(private el: ElementRef) { }

  @Input()
  set appError (errors: any) {
    if (errors == null) {
      this.el.nativeElement.innerHTML = '';
    } else if (errors.required) {
      this.el.nativeElement.innerHTML = 'Este campo es requerido';
    } else if (errors.minlength) {
      this.el.nativeElement.innerHTML = `Este campo requiere como mínimo ${errors.minlength.requiredLength} caracteres`
    } else if (errors.maxlength) {
      this.el.nativeElement.innerHTML = `Este campo acepta como máximo ${errors.maxlength.requiredLength} caracteres`
    } else if (errors.email) {
      this.el.nativeElement.innerHTML = 'Correo electronico no valido';
    } else if (errors.pattern) {
      this.el.nativeElement.innerHTML = this.textForPattern || 'Formato no admitido'
    }
  }

}

import { SizeImage } from '../models/image';

/**
 * crea un elemento html, un solo nodo
 * @param {string} html
 * @returns {ElementHTML}
 */
export function createElement (html) {
  if (!html) return null;
  var div = document.createElement('div');
  div.innerHTML = html.trim()
  return div.firstChild;
}

/**
 * obtiene el tamaño de imagen
 * @param {File} file
 * @returns {Object}
 */
export function getSizeImg (file): Promise<SizeImage> {
  let _URL = window.URL
  let img = new Image()
  return new Promise((resolve, reject) => {
    img.onload = function (event) {
      var el: any = event.target
      return resolve({width: el.width, height: el.height})
    }
    img.src = _URL.createObjectURL(file)
  })
}

/**
 * obtiene el tamaño de imagen
 * @param {File} file
 * @returns {String}
 */
export function convertFileToUrl (file): Promise<string> {
  var reader = new FileReader()
  return new Promise (resolve => {
    reader.onload = function (event) {
      var el: any = event.target;
      resolve(el.result);
    }
    reader.readAsDataURL(file)
  })
}

/**
 * espera un tiempo time para ejecutar resolve
 * @param {number} time - tiempo que espera
 * @returns {Promise}
 */
export function sleep(time) {
  return new Promise(resolve => {
    setTimeout(resolve, time)
  })
}

export function normaliza(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  // remove accents, swap ñ for n, etc
  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
  var to   = "aaaaaeeeeeiiiiooooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }
  return str;
}

export function cloneObject(obj: any) {
  return JSON.parse(JSON.stringify(obj));
}


export function addEventListenerVisibility(callback) {
  var hidden, visibilityChange; 
  if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
    hidden = "hidden";
    visibilityChange = "visibilitychange";
  } else if (typeof (document as any).msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
  } else if (typeof (document as any).webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
  }

  if (typeof document.addEventListener === "undefined" || hidden === undefined) {
    console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
  } else {
    // Handle page visibility change   
    document.addEventListener(visibilityChange, () => {
      callback(document[hidden] == false)
    }, false);
  
  }
}
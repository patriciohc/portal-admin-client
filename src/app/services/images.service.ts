import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { AppStore } from './../reducers/index';
import { Store, select } from '@ngrx/store';
import { selectAccount } from '../selectors/account.selectors';
import { Account } from '../models/account';
import { PATH_LOGO, PATH_PRODUCTS, PATH_CATEGORIAS, PATH_PROMOCIONAL } from '../constants/share';
import { environment as env } from '../../environments/environment';

import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class ImageService {

    account: Account;
    account$: Observable<Account>;

    constructor(
        private afStorage: AngularFireStorage,
        private store: Store<AppStore>,

    ) {
        this.account$ = this.store.pipe(select(selectAccount));
        this.account$.subscribe((account: Account) => {
            if (account) {
                this.account = account;
            }
        });
    }

    async saveImage(path: string, file: File) {
        let fullPath = '/' + this.account.usuario.id + path;
        try {
            const result = await this.afStorage.upload(fullPath, file);
            if (result.state != 'success') {
                Swal.fire('', 'Ocurrio un error al guardar la imagen', 'warning');
                return false;
            } else {
                return true;
            }
        } catch (error) {
            Swal.fire('', 'Ocurrio un error al guardar la imagen', 'warning');
            console.log(error);
            return false;
        }
    }

    getPathByType(type: string) {
        switch(type) {
            case 'logo':
                return PATH_LOGO;
            case 'productos':
                return PATH_PRODUCTS;
            case 'categorias':
                return PATH_CATEGORIAS;
            case 'promocional':
                return PATH_PROMOCIONAL;
        }
    }

    getFullPathImg(type: string, imgName: string) {
        let usuarioId = this.account.usuario.id;
        let path = this.getPathByType(type);
        path = path.replace(/\//g, '%2F');
        return env.bucket + usuarioId + path + '%2F' + imgName + '?alt=media';
    }
}
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment as env } from './../../environments/environment';
import { AccountService } from '../logposeapp-api/account.service';

const ONESIGNAL_CONFIG = {
    appId: env.onesignal.apiKey,
    autoRegister: false,
    notifyButton: {
        enable: true,
    }
}

@Injectable({
    providedIn: 'root'
})
export class OneSignalService {

    private oneSignal: any;
    public notifications: BehaviorSubject<any>;
    public changeSubscribe: BehaviorSubject<boolean>;

    constructor(private accountService: AccountService) {
        this.notifications = new BehaviorSubject(null);
        this.changeSubscribe = new BehaviorSubject(null);
        this.oneSignal = (<any>window).OneSignal || [];
    }

    public initOneSignal() {
        this.oneSignal.push(() => {
            this.oneSignal.init(ONESIGNAL_CONFIG);
            this.listenSubcription();
            this.listenNotifications();
        });
    }

    public async updateUserId() {
        const id_device = await this.getUserId();
        if (id_device) {
            this.accountService.updateUsuario({id_device}).toPromise();
        }
    }

    private listenNotifications() {
        this.oneSignal.push(() => {
            this.oneSignal.on('notificationDisplay', (event) => {
                console.log('OneSignal notification displayed:', event);
                console.log(event);
                this.notifications.next(event);
            });
        });
    }

    private listenSubcription() {
        this.oneSignal.push(() => {
            this.oneSignal.on('subscriptionChange', async (isSubscribed: boolean) => {
                console.log("The user's subscription state is now:", isSubscribed);
                this.updateUserId();
                this.changeSubscribe.next(isSubscribed);
            });
        });
    }

    public getUserId(): Promise<string> {
        return new Promise((resolve) => {
            this.oneSignal.push(() => {
                this.oneSignal.getUserId((userId: string) => {
                    console.log("OneSignal User ID:", userId);
                    return resolve(userId);
                });
            });
        })
    }

    public sendTagApp() {
        return new Promise((resolve, reject) => {
            this.oneSignal.push(async () => {
                try {
                    const result = await this.oneSignal
                        .sendTags({tagApp: env.onesignal.tagApp});
                    resolve(result);
                } catch (error) {
                    reject(error);
                }
            });
        })
    }

}

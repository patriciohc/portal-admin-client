import { GenericResponse } from '../models/generic-response';
import { BaseService } from '../logposeapp-api/base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTicketPrint } from '../models/ticket.model';
import { KEY_PRINTER_SERVICE_PORT } from './../constants/share';

class UserConfig {
    printerId: string;
}

@Injectable({
    providedIn: 'root'
})
export class PrinterService extends BaseService {

    server = 'http://localhost';
    port: string;

    constructor (public http: HttpClient) {
        super(http);
        this.port = localStorage.getItem(KEY_PRINTER_SERVICE_PORT);
    }

    public savePort(port: string) {
        this.port = port;
        localStorage.setItem(KEY_PRINTER_SERVICE_PORT, port);
    }

    public getPort() {
        return this.port;
    }

    private getUrl(path: string) {
        if (!this.port) {
            throw 'No se ha configurado el puerto del servicio de impresion';
        }
        return this.server + ':' + this.port + '/' + path;
    }

    public async getListPrinters() {
        let url = this.getUrl('getListPrinters');
        return this.rawGet<Array<any>>(url).toPromise();
    }

    public async getPrinterStatus() {
        let url = this.getUrl('getPrinterStatus');
        return this.rawGet<{status: boolean}>(url).toPromise();
    }

    public async initPrinter() {
        let url = this.getUrl('initPrinter');
        return this.rawGet<GenericResponse>(url).toPromise();
    }

    public async printerTest() {
        let url = this.getUrl('printerTest');
        return this.rawGet<GenericResponse>(url).toPromise();
    }
        
    public async getUserConfig() {
        let url = this.getUrl('getUserConfig');
        return this.rawGet<any>(url).toPromise();
    }
        
    public async saveUserConfig(userConfig: UserConfig) {
        let url = this.getUrl('saveUserConfig');
        return this.rawPost<GenericResponse>(url, userConfig).toPromise();
    }

    public async printerTicket(ticket: DataTicketPrint) {
        let url = this.getUrl('printerTicket');
        return this.rawPost<boolean>(url, ticket).toPromise();
    }

    public async testConecction() {
        if (!this.port) {
            return false;
        }
        let url = this.getUrl('testConecction');
        try {
            await this.rawGet(url).toPromise();
            return true;
        } catch (error) {
            return false;
        }
    }

}
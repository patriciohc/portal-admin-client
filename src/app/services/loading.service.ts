import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoadingService {

  public eventLoading: Subject<{text?: string, show: boolean}>;

  constructor() { 
    this.eventLoading = new Subject(); 
  }

  public show(text: string = 'Cargando...') {
    this.eventLoading.next({text, show: true});
  }

  public hide() {
    this.eventLoading.next({show: false});
  }
}

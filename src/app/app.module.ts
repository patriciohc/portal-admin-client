import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG, HammerModule } from '@angular/platform-browser';
import { AngularWebStorageModule } from 'angular-web-storage';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// routing
import { routing } from './app.routers';

// material
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

// services
import { ApiService } from './logposeapp-api/api.service';
import { HttpInterceptorService } from './auth/http-interceptor.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { LoadingService } from './services/loading.service';
// import { EntityService } from './services/entity.service';
// import { CategoriasProdcutoStore } from './store/categorias-producto.store';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { BannerComponent } from './components/banner/banner.component';
import { UnidadesComponent } from './pages/unidades/unidades.component';
import { FormUnidadComponent } from './components/form-unidad/form-unidad.component';
import { TableComponent } from './components/table/table.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { RegistrarUnidadComponent } from './pages/unidades/registrar-unidad/registrar-unidad.component';
import { EditUnidadComponent } from './pages/unidades/edit-unidad/edit-unidad.component';
import { ProductsComponent } from './pages/products/products.component';
import { GaleryComponent } from './components/galery/galery.component';
import { EditProductComponent } from './pages/products/edit-product/edit-product.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { EditCategoriesComponent } from './pages/categories/edit-categories/edit-categories.component';
import { UnidadProductsComponent } from './pages/unidades/unidad-products/unidad-products.component';
import { PedidosComponent } from './pages/pedidos/pedidos.component';
import { DialogsComponent } from './components/dialogs/dialogs.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { UserRegisterComponent } from './pages/user-register/user-register.component';
import { PushNotificationComponent } from './pages/push-notification/push-notification.component';
import { ImageEditorComponent } from './components/image-editor/image-editor.component';
import { PasswordRecoveryComponent } from './pages/password-recovery/password-recovery.component';
import { MenuAdminComponent } from './pages/menu-admin/menu-admin.component';
import { SaleComponent } from './pages/sale/sale.component';
import { LoadingButtonComponent } from './components/loading-button/loading-button.component';
import { ReportSalesComponent } from './pages/report-sales/report-sales.component';
import { ChartComponent } from './components/chart/chart.component';
import { ConfirmEmailComponent } from './pages/confirm-email/confirm-email.component';
import { ErrorDirective } from './directives/error.directive';
import { HomeEmployeeComponent } from './pages/home-employee/home-employee.component';
import { ModalProductosComponent } from './pages/unidades/unidad-products/modal-productos/modal-productos.component';
import { ModalSelectUnidadComponent } from './components/modal-select-unidad/modal-select-unidad.component';
import { ModalRecargasComponent } from './components/modal-recargas/modal-recargas.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ModalTicketComponent } from './components/modal-ticket/modal-ticket.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { SocioEffects } from './effects/socio.effects';
import { RefreshTokenEffects } from './effects/token.effects';
import {AccountEffects} from './effects/account.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { EmpleadoEffects } from './effects/emplead.effects';
import { UnidadEffects } from './effects/unidad.effects';
import { ProductoEffects } from './effects/producto.effects';
import { CategoriaSocioEffects } from './effects/categoria-socio.effects';
import { CategoriaProductoEffects } from './effects/categoria-producto.effects';
import { PedidosEffects } from './effects/pedido.effects';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ProfileComponent } from './pages/profile/profile.component';
import { VentaEffects } from './effects/venta.effects';
import { PrinterConfigComponent } from './components/printer-config/printer-config.component';
import { AboutComponent } from './pages/about/about.component';
import { AvisoPrivacidadComponent } from './pages/aviso-privacidad/aviso-privacidad.component';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
    'pan': {direction: 30}
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BannerComponent,
    UnidadesComponent,
    FormUnidadComponent,
    TableComponent,
    MapaComponent,
    RegistrarUnidadComponent,
    EditUnidadComponent,
    ProductsComponent,
    GaleryComponent,
    EditProductComponent,
    CategoriesComponent,
    EditCategoriesComponent,
    UnidadProductsComponent,
    PedidosComponent,
    DialogsComponent,
    UsuariosComponent,
    UserRegisterComponent,
    PushNotificationComponent,
    ImageEditorComponent,
    PasswordRecoveryComponent,
    MenuAdminComponent,
    SaleComponent,
    LoadingButtonComponent,
    ReportSalesComponent,
    ChartComponent,
    ConfirmEmailComponent,
    ErrorDirective,
    HomeEmployeeComponent,
    ModalProductosComponent,
    ModalSelectUnidadComponent,
    ModalRecargasComponent,
    SettingsComponent,
    ModalTicketComponent,
    ProfileComponent,
    PrinterConfigComponent,
    AboutComponent,
    AvisoPrivacidadComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig, 'portal-admin-client'),
    MatNativeDateModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatIconModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    AngularWebStorageModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatListModule,
    MatDatepickerModule,
    // MatNativeDateModule,
    MatExpansionModule,
    MatSidenavModule,
    MatSlideToggleModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([
      AppEffects,
      SocioEffects,
      RefreshTokenEffects,
      AccountEffects,
      EmpleadoEffects,
      UnidadEffects,
      ProductoEffects,
      CategoriaSocioEffects,
      PedidosEffects,
      VentaEffects,
      CategoriaProductoEffects
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    HammerModule
  ],
  providers: [
    LoadingService,
    ApiService,
    AuthGuardService,
    // EntityService,
    // CategoriasProdcutoStore,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalProductosComponent,
    ModalSelectUnidadComponent,
    ModalRecargasComponent,
    ModalTicketComponent,
    ImageEditorComponent

  ]
})
export class AppModule { }


export class CategoriaProducto {
    public id: string;
    public nombre?: string;
    public descripcion?: string;
    public imagen?: string;

    constructor (props: any = {}) {
      this.id = props.id;
      this.nombre = props.nombre;
      this.descripcion = props.descripcion;
      this.imagen = props.imagen;
    }
  }
import { TipoVenta } from './venta.model';
import { RecargaElectronica } from './producto-especial.model';

export class DataTicketPrint {
    no_ticket: string;
    fecha: string;
    tipo: string;
    socio: {
        razon_social: string
    };
    unidad: {
        direccion: string, 
        telefono: string
    };
    items: RecargaElectronica|Array<TicketProducto>;
    total: number;
}

export class TicketProducto {
    producto: string;
    cantidad: number;
    total: number;
}

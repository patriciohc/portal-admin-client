
export class SocioSettings {
    id?: string;
    pin?: string;
    settings: Settings;
}

export class Settings {
    id?: string;
    nombre: string;
    info: any;
}
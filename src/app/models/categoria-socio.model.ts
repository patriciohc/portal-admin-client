export interface CategoriaSocio {
  id: string;
  nombre: string;
  descripcion: string;
}
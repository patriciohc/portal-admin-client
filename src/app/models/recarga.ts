export class InfoRecargas {
    compania: string;
    servicios: Array<ServiciosCarrier>;
}

export class Producto {
    nombre: string;
    montoCodigo: string;
} 

export class ServiciosCarrier {
    nombre: string;
    is_paquete: boolean;
    productos: Array<Producto>
}

export class Recarga {
    compania: string;
    telefono: string;
    producto: Producto;
    is_paquete: boolean;
    unidad_id: string;
    pin: string;
}
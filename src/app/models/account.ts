import { Usuario } from './usuario';

export const PERFIL_SUPERUSER = 'super';
export const PERFIL_SOCIO = 'socio';
export const PERFIL_EMPLEADO = 'empleado';
export const PERFIL_USUARIO = 'usuario';

export type TypeAccount = 'super' | 'socio' | 'admin' | 'empleado' | 'usuario';


export class Account {
  accountIsCreated: boolean;
  usuario: Usuario;
  perfil: string;
  id_socio?: string;
  id_empleado?: string;
  id_unidad?: string;
  /**
   * tiene permisos para agregar editar o eliminar, productos, unidades, empleados
   */
  hasPermisionCrud: boolean;
  
  /**
   * tiene permisos para consultar reportes de venta
   */
  hasPermissionReports: boolean;

  /**
   * tiene permisos para cambiar la configuracion del socio
   */
  hasPermissionConfig: boolean;
}

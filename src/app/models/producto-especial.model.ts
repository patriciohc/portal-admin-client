
import { ServicioTaxi } from './servicio-taxi.model';

export class CatProductosEspeciales {
    id: string;
    nombre: string;
    iva: number;
    descripcion: string;
    info: any;
}

export class RecargaElectronica {
    cantidad: string;
    compania: string;
    descripcion: string;
    folio: string;
    telefono: string;
    is_paquete: boolean;
}

export class ProductoEspecial {
    id?: string;
    producto?: CatProductosEspeciales;
    info: ServicioTaxi|RecargaElectronica;

    constructor(id: string, info:ServicioTaxi|RecargaElectronica ) {
        this.id = id;
        this.info = info;
    }
}
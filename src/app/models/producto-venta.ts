import { Producto } from './producto.model';

export class ProductoVenta {
    id?: string;
    producto: Producto;
    cantidad: number;
    descuento: number;
    total: number;
    subtotal: number;
    iva: number;
}

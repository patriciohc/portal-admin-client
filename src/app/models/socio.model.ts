export interface SocioStyles {
  id?: string
  imagen_promocional?: string;
}


export interface Socio {
  id: string;
  razon_social: string;
  nombre_empresa: string;
  descripcion: string;
  categorias: Array<any>;
  has_notification: boolean;
  has_chat: boolean;
  has_punto_venta: boolean;
  has_facturacion:boolean;
  has_solicitud_servicio: boolean;
  logo_is_saved: boolean;
  zona_horaria: string;
  palabras_clave: string;
  direccion_fiscal: string;
  rfc: string;
  styles: SocioStyles;
}

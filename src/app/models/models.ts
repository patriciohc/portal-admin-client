export class Operador {
  public id?: number;
  public nombre?: string;
  public apellido_paterno?: string;
  public apellido_materno?: string;
  public correo_electronico?: string;
  public rol?: string;
}

export interface Response {
  success: boolean;
  code: string;
  message: string;
  data?: any;
  error?: any;
}

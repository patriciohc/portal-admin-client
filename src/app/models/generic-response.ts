export interface GenericResponse {
    success: boolean;
    message?: string;
    error?: any;
    data?: any;
}

export class Producto {
  public id?: string;
  public nombre: string;
  public codigo: string;
  public descripcion: string;
  public precio?: number;
  public precio_publico?: number;
  public precio_online?: number;
  public imagen?: Array<string>;
  public categoria: any;
  public iva: number;
  public activo: boolean;

  constructor(props: any = {}) {
    if (props.id) {
      this.id = props.id;
    }
    this.nombre = props.nombre;
    this.codigo = props.codigo
    this.descripcion = props.descripcion
    this.precio = props.precio;
    this.precio_publico = props.precio_publico;
    this.imagen = props.imagen;
    this.categoria = props.categoria;
    this.activo = props.activo;
  }
}
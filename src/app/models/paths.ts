export interface Paths {
    basePath: string;
}

export interface Routes {
    [model:string]: Paths;
}
export class SizeImage {
    width: number;
    height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }
}

export class Imagen {
    id?: string;
    key: string;
    url: string;
    type_image: boolean;
    label: string;
    date: string;
}


export class Unidad {
    public id?: string
    public nombre?: string;
    public direccion?: string;
    public telefono?: string;
    public lat?: string;
    public lng?: string;
    public referencia?: string;
    public hora_apetura?: string;
    public hora_cierre?: string;
    public id_cliente?: string;
    public servicio_domicilio?: boolean;
    public productos?: Array<{id: string}>;
    public socio?: any;

    constructor (props: any = {}) {
        this.nombre = props.nombre
        this.direccion = props.direccion;
        this.telefono = props.telefono;
        this.hora_apetura = props.hora_apetura;
        this.hora_cierre = props.hora_cierre;
        this.id_cliente = props.id_cliente;   
        this.servicio_domicilio = props.servicio_domicilio;
    }
}
  
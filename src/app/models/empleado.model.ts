
export interface Empleado {
    id: string;
    nombre: string;
    apellidos: string;
    correo_electronico: string;
    telefono: string;
    foto: string;
}
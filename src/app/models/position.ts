export class Position {
  
    constructor (lat: any, lng: any) {
      this.lat = lat;
      this.lng = lng
    }
  
    lat: any;
    lng: any;
}
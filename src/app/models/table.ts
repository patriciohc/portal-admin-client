export class Catalog {
    id: number|string;
    nombre: string
  }
  
export class Action {
    key: string;
    class?: string;
    icon?: string;
}
  
export class Column {
    text: string;
    type: 'string'|'catalog'|'action';
    icon?: string
    keyReferenceToRow?: string|Array<string>;
    dataCatalog?: Array<Catalog>;
    dataAction?: Action;
    editable?: boolean;
}
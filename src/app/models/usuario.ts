export class Usuario {
    id?: string;
    nombre?: string;
    apellidos?: string;
    telefono?: string;
    correo_electronico?: string;
    password?: string;
    type_login?: string;
    correo_electronico_valido?: boolean;
    telefono_valido?: boolean;
    foto_is_saved?: boolean;
    id_device?: string;
}
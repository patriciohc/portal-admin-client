// producto especial
import { Position } from './position';
import { Empleado } from './empleado.model';

export class ServicioTaxi {
    direccion_origen: Position;
    direccion_destino?: Position;
    empleado_asignado?: Empleado;
}


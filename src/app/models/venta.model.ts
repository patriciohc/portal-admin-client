import { ProductoVenta } from './producto-venta';
import { ProductoEspecial, RecargaElectronica } from './producto-especial.model';
import { Usuario } from './usuario';
import { Empleado } from './empleado.model';
import { Unidad } from './unidad.model';

export enum MetodoPago {
    EFECTIVO,
    PAYPAL,
    VISA
}

export enum EstatusVenta {
    ESPERANDO, // esperando a ser aceptado por la sucursal
    ACEPTADO, // aceptado por la sucursal
    EN_PROGRESO, // en progreso (en camino)
    FINALIZADO, // finalizdo
    CANCELADO // cancelado
}

export enum TipoVenta {
    REGULAR, // lista de productos regulares
    ESPECIAL // productos especial
}

export enum TipoHoraEntrega {
    LO_ANTES_POSIBLE = 0, // no requires hora_entrega
    HORA_PROGRAMADA = 1, // se desea en la hora programada
    DESPUES_HORA_PROGRAMADA = 2 // se desa a cualquier hora despues de la hora programada
}

export enum TipoEntrega {
    MOSTRADOR, // venta en mostrador
    DOMICILIO, // entrega a domicilio
}

export class InformacionEntrega {
    id?: string;
    lat?: number;
    lng?: number;
    hora_entrega?: string;
    tipo_entrega: TipoEntrega;
    tipo_hora_entrega: TipoHoraEntrega;
    repartidor_asignado?: Empleado;
}

export class Venta {
    id?: string;
    unidad: Unidad;
    comprador?: Usuario; // aplica para pedidos por app
    no_ticket?: string;
    fecha: Date | string;
    fecha_fin?: Date;
    subtotal: number;
    iva: number;
    descuento: number;
    total: number;
    lista_productos: Array<ProductoVenta>;
    producto_especial: ProductoEspecial;
    venta_en_linea: boolean;
    tipo: TipoVenta;
    metodo_pago: MetodoPago;
    informacion_entrega: InformacionEntrega;
    estatus?: EstatusVenta;

    constructor(unidadId: string, isOnLine = false, tipo = TipoVenta.REGULAR) {
        this.unidad = {id: unidadId};
        this.total = 0;
        this.lista_productos = [];
        this.venta_en_linea = isOnLine;
        this.tipo = tipo;
        this.metodo_pago = MetodoPago.EFECTIVO;
        this.estatus = EstatusVenta.FINALIZADO;
        
        const infoEntrega = new InformacionEntrega();
        infoEntrega.tipo_entrega = TipoEntrega.MOSTRADOR;
        infoEntrega.tipo_hora_entrega = TipoHoraEntrega.LO_ANTES_POSIBLE;
        
        this.informacion_entrega = infoEntrega; 
    }

    static creteFromRecarga(recargaResult: RecargaElectronica) {
        let venta = new Venta(null, false);
        venta.producto_especial = new ProductoEspecial(null, recargaResult);
        return venta;
    }

    static calculates (listaProductos: Array<any>) {
        let subtotal = 0;
        let total = 0;
        let iva = 0;
        for (let i = 0; i < listaProductos.length; i++) {
            subtotal += listaProductos[i].subtotal;
            total += listaProductos[i].total;
            iva += listaProductos[i].iva;
        }
        return {subtotal, total, iva};
    }


    static convertToModel (entity: any) {
        if (entity.comprador && entity.comprador.usuario) {
            entity.comprador = entity.comprador.usuario;
        }
    }
}
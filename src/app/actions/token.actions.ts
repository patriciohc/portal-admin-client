import { createAction, props } from '@ngrx/store';
import { TypeAccount } from '../models/account';
import {RequestError} from '../models/request-error';

export const loadRefreshToken = createAction(
  '[Token] Load refresh token',
  props<{
      correo_electronico: string,
      password: string,
      typeAccount: TypeAccount
  }>()
);

export const reloadRefreshToken = createAction(
  '[Token] reLoad refresh token'
);

export const setRefreshToken = createAction(
  '[Token] set refresh token',
  props<{refreshToken: string}>()
);

export const setToken = createAction(
  '[Token] set token',
  props<{token: string}>()
);

export const loadTokenFailure = createAction(
  '[Token] load token failure',
  props<{requestError: RequestError}>()
);

export const cleanToken = createAction(
  '[Token] clean refresh token data'
);

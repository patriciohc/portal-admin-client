import { createAction, props } from '@ngrx/store';
import { Socio, SocioStyles } from '../models/socio.model';
import {RequestError} from '../models/request-error';


export const loadSocio = createAction(
  '[Socio] Load Socio'
);

export const setSocio = createAction(
  '[Socio] Set data Socio',
  props<{socio: Socio, isNew: boolean}>()
);

export const createSocio = createAction(
  '[Socio] Create socio in data base',
  props<{socio: Socio}>()
);

export const updateSocio = createAction(
  '[Socio] Update data Socio in data base',
  props<{socio: Socio}>()
);

export const updateSocioLocalData = createAction(
  '[Socio] Update data Socio in local data',
  props<{socio: Socio}>()
);

export const updateStyles = createAction(
  '[Socio] Update data Socio styles in data base',
  props<{styles: SocioStyles}>()
);

export const updateLocalStyles = createAction(
  '[Socio] Update data Socio styles in local data',
  props<{styles: SocioStyles}>()
);

export const cleanSocio = createAction(
  '[Socio] clean data Socio in local data',
);

export const loadSocioFailure = createAction(
  '[Socio] load socio failure',
  props<{requestError: RequestError}>()
);

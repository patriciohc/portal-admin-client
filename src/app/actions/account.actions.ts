import { createAction, props } from '@ngrx/store';
import {Account} from '../models/account';
import {RequestError} from '../models/request-error';
import { Usuario } from './../models/usuario';

export const loadAccount = createAction(
  '[Account] Load account'
);

export const updateUsuario = createAction(
  '[Account] Call the api for to update data of user',
  props<{usuario: Usuario}>()
);

export const setLocalDataUsuario = createAction(
  '[Account] update local data of usuario',
  props<{usuario: Usuario}>()
);

export const setAccount = createAction(
  '[Account] Set data account',
  props<{account: Account}>()
);

export const loadAccountFailure = createAction(
  '[Account] load account failure',
  props<{requestError: RequestError}>()
);

export const cleanAccount = createAction(
  '[Account] clean data account'
);



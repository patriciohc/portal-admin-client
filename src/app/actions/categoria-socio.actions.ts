import { createAction, props } from '@ngrx/store';
import { RequestError } from '../models/request-error';
import { CategoriaSocio } from './../models/categoria-socio.model';


export const loadListCategoriasSocio = createAction(
    '[CategoriaSocio] Call the api to load list'
);

export const setListCategoriasSocio = createAction(
    '[CategoriaSocio] Set data list',
    props<{ categorias: Array<CategoriaSocio> }>()
);

export const requestFailure = createAction(
    '[CategoriaSocio] HTTP error empleados failure',
    props<{ requestError: RequestError }>()
);

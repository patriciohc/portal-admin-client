import { createAction, props } from '@ngrx/store';
import { Empleado } from '../models/empleado.model';
import {RequestError} from '../models/request-error';


export const loadListEmpleados = createAction(
  '[Empleado] Call the api to load list Empleados'
);

export const setListEmpleados = createAction(
    '[Empleado] Set data list empleados',
    props<{empleados: Array<Empleado>}>()
);

export const addEmpleado = createAction(
    '[Empleado] Call the api to add empleado to socio',
    props<{correo_electronico: string}>()
);

export const deleteEmpleado = createAction(
  '[Empleado] Call the api to detele empleado of the socio loggued',
  props<{id: string}>()
);

export const deleteEmpleadoToList = createAction(
  '[Empleado] delete Empleado to local data list',
  props<{id: string}>()
);

export const addEmpleadoToList = createAction(
    '[Empleado] add Empleado to local data list',
    props<{empleado: Empleado}>()
);

export const requestEmpleadosFailure = createAction(
  '[Empleado] HTTP error empleados failure',
  props<{requestError: RequestError}>()
);

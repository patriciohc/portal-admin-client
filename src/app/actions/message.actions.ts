import { createAction, props } from '@ngrx/store';


export const newMessage = createAction(
    '[Message] New message',
    props<{level?: string, text: string}>()
);

export const resetMessage = createAction(
    '[Message] clear messages'
);
import { createAction, props } from '@ngrx/store';
import { RequestError } from '../models/request-error';
import { CategoriaProducto } from '../models/categoria-producto.model';


export const loadListCategoriaProducto = createAction(
    '[CategoriaProducto] call the api logpose for to get list categoria producto'
);

export const addNewCategoriaProducto = createAction(
    '[CategoriaProducto] call the api logpose for to add new categoria producto',
    props<{ categoria: CategoriaProducto }>()
);

export const removeCategoriaProducto = createAction(
    '[CategoriaProducto] call the api logpose for to remove categoria producto',
    props<{ id: string }>()
);

export const updateCategoriaProducto = createAction(
    '[CategoriaProducto] call the api logpose for to update categoria producto',
    props<{ categoria: CategoriaProducto }>()
);

export const updateLocalCategoriaProducto = createAction(
    '[CategoriaProducto] update categoria producto to loca list',
    props<{ categoria: CategoriaProducto }>()
);

export const removeLocalCategoriaProducto = createAction(
    '[CategoriaProducto] remove categoria producto from local data',
    props<{ id: string }>()
);

export const setListCategoriaProducto = createAction(
    '[CategoriaProducto] set list categoria to local data',
    props<{ categorias: Array<CategoriaProducto> }>()
);

export const addLocalNewCategoriaProducto = createAction(
    '[CategoriaProducto] add new categoria producto to loca list',
    props<{ categoria: CategoriaProducto }>()
);

export const requestFailure = createAction(
    '[CategoriaProducto] HTTP error empleados failure',
    props<{ requestError: RequestError }>()
);

export const cleanError = createAction(
    '[CategoriaProducto] Clean HTTP error'
);


import { createAction, props } from '@ngrx/store';
import { Venta } from './../models/venta.model';


export const newTicket = createAction(
  '[Ticket] Load account',
  props<{venta: Venta}>()
);

export const clearTicket = createAction(
  '[Ticket] clear data ticket'
);

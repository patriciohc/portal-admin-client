import { createAction, props } from '@ngrx/store';
import { Socio } from '../models/socio.model';
import {RequestError} from '../models/request-error';
import {Unidad} from '../models/unidad.model';


// request actions
export const loadListUnidades = createAction(
  '[Unidad] Call api to load unidades list from server'
);

export const addUnidad = createAction(
  '[Unidad] Call api to add unidad to list',
  props<{unidad: Unidad}>()
);

export const updateUnidad = createAction(
  '[Unidad] Call api to update unidad',
  props<{unidad: Unidad}>()
);

export const deleteUnidad = createAction(
  '[Unidad] call api to remove unidad to list',
  props<{id: string}>()
);

export const addProductsToUnidad = createAction(
  '[Unidad] add products to unidad',
  props<{id: string, list: Array<{id: string}>}>()
);

export const addAllProductsToUnidad = createAction(
  '[Unidad] add all products to unidad',
  props<{idUnidad: string}>()
);

export const removeProductFromUnidad = createAction(
  '[Unidad] remove unidad from the unidad',
  props<{idUnidad: string, idProducto: string}>()
);

// local actions
export const setListUnidades = createAction(
  '[Unidad] Set local data list unidades',
  props<{unidades: Array<Unidad>}>()
);

export const updateLocalUnidad = createAction(
  '[Unidad] update local data unidad',
  props<{unidad: Unidad}>()
)

export const deleteUnidadToList = createAction(
  '[Unidad] delete unidad to local data list',
  props<{id: string}>()
);

export const addUnidadToList = createAction(
    '[Unidad] add unidad to local data list',
    props<{unidad: Unidad}>()
);

export const requestFailure = createAction(
  '[Unidad] request failure',
  props<{requestError: RequestError}>()
);

export const cleanNewUniad = createAction(
  '[Unidad] clean new unidad'
);

export const localUpdateProductsUnidad = createAction(
  '[Unidad] update list products of the unidad in local data',
  props<{id: string, list: Array<{id: string}>}>() 
)

export const localRemoveProductsUnidad = createAction(
  '[Unidad] remove product of the unidad in local data',
  props<{idUnidad: string, idProducto: string}>() 
)

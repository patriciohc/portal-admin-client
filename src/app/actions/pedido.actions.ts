import { createAction, props } from '@ngrx/store';
import { RequestError } from '../models/request-error';
import { Venta, EstatusVenta } from '../models/venta.model';


export const updateStatus = createAction(
    '[Pedidos] Actuliza estatus en base de datos',
    props<{pedidoId: string, estatus: EstatusVenta}>()
);

export const loadListPedidos = createAction(
    '[Pedidos] Carga lista de pedidos',
    props<{unidadId: string}>()
);

export const setListPedidos = createAction(
    '[Pedidos] set data list pedidos',
    props<{pedidos: Array<Venta>}>()
);

export const requestFailure = createAction(
    '[Pedido] HTTP error productos failure',
    props<{ requestError: RequestError }>()
);

export const updateEstatus = createAction(
    '[Venta] update estatus in data base',
    props<{pedidoId: string, estatus: EstatusVenta}>()
);

export const updateEstatusLocal = createAction(
    '[Venta] update estatus in  local data',
    props<{pedidoId: string, estatus: EstatusVenta}>()
);


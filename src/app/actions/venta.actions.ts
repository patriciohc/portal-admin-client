import { createAction, props } from '@ngrx/store';
import { RequestError } from '../models/request-error';
import { Venta, EstatusVenta } from '../models/venta.model';
import { Producto } from './../models/producto.model';
import { Recarga } from './../models/recarga';


export const saveVenta = createAction(
    '[Venta] call the api logpose for to save venta',
    props<{venta: Venta}>()
);

export const makeRecarga = createAction(
    '[Venta] call the api logpose make recagar',
    props<{recarga: Recarga}>()
);

export const setStatusRecargaRequest = createAction(
    '[Venta] la recarga electronica se realizo correctamente',
    props<{statusRecargaRequest: boolean}>()
);

export const clearDataRecarga = createAction(
    '[Venta] limpia datos de recarga electronica'
);

export const detailVenta = createAction(
    '[Venta] call the api logpose for to get the detail venta',
    props<{id: string}>()
);

export const loadReportVentas = createAction(
    '[Venta] call the api logpose for to load ventas',
    props<{unidadId: string, initDate: string, endDate: string}>()
);

export const setReportVentas = createAction(
    '[Venta] set local data ventas',
    props<{reporteVentas: any}>()
);

export const newLocalVenta = createAction(
    '[Venta] add new venta to local data',
    props<{unidadId: string}>()
);

export const addProductoToVenta = createAction(
    '[Venta] add producto from current venta in local data',
    props<{producto: Producto, cantidad: number}>()
);


export const removeProductToVenta = createAction(
    '[Venta] remove producto from current venta in local data',
    props<{productoId: string}>()
)

export const requestFailure = createAction(
    '[Venta] HTTP error in venta services',
    props<{ requestError: RequestError }>()
);

export const okRequestFailure = createAction(
    '[Venta] clean error property from state'
);


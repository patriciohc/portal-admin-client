import { createAction, props } from '@ngrx/store';
import { RequestError } from '../models/request-error';
import { Producto } from './../models/producto.model';


export const loadListProductos = createAction(
    '[Producto] Call the api to load list productos'
);

export const setListProductos = createAction(
    '[Producto] Set data list productos',
    props<{ productos: Array<Producto> }>()
);

export const addProducto = createAction(
    '[Producto] Call the api to add new producto to socio',
    props<{ producto: Producto }>()
);

export const updateProducto = createAction(
    '[Producto] call api to update producto',
    props<{producto: Producto}>()
  );

export const deleteProducto = createAction(
    '[Producto] Call the api to detele producto of the socio loggued',
    props<{ id: string }>()
);

export const deleteProductoToList = createAction(
    '[Producto] delete producto to local data list',
    props<{ id: string }>()
);

export const addProductoToList = createAction(
    '[Producto] add producto to local data list',
    props<{ producto: Producto }>()
);

export const updateProductoToList = createAction(
    '[Producto] update producto to local data list',
    props<{ producto: Producto }>()
);

export const requestFailure = createAction(
    '[Producto] HTTP error productos failure',
    props<{ requestError: RequestError }>()
);

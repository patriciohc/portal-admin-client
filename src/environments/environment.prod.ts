export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAFp5d4T4_74wN4sfd2qmNc2-5Bmqc3zOk",
    authDomain: "logposeapp-prod.firebaseapp.com",
    databaseURL: "https://logposeapp-prod.firebaseio.com",
    projectId: "logposeapp-prod",
    storageBucket: "logposeapp-prod.appspot.com",
    messagingSenderId: "533004015475",
    appId: "1:533004015475:web:13ce4ba1926162cfeb1706"
  },

  logposeApiConfig: {
    // api: 'https://voyage-madame-11841.herokuapp.com/api/'
    api: 'https://ws.logposeapp.com/api/'
  },

  onesignal: {
    apiKey: '5a9a6e18-3e23-4466-acbe-fcb181630386',
    tagApp: 'SOCIO_APP'
  },
  /**
  * firebase bucket
  */
  bucket: 'https://firebasestorage.googleapis.com/v0/b/logposeapp-prod.appspot.com/o/'
};

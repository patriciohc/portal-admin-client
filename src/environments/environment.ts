// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD5dt4bZipYnSUboDpzTvr84AoNiUWJjFg",
    authDomain: "logposeapp.firebaseapp.com",
    databaseURL: "https://logposeapp.firebaseio.com",
    projectId: "logposeapp",
    storageBucket: "logposeapp.appspot.com",
    messagingSenderId: "251307182807",
    appId: "1:251307182807:web:5e28e6e95957d908c27e92"
  },

  logposeApiConfig: {
    // api: 'https://ws-neec.herokuapp.com/api/'
    api: 'http://localhost:8088/api/'
  },
  onesignal: {
    apiKey: '57c22654-7696-44a9-8d2c-503590e2554f',
    tagApp: 'SOCIO_APP'
  },
  /**
  * firebase bucket
  */
  bucket: 'https://firebasestorage.googleapis.com/v0/b/logposeapp.appspot.com/o/'
};
